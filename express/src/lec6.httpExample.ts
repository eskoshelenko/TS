import { createServer } from 'http'
const host: string = process.env.HOST || 'localhost'
const port: number = Number(process.env.PORT) || 3000

const server = createServer((req, res) => {
  res.writeHead(200, { 'content-type' : 'application/json' })

  if(req.url === '/ping' && req.method === 'GET') 
    res.write(JSON.stringify({ status: 'Ok' }))
  else 
    res.write(JSON.stringify({ status: 'Route not found' }))
  
  res.end()
})

server.listen(port, host, () => {
  console.log(`Server listens on ${host}:${port}`)
})