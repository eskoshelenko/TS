import express from 'express'
import { StatusCodes, ReasonPhrases } from 'http-status-codes'

import logger from './midleware/loginService'
import notifier from './midleware/notifierService'
import auth from './midleware/authService'

const app = express()

const host: string = process.env.HOST || 'localhost'
const port: number = Number(process.env.PORT) || 3000

// midleware
app.use(logger)
app.use(notifier)

app.get('/ping', (req, res) => {
  res.statusCode = StatusCodes.OK
  res.json({ status: ReasonPhrases.OK })
})

app.use(auth)

app.get('/auth', (req, res) => {
  res.statusCode = StatusCodes.ACCEPTED
  res.json({ status: 'Authorization ' + ReasonPhrases.ACCEPTED })
})

app.use((req, res, next) => {
  res.statusCode = StatusCodes.NOT_FOUND
  res.json({ status: ReasonPhrases.NOT_FOUND })
  next()
})

app.listen(port, host, () => {
  console.log(`Server up and listen on: ${host}:${port}`)
})