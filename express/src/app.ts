import express from 'express'
import exphbs from 'express-handlebars'
import { StatusCodes, ReasonPhrases } from 'http-status-codes'
import { join } from 'path'

import logger from './midleware/loginService'
import notifier from './midleware/notifierService'
import auth from './midleware/authService'
import err from './midleware/errorService'

const app = express()

const host: string = process.env.HOST || 'localhost'
const port: number = Number(process.env.PORT) || 3000

app.engine('handlebars', exphbs({
  defaultLayout: 'main.handlebars',
  layoutDir: join(__dirname, '../views/layouts')
}))

// midleware
app.use(logger)
app.use(notifier)
app.use('/static', express.static(join(__dirname, '../public')))

app.get('/ping', (req, res) => {
  res.statusCode = StatusCodes.OK
  res.json({ status: ReasonPhrases.OK })
})

app.use((req, res, next) => {
  next(JSON.stringify({ 
    msg: ReasonPhrases.NOT_FOUND,  
    status: ReasonPhrases.NOT_FOUND 
  }))
})

app.use(err)

module.exports = app