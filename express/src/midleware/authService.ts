import { StatusCodes, ReasonPhrases } from 'http-status-codes'

export default (req: any, res: any, next: () => any) => {
  if (req.headers.auth === 'usr') {
    // res.setHeader('auth', 'usr')
    next()
  }

  res.statusCode = StatusCodes.FORBIDDEN
  res.json({status: ReasonPhrases.FORBIDDEN})
  next()
}