import { StatusCodes, ReasonPhrases } from 'http-status-codes'

export default (err: any, req: any, res: any, next: (...args: any[]) => any) => {
  process.stdout.write(err.message)
  res.statusCode = err.msg ? 
    StatusCodes.NOT_FOUND :
    StatusCodes.INTERNAL_SERVER_ERROR
  res.json({
    status: err.status ? 
      err.status :
      ReasonPhrases.INTERNAL_SERVER_ERROR
  })
}