"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const http_status_codes_1 = require("http-status-codes");
const path_1 = require("path");
const loginService_1 = __importDefault(require("./midleware/loginService"));
const notifierService_1 = __importDefault(require("./midleware/notifierService"));
const errorService_1 = __importDefault(require("./midleware/errorService"));
const app = (0, express_1.default)();
const host = process.env.HOST || 'localhost';
const port = Number(process.env.PORT) || 3000;
// midleware
app.use(loginService_1.default);
app.use(notifierService_1.default);
app.use('/static', express_1.default.static((0, path_1.join)(__dirname, '../public')));
app.get('/ping', (req, res) => {
    res.statusCode = http_status_codes_1.StatusCodes.OK;
    res.json({ status: http_status_codes_1.ReasonPhrases.OK });
});
app.use((req, res, next) => {
    next(JSON.stringify({
        msg: http_status_codes_1.ReasonPhrases.NOT_FOUND,
        status: http_status_codes_1.ReasonPhrases.NOT_FOUND
    }));
});
app.use(errorService_1.default);
module.exports = app;
