"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_codes_1 = require("http-status-codes");
exports.default = (err, req, res, next) => {
    process.stdout.write(err.message);
    res.statusCode = err.msg ?
        http_status_codes_1.StatusCodes.NOT_FOUND :
        http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
    res.json({
        status: err.status ?
            err.status :
            http_status_codes_1.ReasonPhrases.INTERNAL_SERVER_ERROR
    });
};
