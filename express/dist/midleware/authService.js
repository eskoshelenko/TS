"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_codes_1 = require("http-status-codes");
exports.default = (req, res, next) => {
    if (req.headers.auth === 'usr') {
        // res.setHeader('auth', 'usr')
        next();
    }
    res.statusCode = http_status_codes_1.StatusCodes.FORBIDDEN;
    res.json({ status: http_status_codes_1.ReasonPhrases.FORBIDDEN });
    next();
};
