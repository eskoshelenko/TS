"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const http_status_codes_1 = require("http-status-codes");
const loginService_1 = __importDefault(require("./midleware/loginService"));
const notifierService_1 = __importDefault(require("./midleware/notifierService"));
const authService_1 = __importDefault(require("./midleware/authService"));
const app = (0, express_1.default)();
const host = process.env.HOST || 'localhost';
const port = Number(process.env.PORT) || 3000;
// midleware
app.use(loginService_1.default);
app.use(notifierService_1.default);
app.get('/ping', (req, res) => {
    res.statusCode = http_status_codes_1.StatusCodes.OK;
    res.json({ status: http_status_codes_1.ReasonPhrases.OK });
});
app.use(authService_1.default);
app.get('/auth', (req, res) => {
    res.statusCode = http_status_codes_1.StatusCodes.ACCEPTED;
    res.json({ status: 'Authorization ' + http_status_codes_1.ReasonPhrases.ACCEPTED });
});
app.use((req, res, next) => {
    res.statusCode = http_status_codes_1.StatusCodes.NOT_FOUND;
    res.json({ status: http_status_codes_1.ReasonPhrases.NOT_FOUND });
    next();
});
app.listen(port, host, () => {
    console.log(`Server up and listen on: ${host}:${port}`);
});
