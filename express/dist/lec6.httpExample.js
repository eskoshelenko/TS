"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = require("http");
const host = process.env.HOST || 'localhost';
const port = Number(process.env.PORT) || 3000;
const server = (0, http_1.createServer)((req, res) => {
    res.writeHead(200, { 'content-type': 'application/json' });
    if (req.url === '/ping' && req.method === 'GET')
        res.write(JSON.stringify({ status: 'Ok' }));
    else
        res.write(JSON.stringify({ status: 'Route not found' }));
    res.end();
});
server.listen(port, host, () => {
    console.log(`Server listens on ${host}:${port}`);
});
