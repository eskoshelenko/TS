"use strict";
var JS02;
(function (JS02) {
    function sum(a, b) {
        return a + b;
    }
    function cube(a) {
        return a * a * a;
    }
    const arrowSum = (a, b) => a + b;
    const arrowCube = (a) => {
        return a * a * a;
    };
    // context
})(JS02 || (JS02 = {}));
// const arrowLog = (): void => console.log(this)
// function log(): void {
//   console.log(this)
// }
const person = {
    name: 'SomeName',
    age: 20,
    // log: log,
    // arrowLog: arrowLog,
    delayLog() {
        const self = this; // to bind this
        setTimeout(function () {
            console.log(self.name + ' ' + self.age);
        }, 500);
    },
    delayLogBind() {
        setTimeout(() => {
            console.log(this.name + ' ' + this.age);
        }, 1000);
    }
};
// person.log() // returns object person
// person.arrowLog() // binds context when created current - global obj
person.delayLog();
person.delayLogBind();
