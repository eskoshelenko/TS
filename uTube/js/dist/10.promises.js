"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var JS10;
(function (JS10) {
    const promise = new Promise((resolve, reject) => {
        const isResolved = Math.random() < 0.5;
        const result = isResolved ? resolve : reject;
        const text = isResolved ? 'Get data after delay' : 'Oups error appears';
        setTimeout(result, 500, text);
    });
    promise
        .then(data => console.log('Data: ', data))
        .catch(e => console.log('Error: ', e))
        .finally(() => console.log('Ending'));
    function asyncDelay() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield promise;
            return data;
        });
    }
    asyncDelay()
        .then(data => console.log('Data: ', data))
        .catch(e => console.log('Error: ', e))
        .finally(() => console.log('Ending'));
    // Promise.all([]) => arr data or err
    // Promise.race([]) => 1st resolved promise data
})(JS10 || (JS10 = {}));
