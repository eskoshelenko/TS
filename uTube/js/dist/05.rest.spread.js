"use strict";
var JS05;
(function (JS05) {
    // Rest
    function average(...args) {
        return args.reduce((acc, el) => acc += el) / args.length;
    }
    const arr = [1, 2, 3, 5, 8, 13];
    // Spread
    console.log(average(...arr));
    // Math.max.apply(null, arr) in earliest versions js
    const fib = [1, ...arr];
    console.log(fib);
    // Destructuring
    const obj = {
        name: 'SomeName',
        surname: 'SomeSurname',
        desc: {
            smth: 'New'
        }
    };
    const { name, surname, desc: { smth } } = obj;
    const [first, second, third] = arr;
    console.log(smth);
})(JS05 || (JS05 = {}));
