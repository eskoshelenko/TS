"use strict";
var JS04;
(function (JS04) {
    const title = 'Some title!';
    const isVisible = () => Math.random() > 0.5;
    const template = `
    ${isVisible() ? `<p>Lorem</p>` : ''}
    <h1 id='demo' style="color: red">${title}</h1>
  `;
    console.log(template);
    const str = 'Hello!';
    console.log(str.startsWith('He'));
    console.log(str.endsWith('!'));
    console.log(str.includes('llo'));
    console.log(str.repeat(2));
    console.log(str.trim());
    console.log(str.padStart(10, '1234'));
    console.log(str.padEnd(8, '4321'));
})(JS04 || (JS04 = {}));
