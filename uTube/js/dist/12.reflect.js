"use strict";
var JS12;
(function (JS12) {
    class Student {
        constructor(name) {
            this.name = name;
        }
        greet() {
            console.log(`Hello, my name is ${this.name}`);
        }
    }
    class ProtoStudent {
        constructor() {
            this.university = 'Oksford';
        }
    }
    // const student = new Student('Vasia')
    const student = Reflect.construct(Student, ['Ivan']); // 
    console.log(student);
    console.log(student.__proto === ProtoStudent.prototype);
    Reflect.apply(student.greet, { name: 'Nova' }, []);
    console.log(Reflect.ownKeys(student));
    // Reflect.preventExtensions(student)
    // student.age = 20
    // console.log(student)
})(JS12 || (JS12 = {}));
