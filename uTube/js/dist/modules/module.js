"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.compute = exports.color = void 0;
const privateVariable = 42;
exports.color = 'red';
function compute(a, b) {
    return a + b;
}
exports.compute = compute;
exports.default = {
    log() {
        console.log(privateVariable);
    }
};
