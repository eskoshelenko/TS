"use strict";
var JS06;
(function (JS06) {
    const cityField = 'cityField';
    const job = 'Frontend';
    const person = {
        name: 'someName',
        age: 40,
        'country-live': 'Zimbabue',
        [cityField]: 'Kanoha',
        job,
        print: () => 'Person',
        toString: function () {
            return Object
                .keys(this)
                .filter(key => key !== 'toString')
                .map(key => this[key])
                .join(' ');
        }
    };
    console.log(person.toString());
    console.log(person.print());
    const first = { a: 1 }, second = { b: 2 }, third = { c: 3 };
    console.log(Object.assign(first, second), first);
    const clonrObj = Object.assign({}, second, third);
    console.log(clonrObj, second);
    console.log(Object.entries(clonrObj));
    console.log(Object.keys(clonrObj));
    console.log(Object.values(clonrObj));
})(JS06 || (JS06 = {}));
