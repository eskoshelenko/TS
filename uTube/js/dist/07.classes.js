"use strict";
var JS07;
(function (JS07) {
    class Person {
        constructor(name) {
            this.type = 'human';
            this.name = name;
        }
        greet() {
            console.log('Hello, ' + this.name);
        }
    }
    class Programmer extends Person {
        constructor(name, job) {
            super(name);
            this.name = name;
            this._job = job;
        }
        get job() {
            return this._job.toUpperCase();
        }
        set job(job) {
            job.length < 2 ?
                console.log('validation failed') :
                this._job = job;
        }
        greet() {
            super.greet();
            console.log('rewrited');
        }
    }
    const max = new Person('Max');
    console.log(max);
    max.greet();
    console.log(max.type);
    const frontend = new Programmer('dude', 'frontend');
    console.log(frontend);
    frontend.greet();
    frontend.job = '1';
    console.log(frontend.job);
    frontend.job = 'backend';
    console.log(frontend.job);
    // Static
    class Util {
        static average(...args) {
            return args.reduce((acc, el) => acc += el, 0) / args.length;
        }
    }
    console.log(Util.average(1, 2, 3, 4, 5, 6, 7, 8, 8));
})(JS07 || (JS07 = {}));
