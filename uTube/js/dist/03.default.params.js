"use strict";
var JS03;
(function (JS03) {
    const defaultA = 10;
    const defaultB = 30;
    const getDefault = (c) => c * 2;
    function compute(a = defaultA, b = defaultB, c = getDefault(10)) {
        return a + b + c;
    }
    console.log(compute(3));
})(JS03 || (JS03 = {}));
