"use strict";
var JS01;
(function (JS01) {
    let a = 24;
    if (true) {
        let a = 42;
    }
    console.log(a); // 24
    const COLOR = '#ffeebb';
    console.log(COLOR); //#ffeebb
    const arr = [1, 2, 3, 5, 8];
    console.log(arr); // [ 1, 2, 3, 5, 8 ]
    arr.push(13);
    console.log(arr); // [ 1, 2, 3, 5, 8, 13 ]
    const obj = {};
    obj.name = 'Igor';
    console.log(obj); // { name: 'Igor' }
})(JS01 || (JS01 = {}));
