namespace JS07 {
  class Person {
    public type: string = 'human'
    name: string

    constructor(name: string) {
      this.name = name
    }

    public greet (): void {
      console.log('Hello, ' + this.name)
    }
  }

  class Programmer extends Person {
    private _job: string

    constructor(
      public name: string,
      job: string
    ){
      super(name)
      this._job = job
    }

    get job(): string {
      return this._job.toUpperCase()
    }

    set job(job: string) {
      job.length < 2 ? 
        console.log('validation failed') : 
        this._job = job
    }

    public greet(): void {
      super.greet()
      console.log('rewrited')
    }
  }

  const max: Person = new Person('Max')
  console.log(max)
  max.greet()
  console.log(max.type)

  const frontend: Programmer = new Programmer('dude', 'frontend')

  console.log(frontend)
  frontend.greet()
  frontend.job = '1'
  console.log(frontend.job)
  frontend.job = 'backend'
  console.log(frontend.job)

  // Static
  class Util {
    static average(...args: number[]): number {
      return args.reduce((acc, el) => acc += el, 0) / args.length
    }
  }

  console.log(Util.average(1,2,3,4,5,6,7,8,8))
}