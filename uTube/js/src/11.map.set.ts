namespace JS11 {
  // ============= Map ================
  const map: Map<any, any> = new Map([
    ['key1', 1],
    ['key2', 2]
  ])

  console.log(map.get('key1'))
  map.set('key3', 3).set(NaN, 'Number')
  console.log(map)
  // map.clear()
  console.log(map.has(42))
  // map.size map.delete(key)
  console.log(map.keys())
  console.log(map.values())
  console.log(map.entries())

  // ============= Set ================
  const set: Set<number> = new Set([1,1,2,3,4,8,8,13,21])

  console.log(set)
  // set.add(val).delete(val).clear().size
  console.log(set.keys())
  console.log(set.values())
  console.log(set.entries())
}