namespace JS01 {
  type Key = string | number | symbol 
  interface Obj {
    [key: Key]: string    
  }
  
  let a: number = 24
  
  if(true) {
    let a:number = 42
  }
  
  console.log(a) // 24
  
  const COLOR: string = '#ffeebb'
  
  console.log(COLOR) //#ffeebb
  
  const arr: number[] = [1, 2, 3, 5, 8]
  
  console.log(arr) // [ 1, 2, 3, 5, 8 ]
  arr.push(13)
  console.log(arr) // [ 1, 2, 3, 5, 8, 13 ]
  
  const obj: Obj = {}
  obj.name = 'Igor'
  
  console.log(obj) // { name: 'Igor' }
}