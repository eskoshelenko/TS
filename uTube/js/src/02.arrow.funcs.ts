namespace JS02 {
  function sum(a: number,b: number): number {
    return a + b
  }

  function cube (a: number): number {
    return a * a * a
  }

  const arrowSum = (a: number, b: number): number => a + b

  const arrowCube = (a: number):number => {
    return a * a * a
  }

  // context

  

  
}

// const arrowLog = (): void => console.log(this)

// function log(): void {
//   console.log(this)
// }

const person = {
  name: 'SomeName',
  age: 20,
  // log: log,
  // arrowLog: arrowLog,
  delayLog() {
    const self = this // to bind this

    setTimeout(function () {
      console.log(self.name + ' ' + self.age)
    }, 500)
  },

  delayLogBind() {
    setTimeout(() => {
      console.log(this.name + ' ' + this.age)
    }, 1000)
  }
}

// person.log() // returns object person
// person.arrowLog() // binds context when created current - global obj
person.delayLog()
person.delayLogBind()