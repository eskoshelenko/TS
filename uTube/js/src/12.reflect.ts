namespace JS12 {
  class Student {
    constructor(public name: string){}

    greet() {
      console.log(`Hello, my name is ${this.name}`)
    }
  }

  class ProtoStudent {
    university: string = 'Oksford'
  }

  // const student = new Student('Vasia')
  const student = Reflect.construct(Student, ['Ivan']) // 

  console.log(student)
  console.log(student.__proto === ProtoStudent.prototype)

  Reflect.apply(student.greet, { name: 'Nova'}, [])
  console.log(Reflect.ownKeys(student))

  // Reflect.preventExtensions(student)

  // student.age = 20

  // console.log(student)
}