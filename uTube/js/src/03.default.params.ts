namespace JS03 {
  const defaultA: number = 10
  const defaultB: number = 30
  const getDefault = (c:number): number => c * 2


  function compute(
    a:number = defaultA, 
    b:number = defaultB, 
    c:number = getDefault(10)
  ):number {
    return a + b + c
  }

  console.log(compute(3))
}