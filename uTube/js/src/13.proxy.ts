namespace JS13 {
  const validator = {
    get(target: {[key:string]:any}, prop: string) {
      return prop in target ? target[prop] : `this ${prop} doesn't exists`
    },
  
    set(target: {[key:string]:any}, prop: string, value: any) {
      if(value.length > 2) {
        Reflect.set(target, prop, value)
      }
      else {
        console.log('Too small value')
      }
    }
  }

  const form = {
    login: 'login',
    password: 'password'
  }

  const formProxy = new Proxy(form, validator)

  console.log(formProxy)
  console.log(formProxy.login)
  console.log(formProxy.password)
  console.log(formProxy.noProp)

  // formProxy.password = '12'

  function log(message: any) {
    console.log('Log: ', message)
  }

  const proxy = new Proxy(log, {
    apply(target, thisArg, argArray) {
      if(argArray.length === 1) {
        Reflect.apply(target, thisArg, argArray)
      }
      else {
        console.log('number of arguments not allowed')
      }
    }
  })

  proxy('qwewqe')
  proxy('qwe', 1)
}