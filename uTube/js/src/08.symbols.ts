namespace JS08 {
  const symbol1: symbol = Symbol("demo")
  const symbol2: symbol = Symbol("demo")

  console.log(symbol1)
  console.log(symbol2)
  console.log('Is symbols are equals? ', symbol1 === symbol2, symbol1 == symbol2)

  const obj = {
    name: 'Name',
    demo: 'Demo',
    [symbol1]: 'meta'
  }

  console.log(obj.name, obj[symbol1], obj[symbol2])

  // props with type symbol not iterable
  for (const key in obj) {
    console.log(key)
  }

  // on mdn docs can knew more about symbols
}