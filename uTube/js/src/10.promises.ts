namespace JS10 {  
  const promise: Promise<string> = new Promise((resolve, reject) => {
    const isResolved: boolean = Math.random() < 0.5
    const result = isResolved ?  resolve : reject
    const text = isResolved ? 'Get data after delay' : 'Oups error appears'

    setTimeout(result, 500, text)
  })

  promise
    .then(data => console.log('Data: ', data))
    .catch(e => console.log('Error: ', e))
    .finally(() => console.log('Ending'))

  async function asyncDelay() {
    const data = await promise
    
    return data
  }

  asyncDelay()
    .then(data => console.log('Data: ', data))
    .catch(e => console.log('Error: ', e))
    .finally(() => console.log('Ending'))

  // Promise.all([]) => arr data or err
  // Promise.race([]) => 1st resolved promise data
}