const privateVariable: number = 42

export const color: string = 'red'
export function compute (a: number, b: number): number {
  return a + b
}

export default {
  log(): void {
    console.log(privateVariable)
  }
}