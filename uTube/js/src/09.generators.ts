namespace JS09 {
  const arr: number[] = [1, 2, 3, 4, 5]
  const str: string = 'Hello'

  console.log(arr[Symbol.iterator])
  console.log(str[Symbol.iterator])

  const iter: IterableIterator<number> = arr[Symbol.iterator]()
  const iterStr: IterableIterator<string> = str[Symbol.iterator]()

  console.log(iter.next()) //{ value: 1, done: false }
  console.log(iter.next()) //{ value: 2, done: false }
  console.log(iter.next()) //{ value: 3, done: false }
  console.log(iter.next()) //{ value: 4, done: false }
  console.log(iter.next()) //{ value: 5, done: false }
  console.log(iter.next()) //{ value: undefined, done: true }

  console.log(iterStr.next()) //{ value: 'H', done: false }
  console.log(iterStr.next()) //{ value: 'e', done: false }
  console.log(iterStr.next()) //{ value: 'l', done: false }
  console.log(iterStr.next()) //{ value: 'l', done: false }
  console.log(iterStr.next()) //{ value: 'o', done: false }
  console.log(iterStr.next()) //{ value: undefined, done: true }

  for(let item of arr) console.log(item)
  for(let item of str) console.log(item)

  const country = {
    values: ['ru', 'kz', 'ua', 'by'],
    // can use to iterate random structure
    [Symbol.iterator]() {
      let i: number = 0

      return {
        next: () => {
          const value = this.values[i]

          i += 1

          return {
            value,
            done: i > this.values.length
          }
        }
      }
    }
  }

  for(let item of country) console.log(item)

  // Generator

  function *gen (num: number = 4) {
    for(let i = 0; i < num; i += 1) {
      try {
        yield i
      } catch (e) {
        console.log('Error: ', e)
      }
    }
  }

  const generator = gen(3)

  console.log(generator.next()) //{ value: 0, done: false }
  console.log(generator.throw('My Error')) //Error:  My Error
  console.log(generator.next()) //{ value: 2, done: false }
  console.log(generator.next()) //{ value: undefined, done: true }

  for(let i of gen(3)) console.log(i) // 0 1 2 
  
}