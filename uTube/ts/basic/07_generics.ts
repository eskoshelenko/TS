const arrOfNums: Array<number> = [1,2,3,4,5]
const arrOfStrs: string[] = ['1','2','3','4','5']

function reverse<T>(array: T[]): T[] {
  return array.reverse()
}

reverse(arrOfNums)
reverse(arrOfStrs)