function strip(x: string | number) {
  if(typeof x === 'string') {
    return x.trim()
  }

  return x.toFixed(2)
}

class MyResponse {
  header = 'response Header'
  result = 'response Result'
}

class MyError {
  header = 'error Header'
  message = 'error Message'
}

function handle(res: MyResponse | MyError) {
  if (res instanceof MyResponse) {
    return {
      info: res.header + res.result
    }
  }
  else {
    return {
      info: res.header + res.message
    }
  }
}

// ================================================
type AlertType = 'success' | 'danger' | 'warning'

function setAlertType(type: AlertType) {}

setAlertType('success')
setAlertType('warning')

// setAlertType('default') // error appears