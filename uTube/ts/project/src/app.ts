const msg: string = 'Hello world!'

class Person {
  constructor(private name: string) {}
}

const max = new Person('Max')

console.log(msg)
