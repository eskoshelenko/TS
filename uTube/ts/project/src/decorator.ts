// function Log(constructor: Function) {
//   console.log(constructor)
// }
// function Log2(target: any, propName: string | Symbol) {
//   console.log(target)
//   console.log(propName)
// }
// function Log3(target: any, propName: string | Symbol, discriptor: PropertyDescriptor) {
//   console.log(target)
//   console.log(propName)
//   console.log(discriptor)
// }


// @Log
// class Component {
//   @Log2
//   name: string

//   @Log3
//   get componentName() {
//     return this.name
//   }

//   constructor(name: string) {
//     this.name = name
//   }

//   @Log3
//   logName(): void {
//     console.log(`Component Name: ${this.name}`)
//   }
// }
interface ComponentDecorator {
  selector: string
  template: string
}

function Component(config: ComponentDecorator) {
  // return function(Constructor: Function) {
  return function
    <T extends {new(...args:any[]): object}>
  (Constructor: T) {
    return class extends Constructor {
      constructor(...args: any[]) {
        super(...args)

        const el = document.querySelector(config.selector)! // disable indicator when el can be null
        el.innerHTML = config.template
      }
    }
  }
}

function Bind(_: any, _2:any, descriptor: PropertyDescriptor) {
  const original = descriptor.value // original func will be in value prop of descriptor, when use decorator on methods

  return {
    configurable: true,
    enumerable: false,
    get() {
      return original.bind(this)
    }
  }
}

@Component({
  selector: '#card',
  template: `
    <div>
      <div>
        <span>Card Component</span>
      </div>
    </div>
  `
})
class CardComponent {
  constructor(public name: string) {
  }
  
  @Bind
  logName(): void {
    console.log(`Component Name: ${this.name}`)
  }
}

const card = new CardComponent('My Card Component')

const btn = document.querySelector('#btn')!

btn.addEventListener('click', card.logName)


// ====================================================
type ValidatorType = 'required' | 'email'
interface ValidatorConfig {
  [prop: string]: {
    [validateProp: string]: ValidatorType
  }
}

const validators: ValidatorConfig = {}

function Required(target: any, propName: string) {
  validators[target.constructor.name] = {
    ...validators[target.constructor.name],
    [propName]: 'required'
  }
}

function validate(obj: any): boolean {
  const objconfig = validators[obj.constructor.name]
  let isValid = true

  // 1st step check is input obj locates in validators
  if(!objconfig) {
    return true
  }

  // 2nd step check undefined props, toogle flag to false
  Object.keys(objconfig).forEach(key => {
    if(objconfig[key] === 'required') {
      isValid = isValid && obj[key]
    }
  })
  
  return isValid 
}
class Form {
  @Required
  public email: string | void

  constructor(email?: string) {
    this.email = email
  }
}

const form = new Form('somemail@gmail.com')
if(validate(form)) {
  console.log('Valid: ', form)
}
else {
  console.log('Validation error!')
}
