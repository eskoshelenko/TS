// const cars: string[] = ['BMW', 'Audi']
// const cars2: Array<string> = ['Lada', "Mersedes"]

// const promise = new Promise<string> (res => {
//   setTimeout(() => {
//     res('Promise resolved')
//   }, 2000);
// })

// promise.then(data => {
//   console.log(data)
// })

// in this case problens to access to properties
// function mergeObjects(a: object, b: object) {
//   return Object.assign({}, a, b)
// }

// console.log(mergeObjects({name: 'SomeName'}, {age:40}))

// use generics
function mergeObjects<T extends object, R extends object>(a: T, b: R) {
  return Object.assign({}, a, b)
}

console.log(mergeObjects({name: 'SomeName'}, {age:40}))

// ======================================
interface ILength {
  length: number
}
function widthCount<T extends ILength>(value: T): {value: T, count: string} {
  return {
    value,
    count: `В этом объекте ${value.length} символов`
  }
}

console.log(widthCount('Ровно15символов'))

// =======================================

// function getObjectValue(obj: object, key: string) {
//   return obj[key]
// }

function getObjectValue<T extends object, R extends keyof T>(obj: T, key: R) {
  return obj[key]
}

const person = {
  name: 'SomeName',
  age: 45,
  job: 'Javascript'
}

console.log(getObjectValue(person, 'name'))
console.log(getObjectValue(person, 'age'))
console.log(getObjectValue(person, 'job'))

// ===================
// class Collection<T extends number | string | boolean> {
class Collection<T> {
  // private _items: T[] = []
  constructor(private _items: T[] = []) {}

  add(item: T) {
    this._items.push(item)
  }

  remove(item: T) {
    this._items = this._items.filter(i => i !== item)
  }

  get items(): T[] {
    return this._items
  }
}

const strings = new Collection(['I', 'am', 'Strings'])
strings.add('!')
strings.remove('am')
console.log(strings.items)

// const objs = new Collection([{a: 1}, {b: 2}])
// objs.add({c: 3})
// objs.remove({b: 2})
// console.log(objs.items)

// ====================================

interface Car {
  model: string
  year: number
}

function createValidateCar(model: string, year: number): Car {
  const car: Partial<Car> = {}

  if(model.length > 3) {
    car.model = model
  }

  if(year > 2000) {
    car.year = year
  }

  return car as Car
}

///

const cars1: Array<string> = ['Ford', 'Audi']
cars1.shift() // works

const cars2: Readonly<Array<string>> = ['Ford', 'Audi']
//cars2.shift() // doesn't works

const ford: Car = {
  model: 'Ford',
  year: 2020
}

ford.model = 'Ferrari' // works

const bmw: Readonly<Car> = {
  model: 'BMW',
  year: 2022
}

// bmw.model = 'Lada' // doesn't works