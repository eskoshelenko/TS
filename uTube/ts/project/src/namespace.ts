/// <reference path="form-namespace.ts" />

namespace FormNamespace {
  class MyForm {
    private type: FormType = 'inline'
    private state: FormState = 'active'
  
    constructor(public email: string) {}
  
    getInfo(): FormInfo {
      return {
        type: this.type,
        state: this.state
      }
    }
  }

  export const form1 = new MyForm('t@gmail.com')  
}

console.log(FormNamespace.form1)



