"use strict";
var FormNamespace;
(function (FormNamespace) {
    class MyForm {
        constructor(email) {
            this.email = email;
            this.type = 'inline';
            this.state = 'active';
        }
        getInfo() {
            return {
                type: this.type,
                state: this.state
            };
        }
    }
    FormNamespace.form1 = new MyForm('t@gmail.com');
})(FormNamespace || (FormNamespace = {}));
console.log(FormNamespace.form1);
//# sourceMappingURL=namespace.js.map