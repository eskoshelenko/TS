"use strict";
function mergeObjects(a, b) {
    return Object.assign({}, a, b);
}
console.log(mergeObjects({ name: 'SomeName' }, { age: 40 }));
function widthCount(value) {
    return {
        value,
        count: `В этом объекте ${value.length} символов`
    };
}
console.log(widthCount('Ровно15символов'));
function getObjectValue(obj, key) {
    return obj[key];
}
const person = {
    name: 'SomeName',
    age: 45,
    job: 'Javascript'
};
console.log(getObjectValue(person, 'name'));
console.log(getObjectValue(person, 'age'));
console.log(getObjectValue(person, 'job'));
class Collection {
    constructor(_items = []) {
        this._items = _items;
    }
    add(item) {
        this._items.push(item);
    }
    remove(item) {
        this._items = this._items.filter(i => i !== item);
    }
    get items() {
        return this._items;
    }
}
const strings = new Collection(['I', 'am', 'Strings']);
strings.add('!');
strings.remove('am');
console.log(strings.items);
function createValidateCar(model, year) {
    const car = {};
    if (model.length > 3) {
        car.model = model;
    }
    if (year > 2000) {
        car.year = year;
    }
    return car;
}
const cars1 = ['Ford', 'Audi'];
cars1.shift();
const cars2 = ['Ford', 'Audi'];
const ford = {
    model: 'Ford',
    year: 2020
};
ford.model = 'Ferrari';
const bmw = {
    model: 'BMW',
    year: 2022
};
//# sourceMappingURL=generic.js.map