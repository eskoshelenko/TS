// tsconfig.json
target - in what standard compile
rootDir - what dir shuld compile
outDir - where save compile files
lib - helps to add different modules in project
["dom", "scripthost", "domiterable", "es6"]
jsx - react option
sourceMap - helps to send your ts file in browser
removeComments - del all comments by when compiling
noEmitOnError - helps to prevent compiling when error appears

strict - turn on strict modules
noImplicitAny - error appears when no types of arguments
strictNullChecks - shows potentially null values and throw err
noUnusedLocals - shows unused vars
noUnusedParameters - shows unused params in func
noImplicitReturns - err when do not tell return val

experimentalDecorators - allow using decorators

include, exclude, files - manual manipulating, what files shuld to compile

// run compiling
tsc

// run watch
tsc -w

// initializationg tsconfig
tsc --init