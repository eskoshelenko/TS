"use strict";
var Tasks;
(function (Tasks_1) {
    class Tasks {
        task1() {
            function isCharUniq(str) {
                const strArr = str.split('');
                const setArr = [...new Set(strArr)];
                return strArr.length === setArr.length;
            }
            console.log(isCharUniq('abcdef'), isCharUniq('1234567'), isCharUniq('abcABC'), isCharUniq('abcadef'));
        }
        task2() {
            function flatten(arr) {
                return arr.reduce((acc, el) => acc.concat(Array.isArray(el) ? flatten(el) : el), []);
            }
            console.log(flatten([[1], [[2, 3]], [[[[4]]]]]));
        }
        task3() {
            function noDublications(str) {
                let newStr = '';
                for (let i = str.length; i; i -= 1) {
                    const idx = i - 1;
                    if (!str.slice(0, idx).includes(str[idx])) {
                        newStr = str[idx] + newStr;
                    }
                    else
                        continue;
                }
                return newStr;
            }
            console.log(noDublications('abcd'), noDublications('aabbcccdd'), noDublications('abcddbca'), noDublications('abababcdcdcd'));
        }
        task4() {
            function highestFrequency(arr) {
                const map = new Map();
                arr.forEach(el => map.set(el, map.has(el) ? map.get(el) + 1 : 1));
                return Array.from(map)
                    .sort(([_1, prev], [_2, next]) => next - prev)[0][0];
            }
            console.log(highestFrequency(['a', 'b', 'c', 'c', 'd', 'e']), highestFrequency(['abc', 'def', 'abc', 'def', 'abc']), highestFrequency(['abc', 'def']), highestFrequency(['abc', 'abc', 'def', 'def', 'def', 'ghi', 'ghi', 'ghi']));
        }
        task5() {
            function isReversed(str1, str2) {
                const normalizeAtr1 = str1.split('').sort().join('');
                const normalizeAtr2 = str2.split('').sort().join('');
                return normalizeAtr1 === normalizeAtr2;
            }
            console.log(isReversed('javascript', 'scriptjava'), isReversed('javascript', 'iptjavascr'), isReversed('javascript', 'java'));
        }
        task6() {
            function arrSubset(arr1, arr2) {
                let result = true;
                arr2.forEach(el => {
                    const idx = arr1.indexOf(el);
                    if (idx === -1)
                        result = false;
                    delete arr1[idx];
                });
                return result;
            }
            console.log(arrSubset([1, 2, 3], [3, 2, 1]), arrSubset([2, 1, 1, 3], [3, 2, 1]), arrSubset([1, 1, 1, 3], [1, 3, 3]), arrSubset([1, 2], [1, 2, 3]));
        }
        task7() {
            function allAnagrams(arr) {
                const anagram = arr[0].split('').sort().join('');
                return arr
                    .every(el => el.split('').sort().join('') === anagram);
            }
            console.log(allAnagrams(['abcd', 'bdac', 'cabd']), allAnagrams(['abcd', 'bdXc', 'cabd']));
        }
        task8() {
            function rotate(arr) {
                const result = [];
                arr.forEach((subArr) => {
                    subArr.forEach((el, idx) => {
                        result[idx] ?
                            result[idx].push(el) :
                            result[idx] = [el];
                    });
                });
                return result.map(el => el.reverse());
            }
            function rotateBy(arr, deg) {
                let rotates = deg / 90;
                let result = arr;
                while (rotates) {
                    result = rotate(result);
                    rotates--;
                }
                return result;
            }
            const matrix = [
                [1, 2, 3],
                [4, 5, 6],
                [7, 8, 9]
            ];
            console.log(rotate(matrix), rotateBy(matrix, 360));
        }
        task9() {
            function search(arr, target) {
                let start = 0, end = arr.length - 1;
                if (target < arr[start] || target > arr[end]) {
                    return -1;
                }
                while (true) {
                    if (target === arr[start]) {
                        return start;
                    }
                    if (target === arr[end]) {
                        return end;
                    }
                    if (end - start <= 1) {
                        return -1;
                    }
                    const middle = Math.floor((start + end) / 2);
                    if (target > arr[middle]) {
                        start = middle + 1;
                    }
                    else if (target < arr[middle]) {
                        end = middle - 1;
                    }
                    else {
                        return middle;
                    }
                }
            }
            console.log(search([1, 3, 6, 13, 17], 13), search([1, 3, 6, 13, 17], 12));
        }
        task10() {
        }
        task11() {
            class Queue {
                storage = {};
                last = 0;
                first = 0;
                constructor() { }
                enqueue(item) {
                    this.storage[this.last] = item;
                    this.last++;
                }
                dequeue() {
                    if (!this.size)
                        return;
                    const value = this.storage[this.first];
                    delete this.storage[this.first];
                    this.first++;
                    return value;
                }
                get size() {
                    return this.last - this.first;
                }
            }
            class LinkedList {
                length = 0;
                head;
                tail;
                addToTail(value) {
                    const node = {
                        value,
                        next: null
                    };
                    if (!this.length) {
                        this.head = node;
                        this.tail = node;
                    }
                    else {
                        this.tail = node;
                    }
                }
                removeFromHead() {
                    if (!this.length)
                        return;
                    const value = this.head.value;
                    this.head = this.head.next;
                    this.length--;
                    return value;
                }
                _size() {
                    return this.length;
                }
            }
            class List extends LinkedList {
                enqueue;
                dequeue;
                constructor() {
                    super();
                    this.enqueue = this.addToTail;
                    this.dequeue = this.removeFromHead;
                }
                get size() {
                    return super._size();
                }
            }
        }
        task12() {
            function deepEqual(el1, el2) {
                if (isNaN(el1) && isNaN(el2))
                    return true;
                if (typeof el1 !== typeof el2) {
                    return false;
                }
                if (typeof el1 !== 'object' || el1 === null || el2 === null) {
                    return el1 === el2;
                }
                if (Object.keys(el1).length !== Object.keys(el2).length) {
                    return false;
                }
                for (const key of Object.keys(el1)) {
                    if (!deepEqual(el1[key], el2[key])) {
                        return false;
                    }
                }
                return true;
            }
            const source = { a: 1, b: { c: 1 } };
            const test1 = { a: 1, b: { c: 1 } };
            const test2 = { a: 1, b: { c: 2 } };
            console.log(deepEqual(source, test1), deepEqual(source, test2), deepEqual(NaN, NaN), deepEqual('test', 'test!'), deepEqual());
        }
        task13() {
            function fibonacci(n) {
                const oneArr = (n) => {
                    const result = [];
                    while (n) {
                        result.push(1);
                        n--;
                    }
                    return result;
                };
                if (n <= 2)
                    return oneArr(n);
                let result = oneArr(2);
                while (n > 2) {
                    const length = result.length;
                    result.push(result[length - 1] + result[length - 2]);
                    n--;
                }
                return result;
            }
            const cachedFibonacci = (function () {
                const seq = [1, 1];
                return function (n) {
                    if (seq.length >= n) {
                        return seq.slice(0, n);
                    }
                    while (seq.length < n) {
                        seq.push(seq[seq.length - 1] + seq[seq.length - 2]);
                    }
                    return seq;
                };
            })();
            console.log(fibonacci(8));
        }
        task14() {
            Function.prototype.myBind = function (context, ...args) {
                return (...rest) => {
                    return this.call(context, ...args.concat(rest));
                };
            };
        }
        execute() {
            this.task1();
            this.task2();
            this.task3();
            this.task4();
            this.task5();
            this.task6();
            this.task7();
            this.task8();
            this.task9();
            this.task10();
            this.task12();
            this.task13();
        }
    }
    const execute = new Tasks().execute();
})(Tasks || (Tasks = {}));
