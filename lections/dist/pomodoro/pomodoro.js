"use strict";
const min = document.getElementById('min');
const sec = document.getElementById('sec');
const audio = document.querySelector('audio');
const element = document.querySelector('.pomodoro');
const pomodoroDuration = 1000 * 60 * 0.5;
const shortBreakDuration = 1000 * 60 * 0.2;
let intervalId, pomodoro, shortBreak, current;
function currentTimestamp() {
    return new Date().getTime();
}
function formatOutput(num) {
    return num < 10 ? `0${num}` : `${num}`;
}
function tick(period) {
    let diff = period - currentTimestamp();
    if (diff <= 0) {
        audio.play();
        clearInterval(intervalId);
        next();
        return;
    }
    const minutes = Math.floor(diff % (1000 * 60 * 60) / (1000 * 60));
    const seconds = Math.floor(diff % (1000 * 60) / 1000);
    min.innerHTML = formatOutput(minutes);
    sec.innerHTML = formatOutput(seconds);
}
function next() {
    switch (current) {
        case 'pomodoro':
            shortBreak = currentTimestamp() + shortBreakDuration;
            tick(shortBreak);
            intervalId = setInterval(tick, 1000, shortBreak);
            current = 'short';
            element.classList.remove('bg_danger');
            element.classList.add('bg-warning');
            break;
        case 'short':
            pomodoro = currentTimestamp() + pomodoroDuration;
            tick(pomodoro);
            intervalId = setInterval(tick, 1000, pomodoro);
            current = 'pomodoro';
            element.classList.remove('bg-warning');
            element.classList.add('bg-danger');
            break;
    }
}
pomodoro = currentTimestamp() + pomodoroDuration;
current = 'pomodoro';
intervalId = setInterval(tick, 1000, pomodoro);
