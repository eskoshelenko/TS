"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const candidates_1 = require("./candidates");
function log(...args) {
    console.log(...args);
}
function delay(cb, sec) {
    setTimeout(cb, sec * 1000);
}
Function.prototype.log = function (...args) {
    const result = this.call(this, ...args);
    console.log(result);
    return result;
};
var HW01;
(function (HW01_1) {
    const optionsHW01 = {
        length: 10,
        factorial: 10,
        week: 8,
        room: 150,
        num: 8
    };
    class HW01 {
        static round(num) {
            const [integer, fractional] = num
                .toString().split('.').map(el => Number(el));
            return fractional ? integer + 1 : integer;
        }
        task1(length) {
            let count = 0;
            while (count < length) {
                let result;
                if (count % 3 === 0)
                    result = 'FizBuz';
                else if (count % 2)
                    result = 'Buz';
                else
                    result = 'Fiz';
                log(result);
                count++;
            }
        }
        task2(num) {
            let result = 1;
            while (num) {
                result *= num;
                num--;
            }
            return result;
        }
        task3(weeks) {
            return HW01.round(weeks * 1200 / 500);
        }
        task4(room) {
            const enterance = HW01.round(room / 3 / 9);
            const floor = HW01.round(room / enterance / 3);
            log(enterance, floor);
        }
        task5(num) {
            const char1 = '-';
            const char2 = '#';
            for (let i = 0; i < num; i += 1) {
                const part1 = char1.repeat(num - i - 1);
                const part2 = char2.repeat(1 + 2 * i);
                log(part1 + part2 + part1);
            }
        }
        execute(obj) {
            const { length, factorial, week, room, num } = obj;
            this.task1(length);
            log(this.task2(factorial));
            log(this.task3(week));
            this.task4(room);
            this.task5(num);
        }
    }
    HW01_1.result = function () {
        return new HW01().execute(optionsHW01);
    };
})(HW01 || (HW01 = {}));
var HW02;
(function (HW02_1) {
    const capitals = {
        'Киев': 'Украина',
        'Нью-Йорк': 'США',
        'Амстердам': 'Нидерланды',
        'Берлин': 'Германия',
        'Париж': 'Франция',
        'Лиссабон': 'Португалия',
        'Вена': 'Австрия'
    };
    const namesOfDays = {
        ru: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
        en: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Satyrday', 'Sunday']
    };
    const optionsHW02 = {
        capitals,
        length: 6,
        langDay: {
            lang: 'en',
            day: 1
        },
        numsArr: [19, 5, 42, 2, 77],
        zeroOneArr: [1, 1, 1, 1]
    };
    class HW02 {
        task1(obj) {
            return Object.entries(obj).map(([key, value]) => `${key} - это ${value}`);
        }
        task2(length) {
            const result = [];
            for (let i = 0; i < length; i += 3) {
                result.push([i + 1, i + 2, i + 3]);
            }
            return result;
        }
        task3(lang, day) {
            const obj = Object.assign({}, namesOfDays);
            return obj[lang][day - 1];
        }
        task4(arr) {
            const cloneArr = [...arr];
            const length = arr.length;
            for (let i = 0; i < length - 1; i++) {
                for (let j = 0; j < length - 1 - i; j++) {
                    if (cloneArr[j + 1] < cloneArr[j]) {
                        let t = cloneArr[j + 1];
                        cloneArr[j + 1] = cloneArr[j];
                        cloneArr[j] = t;
                    }
                }
            }
            for (let i = 0; i < length - 1; i += 1) {
                for (let j = 1; j < length; j += 1) {
                    if (cloneArr[i] !== cloneArr[j]) {
                        return cloneArr[i] + cloneArr[j];
                    }
                }
            }
            return 'Values are same';
        }
        task5(arr) {
            const length = arr.length;
            let result = 0;
            arr.forEach((el, idx) => {
                result += el * 2 ** (length - 1 - idx);
            });
            return result;
        }
        execute(obj) {
            const { capitals, length, langDay: { lang, day }, numsArr, zeroOneArr } = obj;
            log(this.task1(capitals));
            log(this.task2(length));
            log(this.task3(lang, day));
            log(this.task4(numsArr));
            log(this.task5(zeroOneArr));
        }
    }
    HW02_1.result = function () {
        return new HW02().execute(optionsHW02);
    };
})(HW02 || (HW02 = {}));
var HW03;
(function (HW03_1) {
    class Employee {
        salary;
        workExperience;
        isPrivileges;
        gender;
        id;
        name;
        surname;
        constructor(id, name, surname, salary, workExperience, isPrivileges, gender) {
            this.salary = salary;
            this.workExperience = workExperience;
            this.isPrivileges = isPrivileges;
            this.gender = gender;
            this.id = id;
            this.name = name;
            this.surname = surname;
        }
    }
    class EmployeeWithFullName extends Employee {
        id;
        name;
        surname;
        salary;
        workExperience;
        isPrivileges;
        gender;
        constructor(id, name, surname, salary, workExperience, isPrivileges, gender) {
            super(id, name, surname, salary, workExperience, isPrivileges, gender);
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.salary = salary;
            this.workExperience = workExperience;
            this.isPrivileges = isPrivileges;
            this.gender = gender;
        }
        getFullName() {
            return `${this.surname} ${this.name}`;
        }
    }
    const emplyeeArr = [
        {
            id: 1,
            name: 'Денис',
            surname: 'Хрущ',
            salary: 1010,
            workExperience: 10,
            isPrivileges: false,
            gender: 'male'
        },
        {
            id: 2,
            name: 'Сергей',
            surname: 'Войлов',
            salary: 1200,
            workExperience: 12,
            isPrivileges: false,
            gender: 'male'
        },
        {
            id: 3,
            name: 'Татьяна',
            surname: 'Коваленко',
            salary: 480,
            workExperience: 3,
            isPrivileges: true,
            gender: 'female'
        },
        {
            id: 4,
            name: 'Анна',
            surname: 'Кугир',
            salary: 2430,
            workExperience: 20,
            isPrivileges: false,
            gender: 'female'
        },
        {
            id: 5,
            name: 'Татьяна',
            surname: 'Капустник',
            salary: 3150,
            workExperience: 30,
            isPrivileges: true,
            gender: 'female'
        },
        {
            id: 6,
            name: 'Станислав',
            surname: 'Щелоков',
            salary: 1730,
            workExperience: 15,
            isPrivileges: false,
            gender: 'male'
        },
        {
            id: 7,
            name: 'Денис',
            surname: 'Марченко',
            salary: 5730,
            workExperience: 45,
            isPrivileges: true,
            gender: 'male'
        },
        {
            id: 8,
            name: 'Максим',
            surname: 'Меженский',
            salary: 4190,
            workExperience: 39,
            isPrivileges: false,
            gender: 'male'
        },
        {
            id: 9,
            name: 'Антон',
            surname: 'Завадский',
            salary: 790,
            workExperience: 7,
            isPrivileges: false,
            gender: 'male'
        },
        {
            id: 10,
            name: 'Инна',
            surname: 'Скакунова',
            salary: 5260,
            workExperience: 49,
            isPrivileges: true,
            gender: 'female'
        },
        {
            id: 11,
            name: 'Игорь',
            surname: 'Куштым',
            salary: 300,
            workExperience: 1,
            isPrivileges: false,
            gender: 'male'
        },
    ];
    class HW03 {
        task1(obj) {
            const { id, name, surname, salary, workExperience, isPrivileges, gender } = obj;
            return new Employee(id, name, surname, salary, workExperience, isPrivileges, gender);
        }
        task2(obj) {
            const { id, name, surname, salary, workExperience, isPrivileges, gender } = obj;
            return new EmployeeWithFullName(id, name, surname, salary, workExperience, isPrivileges, gender);
        }
        task3(arr) {
            return arr.map(obj => this.task2(obj));
        }
        task4(arr) {
            return arr.map(employee => employee.getFullName());
        }
        task5(arr) {
            return arr.reduce((acc, { salary }) => acc + salary, 0) / arr.length;
        }
        task6(arr) {
            return arr[Math.floor(Math.random() * arr.length)];
        }
        task7(arr) {
            return arr[Math.floor(Math.random() * arr.length)];
        }
        task8(arr) {
            return arr[Math.floor(Math.random() * arr.length)];
        }
        execute() {
            const emplyeeConstructArr = this.task3(emplyeeArr);
            log(this.task1(emplyeeArr[0]));
            log(this.task2(emplyeeArr[1]).getFullName());
            log(emplyeeConstructArr);
            log(this.task4(emplyeeConstructArr));
            log(this.task5(emplyeeConstructArr));
            log(this.task6(emplyeeConstructArr));
        }
    }
    HW03_1.result = function () {
        return new HW03().execute();
    };
})(HW03 || (HW03 = {}));
var HW04;
(function (HW04_1) {
    const studentArr = [
        {
            name: 'Сергей',
            surname: 'Войлов',
            ratingPoint: 1000,
            schoolPoint: 1000,
            course: 2,
        },
        {
            name: 'Татьяна',
            surname: 'Коваленко',
            ratingPoint: 880,
            schoolPoint: 700,
            course: 1,
        },
        {
            name: 'Анна',
            surname: 'Кугир',
            ratingPoint: 1430,
            schoolPoint: 1200,
            course: 3,
        },
        {
            name: 'Станислав',
            surname: 'Щелоков',
            ratingPoint: 1130,
            schoolPoint: 1060,
            course: 2,
        },
        {
            name: 'Денис',
            surname: 'Хрущ',
            ratingPoint: 1000,
            schoolPoint: 990,
            course: 4,
        },
        {
            name: 'Татьяна',
            surname: 'Капустник',
            ratingPoint: 650,
            schoolPoint: 500,
            course: 3,
        },
        {
            name: 'Максим',
            surname: 'Меженский',
            ratingPoint: 990,
            schoolPoint: 1100,
            course: 1,
        },
        {
            name: 'Денис',
            surname: 'Марченко',
            ratingPoint: 570,
            schoolPoint: 1300,
            course: 4,
        },
        {
            name: 'Антон',
            surname: 'Завадский',
            ratingPoint: 1090,
            schoolPoint: 1010,
            course: 3
        },
        {
            name: 'Игорь',
            surname: 'Куштым',
            ratingPoint: 870,
            schoolPoint: 790,
            course: 1,
        },
        {
            name: 'Инна',
            surname: 'Скакунова',
            ratingPoint: 1560,
            schoolPoint: 200,
            course: 2,
        }
    ];
    class Student {
        static _id = 1;
        static listOfStudents = [];
        constructor(student) {
            this.addStudent({
                id: Student._id++,
                ...student,
                isSelfPayment: false
            });
        }
        addStudent(student) {
            Student.listOfStudents.push(student);
            Student.listOfStudents = Student.listOfStudents
                .sort((prev, next) => {
                let result = next.ratingPoint - prev.ratingPoint;
                if (result)
                    return result;
                else {
                    return next.schoolPoint - prev.schoolPoint;
                }
            })
                .map((student, idx) => {
                return {
                    ...student,
                    isSelfPayment: idx < 5 ? true : false
                };
            })
                .sort((prev, next) => prev.id - next.id);
        }
    }
    class CustomString {
        reverse(str) {
            let newStr = '';
            for (let i = str.length; i; i -= 1)
                newStr += str[i - 1];
            return newStr;
        }
        ucFirst(str) {
            return str[0].toUpperCase() + str.slice(1);
        }
        ucWords(str) {
            return str
                .split(' ')
                .map(word => this.ucFirst(word))
                .join(' ');
        }
    }
    class Validator {
        static email = /^[a-zA-z._]+@.\w{3,4}.\w{2,3}$/;
        static domain = /^[a-zA-z._]+.\w{2,3}$/;
        static date = /^\d{1,2}.\d{1,2}.\d{4}$/;
        static phone = /^[+]38\s[(]\d{3}[)]\s\d{3}-\d\d-\d\d$/;
        checkIsEmail(str) {
            return Validator.email.test(str);
        }
        checkIsDate(str) {
            return Validator.date.test(str);
        }
        checkIsDomain(str) {
            return Validator.domain.test(str);
        }
        checkIsPhone(str) {
            return Validator.phone.test(str);
        }
    }
    class HW04 {
        task01() {
            studentArr.forEach(student => new Student(student));
            log(Student.listOfStudents);
        }
        task02() {
            const myString = new CustomString();
            log(myString.reverse('qwerty'));
            log(myString.ucFirst('qwerty'));
            log(myString.ucWords('qwerty qwerty qwerty'));
        }
        task03() {
            const validator = new Validator();
            log(validator.checkIsEmail('vasya.pupkin@gmail.com'));
            log(validator.checkIsDomain('google.com'));
            log(validator.checkIsDate('30.11.2019'));
            log(validator.checkIsPhone('+38 (066) 937-99-92'));
        }
        execute() {
            this.task01();
            this.task02();
            this.task03();
        }
    }
    HW04_1.result = function () {
        return new HW04().execute();
    };
})(HW04 || (HW04 = {}));
var HW05;
(function (HW05_1) {
    class HW05 {
        task01() {
            function increaseBy() {
                let count = 0;
                return function (num) {
                    return count += num;
                };
            }
            let counter = increaseBy();
            counter(3);
            counter(5);
            log(counter(228));
            counter = null;
            return increaseBy();
        }
        task02() {
            function updateArr() {
                let arr = [];
                return function (data) {
                    data ? arr.push(data) : arr = [];
                    return arr;
                };
            }
            const getUpdatedArr = updateArr();
            getUpdatedArr(3);
            getUpdatedArr(5);
            log(getUpdatedArr({ name: 'Vasya' }));
            getUpdatedArr();
            log(getUpdatedArr(4));
        }
        task03() {
            function rememberTime() {
                let time;
                return function () {
                    const diffTime = Math.round((Date.now() - time) / 1000);
                    const result = time ? diffTime : 'Enabled';
                    time = Date.now();
                    return result;
                };
            }
            const getTime = rememberTime();
            const counter = this.task01();
            getTime.log();
            delay(() => getTime.log(), counter(2));
            delay(() => getTime.log(), counter(3));
            delay(() => getTime.log(), counter(7));
            delay(() => getTime.log(), counter(60));
            delay(() => getTime.log(), counter(1));
        }
        task04(time) {
            function timer(time) {
                const normilize = (time) => {
                    return time < 10 ? `0${time}` : `${time}`;
                };
                const interval = setInterval(() => {
                    const minutes = Math.floor(time / 60);
                    const min = normilize(minutes);
                    const sec = normilize(time - minutes * 60);
                    if (time) {
                        console.log(`${min}:${sec}`);
                        time--;
                    }
                    else {
                        console.log('Time End');
                        clearInterval(interval);
                    }
                }, 1000);
            }
            return timer(time);
        }
        execute() {
            this.task01();
            this.task02();
            this.task03();
            this.task04(30);
        }
    }
    HW05_1.result = function () {
        return new HW05().execute();
    };
})(HW05 || (HW05 = {}));
var HW06;
(function (HW06_1) {
    class Candidate {
        constructor(obj) {
            Object.keys(obj).forEach(key => this[key] = obj[key]);
        }
        get state() {
            return this.address.split(', ')[2];
        }
    }
    class HW06 {
        task01(arr, index) {
            const length = arr.length;
            for (let i = index; i < length - 1; i += 1) {
                arr[i] = arr[i + 1];
            }
        }
        task02(obj) {
            const newArr = [];
            for (let key in obj)
                newArr.push(key);
            return newArr;
        }
        task03(obj) {
            const newArr = [];
            for (let key in obj)
                newArr.push(obj[key]);
            return newArr;
        }
        task04() {
            const obj = {
                id: 3,
                name: 'Vasya'
            };
            const secondObj = {
                id: 4,
                name: 'Katya'
            };
            const arr = [
                {
                    id: 1,
                    name: 'Kolya'
                },
                {
                    id: 2,
                    name: 'Petya'
                }
            ];
            const insertIntoarr = (obj, candidateId) => {
                const length = arr.length;
                let idx = -1;
                for (let i = 0; i < length; i += 1) {
                    if (arr[i].id === candidateId) {
                        idx = i;
                        break;
                    }
                }
                if (idx >= 0) {
                    for (let i = length; i > idx; i -= 1) {
                        arr[i] = arr[i - 1];
                    }
                    arr[idx] = obj;
                }
            };
            insertIntoarr(obj, 2);
            log(arr);
            insertIntoarr(secondObj, 1);
            log(arr);
        }
        task05() {
            const condidate = new Candidate(candidates_1.condidateArr[0]);
            log(condidate.state);
        }
        task06() {
            function getCompanyNames(arr) {
                const obj = {};
                arr.forEach(({ company }) => obj[company] = true);
                return Object.keys(obj);
            }
            log(getCompanyNames(candidates_1.condidateArr));
        }
        task07() {
            Array.prototype.getUsersByYear = function (year) {
                return this
                    .filter(({ registered }) => {
                    const getDate = registered.split(' ')[0];
                    return year === new Date(getDate).getFullYear();
                })
                    .map(({ _id }) => _id);
            };
            log(candidates_1.condidateArr.getUsersByYear(2017));
        }
        task08() {
            Array.prototype.getCondidatesByUnreadMsg = function (messages) {
                return this.filter(({ greeting }) => {
                    return greeting.match(new RegExp(` ${messages} `));
                });
            };
            log(candidates_1.condidateArr.getCondidatesByUnreadMsg(8));
        }
        task09() {
            Array.prototype.getCondidatesByGender = function (genderStr) {
                return this.filter(({ gender }) => gender === genderStr);
            };
            log(candidates_1.condidateArr.getCondidatesByGender('male'));
        }
        task10() {
            function reduce(arr, cb, initialVal) {
                const length = arr.length;
                const start = initialVal ? 1 : 2;
                let result = initialVal ?
                    cb(initialVal, arr[0], 0, arr) :
                    cb(arr[0], arr[1], 1, arr);
                for (let i = start; i < length; i += 1) {
                    result = cb(result, arr[i], i, arr);
                }
                return result;
            }
            function forEach(cb, arr) {
                const length = arr.length;
                for (let i = 0; i < length; i += 0) {
                    cb(arr[i], i, arr);
                }
            }
            function map(cb, arr) {
                const length = arr.length;
                const result = [];
                for (let i = 0; i < length; i += 0) {
                    result.push(cb(arr[i], i, arr));
                }
                return result;
            }
            function find(cb, arr) {
                const length = arr.length;
                for (let i = 0; i < length; i += 0) {
                    if (cb(arr[i], i, arr))
                        return arr[i];
                }
                return undefined;
            }
            function findIndex(cb, arr) {
                const length = arr.length;
                for (let i = 0; i < length; i += 0) {
                    if (cb(arr[i], i, arr))
                        return i;
                }
                return -1;
            }
            function filter(cb, arr) {
                const length = arr.length;
                const result = [];
                for (let i = 0; i < length; i += 0) {
                    if (cb(arr[i], i, arr))
                        result.push(arr[i]);
                }
                return result;
            }
            function every(cb, arr) {
                const length = arr.length;
                for (let i = 0; i < length; i += 0) {
                    if (!cb(arr[i], i, arr))
                        return false;
                }
                return true;
            }
            function some(cb, arr) {
                const length = arr.length;
                for (let i = 0; i < length; i += 0) {
                    if (cb(arr[i], i, arr))
                        return true;
                }
                return false;
            }
            function reverse(arr) {
                const result = [];
                for (let i = length; i; i -= 1) {
                    result.push(arr[i - 1]);
                }
                return result;
            }
            log(reduce([1, 2, 3, 4, 5], (acc, el) => acc + el, 0));
        }
        execute() {
            this.task04();
            this.task05();
            this.task06();
            this.task07();
            this.task10();
        }
    }
    HW06_1.result = function () {
        return new HW06().execute();
    };
})(HW06 || (HW06 = {}));
var HW07;
(function (HW07_1) {
    class Candidate {
        constructor(obj) {
            Object.keys(obj).forEach(key => this[key] = obj[key]);
        }
        get state() {
            return this.address.split(', ')[2];
        }
    }
    class HW07 {
        task01() {
            Array.prototype.searchCandidatesByPhoneNumber = function (serchPhone) {
                return this.filter(({ phone }) => {
                    phone.match(new RegExp(serchPhone));
                });
            };
        }
        task02() {
            Array.prototype.getCandidateById = function (searchId) {
                let buff = '';
                const result = this.find(({ _id, registred }) => {
                    if (_id === searchId) {
                        const d = new Date(registred.split(' ')[0]);
                        buff = `${d.getDate()}/${d.getMonth()}/${d.getFullYear()}`;
                        return true;
                    }
                    return false;
                });
                return result ? { ...result, registred: buff } : undefined;
            };
        }
        task03() {
            Array.prototype.sortCandidatesArr = function (sortBy) {
                return this.sort((prev, next) => {
                    if (sortBy === 'asc')
                        return prev.ballance - next.ballance;
                    else if (sortBy === 'desc')
                        return next.ballance - prev.ballance;
                    else
                        return 0;
                });
            };
        }
        task04() {
            Array.prototype.getEyeColorMap = function () {
                const obj = {};
                this.forEach(candidate => {
                    obj[candidate.eyeColor] ?
                        obj[candidate.eyeColor].push(candidate) :
                        obj[candidate.eyeColor] = [candidate];
                });
                return obj;
            };
            console.log(candidates_1.condidateArr.getEyeColorMap());
        }
        execute() {
            this.task04();
        }
    }
    HW07_1.result = function () {
        return new HW07().execute();
    };
})(HW07 || (HW07 = {}));
var LC01;
(function (LC01) {
    class Wheel {
        tirePressure;
        constructor(tirePressure) {
            this.tirePressure = tirePressure;
        }
    }
    class Car {
        name;
        dateOfManufacture;
        wheels;
        constructor(name, dateOfManufacture) {
            this.name = name;
            this.dateOfManufacture = dateOfManufacture;
            this.wheels = {
                frontWheels: {
                    left: new Wheel(2),
                    right: new Wheel(2)
                },
                backWheels: {
                    left: new Wheel(2.5),
                    right: new Wheel(2.5)
                }
            };
        }
        getAge() {
            return new Date().getFullYear() - this.dateOfManufacture;
        }
    }
    const car = new Car('Audi', 2010);
    class Wheel1 {
        tirePressure;
        constructor(tirePressure) {
            this.tirePressure = tirePressure;
        }
    }
    class Car1 {
        name;
        dateOfManufacture;
        wheels;
        get color() {
            return this.color;
        }
        set color(value) {
            this.color = value;
        }
        constructor(name, dateOfManufacture, wheels) {
            this.name = name;
            this.dateOfManufacture = dateOfManufacture;
            this.wheels = wheels;
        }
        getAge() {
            return new Date().getFullYear() - this.dateOfManufacture;
        }
    }
    const wheels = {
        frontWheels: {
            left: new Wheel1(2),
            right: new Wheel1(2)
        },
        backWheels: {
            left: new Wheel1(2.5),
            right: new Wheel1(2.5)
        }
    };
    const car1 = new Car1('Audi', 2010, wheels);
    class Transport1 {
        name;
        passengersSeats;
        constructor(name, passengersSeats) {
            this.name = name;
            this.passengersSeats = passengersSeats;
        }
    }
    class Car2 extends Transport1 {
        mark;
        model;
        year;
        passengersSeats;
        constructor(mark, model, year, passengersSeats) {
            super('Car', passengersSeats);
            this.mark = mark;
            this.model = model;
            this.year = year;
            this.passengersSeats = passengersSeats;
        }
    }
    class Plane extends Transport1 {
        mark;
        model;
        maxHeight;
        passengersSeats;
        constructor(mark, model, maxHeight, passengersSeats) {
            super('Plane', passengersSeats);
            this.mark = mark;
            this.model = model;
            this.maxHeight = maxHeight;
            this.passengersSeats = passengersSeats;
        }
    }
    let carObj = new Car2('BMW', 'X5', 2020, 4);
    let planeObj = new Plane('Boeing', '787-9', 13100, 250);
    console.log(carObj);
    console.log(planeObj);
    let _promise = new Promise((resolve, reject) => {
        resolve('значение_при_успешном_выполнении');
        reject('значение_при_ошибке');
    });
    _promise
        .then(console.log)
        .catch(console.log)
        .finally(() => console.log('Always appears1'));
    let result = new Promise((resolve, reject) => {
        let value = Math.round(Math.random());
        setTimeout(() => {
            if (value)
                resolve(value);
            else
                reject(value);
        }, 1000);
    });
    result
        .then(result => console.log('Success: ', result))
        .catch(error => console.log('Error: ', error))
        .finally(() => console.log('JavaScript Promise finished'));
    let result1 = new Promise((resolve, reject) => {
        console.log('Promise executing');
        resolve('webdraftt');
    });
    result1.then(result => console.log('Result: ', result));
    setTimeout(() => result.then(result => console.log('Again: ', result)), 1000);
    let promise1 = new Promise((resolve, reject) => setTimeout(() => resolve(1), 1000));
    let promise2 = new Promise((resolve, reject) => setTimeout(() => resolve(2), 1500));
    let promise3 = new Promise((resolve, reject) => setTimeout(() => resolve(3), 500));
    Promise.all([promise1, promise2, promise3]).then(result => console.log(result));
    let promise4 = new Promise((resolve, reject) => setTimeout(() => resolve(1), 1000));
    let promise5 = new Promise((resolve, reject) => setTimeout(() => resolve(2), 1500));
    let promise6 = new Promise((resolve, reject) => setTimeout(() => reject(3), 500));
    Promise.allSettled([promise4, promise5, promise6]).then(result => console.log(result));
    let promise7 = new Promise((resolve, reject) => setTimeout(() => resolve(1), 1000));
    let promise8 = new Promise((resolve, reject) => setTimeout(() => resolve(2), 1500));
    let promise9 = new Promise((resolve, reject) => setTimeout(() => resolve(3), 500));
    Promise.race([promise7, promise8, promise9]).then(result => console.log(result));
    async function asyncFunc() {
        return new Promise((resolve, reject) => setTimeout(() => resolve(3), 500));
    }
    let asyncFuncExpression = async () => new Promise((resolve, reject) => setTimeout(() => resolve(3), 500));
    let _promise1 = new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(1);
            resolve(1);
        }, 3000);
    });
    async function asyncFunc1() {
        let value = await _promise1;
        console.log(2);
        return 3;
    }
    asyncFunc1().then(result => console.log(result));
    try {
    }
    catch (e) {
    }
    finally {
    }
    let obj, name;
    try {
        name = obj.name;
        alert(name);
    }
    catch (e) {
        name = 'Unknown';
        alert(name);
    }
    try {
        let result = division(3, 0);
    }
    catch (e) {
        console.log(e.name);
        console.log(e.message);
    }
    function division(dividend, divider) {
        if (divider == 0)
            throw new Error('Division by zero not allowed');
        else
            return dividend / divider;
    }
    class TimeError extends Error {
        whatWrong;
        name = 'Time error';
        constructor(message, whatWrong) {
            super(message);
            this.whatWrong = whatWrong;
        }
    }
    function checkTime(hours, minutes, seconds) {
        if (hours < 0 || hours > 23)
            throw new TimeError('Hours value must be between 0 and 23', 'hours');
        else if (minutes < 0 || minutes > 59)
            throw new TimeError('Minutes value must be between 0 and 59', 'minutes');
        else if (seconds < 0 || seconds > 59)
            throw new TimeError('Seconds value must be between 0 and 59', 'seconds');
        return true;
    }
    try {
        checkTime(12, 65, 7);
    }
    catch (e) {
        console.log(e.name);
        console.log(e.message);
        console.log(e.whatWrong);
    }
    LC01.weekDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    function printMessage(message) {
        alert(message);
    }
    LC01.printMessage = printMessage;
    let weekDays1 = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    function printMessage1(message) {
        alert(message);
    }
    let modulePath = './module';
    Promise.resolve().then(() => __importStar(require(modulePath))).then(m => m.printMessage(m.weekDays[1]))
        .catch(err => console.log(err));
})(LC01 || (LC01 = {}));
var LC02;
(function (LC02) {
    fetch('https://example.com/api/data')
        .then(response => console.log(response.json()))
        .catch(err => console.log(err));
    let body = { name: 'Jake' };
    fetch('https://example.com/api/data', {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
            'Content-Type': 'application/json'
        },
        mode: 'no-cors'
    })
        .then(response => console.log(response.json()))
        .catch(err => console.log(err));
    let fileUpload = document.getElementById('fileUpload');
    fileUpload.addEventListener('change', event => {
        let formData = new FormData();
        formData.append('file', fileUpload.files[0]);
        upload(formData);
    });
    function upload(formData) {
        fetch('https://example.com/api/file', {
            method: 'POST',
            body: formData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
            .then(async (response) => console.log(await response.json()))
            .catch(err => console.log(err));
    }
    fetch('https://example.com/api/data', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(async (response) => {
        console.log(response.bodyUsed);
        let result = await response.json();
        console.log(response.bodyUsed);
        return result;
    });
    let abortCtrl = new AbortController();
    let send = document.getElementById('send');
    let cancel = document.getElementById('cancel');
    send?.addEventListener('click', event => sendRequest());
    cancel?.addEventListener('click', event => abortCtrl.abort());
    function sendRequest() {
        fetch('https://example.com/api/data', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
            signal: abortCtrl.signal
        })
            .then(async (response) => console.log(await response.json))
            .catch(err => console.log(err));
    }
    window.navigator.geolocation.getCurrentPosition(data => console.log(data));
    window.history.length;
    window.history.back();
    window.history.forward();
    window.history.go(-2);
    window.history.go(1);
    window.history.pushState({ data: "JavaScript History" }, '', "https://naydyonovdanil.gitbook.io/javascript/moduli");
    window.history.state;
})(LC02 || (LC02 = {}));
