"use strict";
var LC02;
(function (LC02) {
    function someName(x, y) {
        console.log(`x = ${x}, y = ${y}`);
        return x + y;
    }
    const pow = function (num, pow) {
        console.log(`num = ${num}, pow = ${pow}`);
        return num ** pow;
    };
    const average = (...args) => {
        return args.reduce((result, el) => result += el, 0) / args.length;
    };
    const averageShort = (...args) => args
        .reduce((result, el) => result += el, 0) / args.length;
    function arrToFlat(arr) {
        return arr.reduce((acc, el) => acc.concat(Array.isArray(el) ? arrToFlat(el) : el), []);
    }
    function getBlock(height = 0, width = 0) {
        return `<div style"height:${height},width:${width}"></div>`;
    }
    const max = (...args) => Math.max(...args);
    function concat(arr1, arr2) {
        return [...arr1, ...arr2];
    }
    const greeting = ({ name, lastname }) => `Buenos Diaz, ${name} ${lastname}`;
    function makeCounter() {
        let count = 0;
        return function () {
            return count++;
        };
    }
    class Person {
        name;
        talk;
        constructor(name) {
            this.name = name;
            this.talk = function () {
                console.log(`Hi, I am ${this.name}`);
            };
        }
    }
    LC02.execute = function () {
        const arr1 = [1, 2, [3, 4, [5, 6, 7]], [8, 9]];
        const arr2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
        const user = {
            name: 'John',
            lastname: 'Konnor',
            age: 16,
            password: '1234'
        };
        console.log(someName(4, 5));
        console.log(pow(3, 4));
        (function sqrt(num) {
            const result = Math.sqrt(num);
            console.log(`Sqrt from ${num} is ${result}`);
            return result;
        })(9);
        console.log(arrToFlat(arr1));
        console.log(getBlock(), getBlock(10), getBlock(12, 11));
        console.log(concat(arr1, arr2));
        console.log(greeting(user));
        const he = {
            name: 'This name'
        };
        he.talk = function () {
            console.log(`Hi, I am ${this.name}`);
        };
        const anotherHe = {
            name: 'New name',
            talk() {
                return he.talk?.bind(this)();
            }
        };
        he.talk();
        he.talk.call(anotherHe);
        anotherHe.talk();
        const person = new Person('Ivan');
        console.log(person);
        person.talk();
        const counter1 = makeCounter();
        const counter2 = makeCounter();
        console.log(counter1(), counter2());
        console.log(counter1(), counter2());
        console.log(counter1());
        const obj = {
            a: 5,
            first() {
                this.a += 5;
                return this;
            },
            second() {
                this.a -= 2;
                return this;
            }
        };
        console.log(obj.first().second());
    };
})(LC02 || (LC02 = {}));
var HW01;
(function (HW01_1) {
    const str = 'A dummy string for loops practice';
    const arr = str.split(' ');
    const user = {
        name: 'someName',
        surname: 'surname'
    };
    const agedUser = {
        name: 'someName',
        age: 7
    };
    class Vagabond {
        left = 0;
        top = 0;
        goLeft() {
            this.left ? this.left-- : 0;
            return this;
        }
        goRight() {
            this.left++;
            return this;
        }
        goTop() {
            this.top ? this.top-- : 0;
            return this;
        }
        goBottom() {
            this.top++;
            return this;
        }
    }
    class Person {
        firstName;
        lastName;
        constructor(firstName, lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }
    class HW01 {
        task1(str) {
            const upper1stLetter = function (str) {
                let newStr = str[0], idx = 1;
                const length = str.length;
                while (idx < length) {
                    newStr += str[idx - 1] === ' ' ?
                        str[idx].toUpperCase() :
                        str[idx];
                    idx++;
                }
                return newStr;
            };
            return upper1stLetter(str);
        }
        task2(str) {
            const spaceToUnderscore = (str) => {
                let newStr = '', idx = 0;
                const length = str.length;
                if (length)
                    do {
                        newStr += str[idx] === ' ' ? '_' : str[idx];
                        idx++;
                    } while (idx < length);
                return newStr;
            };
            return spaceToUnderscore(str);
        }
        task3(arr) {
            function arrToStrOddIdxs(arr) {
                let newStr = '';
                const length = arr.length;
                for (let i = 0; i < length; i += 1) {
                    if (i % 2)
                        newStr += arr[i];
                }
                return newStr;
            }
            return arrToStrOddIdxs(arr);
        }
        task4(user) {
            let newStr = '';
            for (let key in user) {
                newStr += user[key][0].toUpperCase() + user[key].slice(1) + ' ';
            }
            return newStr.trim();
        }
        task5(user) {
            let profession;
            const { age } = user;
            switch (age) {
                case 14:
                    profession = 'hacker';
                    break;
                case 30:
                    profession = 'resistance leader';
                    break;
                case 40:
                    profession = 'president';
                    break;
                default:
                    profession = 'no information avaliable for this age';
                    break;
            }
            return { ...user, profession };
        }
        task6(...args) {
            let newStr = '';
            let length = args.length;
            for (let i = 0; i < length; i += 1) {
                let isUniq = true;
                for (let j = i + 1; j < length; j += 1) {
                    if (args[i] === args[j]) {
                        isUniq = false;
                        break;
                    }
                }
                newStr += `${isUniq ? args[i] : args[i] + '-' + args[i]},`;
            }
            return newStr;
        }
        task7(str) {
            return str.split(' ', 5).join(' ');
        }
        task8(arr) {
            return arr.every(el => el % 2 === 0);
        }
        task9(arr) {
            return arr.some(el => typeof el === 'number');
        }
        task10(cb, arr) {
            const length = arr.length;
            for (let i = 0; i < length; i += 1) {
                if (!cb(arr[i], i, arr))
                    return false;
            }
            return true;
        }
        task11(arr) {
            return arr.reduce((acc, { age }) => acc + age, 0);
        }
        task12(arr) {
            return arr.filter(({ age }) => age > 18);
        }
        task13(arr) {
            return arr
                .map(({ name, age }) => `<li>Name ${name}, Age ${age}<li>`).join('');
        }
        task14(...args) {
            return Math.min(...args);
        }
        task15({ name, age }) {
            return { name, age };
        }
        task16({ name, age }) {
            return {
                name,
                age,
                export() {
                    return {
                        name: this.name,
                        age: this.age
                    };
                }
            };
        }
        task17() {
            return new Vagabond();
        }
        task18({ firstName, lastName }) {
            return new Person(firstName, lastName);
        }
        task19() {
            function calculator() {
                let result = 0;
                return {
                    plus(num) {
                        result += num;
                        return this;
                    },
                    minus(num) {
                        result -= num;
                        return this;
                    },
                    getResult() {
                        console.log(result);
                        return this;
                    }
                };
            }
            return calculator();
        }
        task20(cb, arr) {
            function applyFunc(cb, arr) {
                let newStr = 'Result: ';
                const length = arr.length;
                const newArr = [];
                for (let i = 0; i < length; i += 1) {
                    newArr[i] = cb(arr[i], i, arr);
                }
                return newStr + newArr.join();
            }
            return applyFunc(cb, arr);
        }
        execute() {
            const agedArr = [
                {
                    age: 14,
                    name: '1'
                },
                {
                    age: 20,
                    name: '2'
                }
            ];
            console.log(this.task1(str));
            console.log(this.task2(str));
            console.log(this.task3(arr));
            console.log(this.task4(user));
            console.log(this.task5(agedUser));
            console.log(this.task6(18, 33, 55, 18, 150, 33, 17, 55, 98, 18));
            console.log(this.task7(str));
            console.log(this.task8([2, 4, 6, 8, 0, 9]));
            console.log(this.task9(['7', 2]));
            console.log(this.task10(el => el % 2 === 0, [2, 4, 6, 8, 0]));
            console.log(this.task11(agedArr));
            console.log(this.task12(agedArr));
            console.log(this.task13(agedArr));
            const vagabond = this.task17();
            console.log(vagabond.goBottom().goLeft().goLeft());
            const person = this.task18({
                firstName: 'FirstName',
                lastName: 'LastName'
            });
            console.log(person);
            const calculator = this.task19();
            calculator.plus(10).minus(5).getResult();
            console.log(this.task20(el => el, [1, 2, 3, 4, 5]));
            console.log(this.task20(({ name, universe }) => `${name} from ${universe}`, [{ name: 'Batman', universe: 'DC' }]));
            console.log(this.task20(el => el * 100, [1, 2, 3, 4, 5]));
        }
    }
    HW01_1.execute = function () {
        return new HW01().execute();
    };
})(HW01 || (HW01 = {}));
var LC03;
(function (LC03) {
    function sample() {
        const x = 5;
        function one() {
            console.log(this);
        }
        return one;
    }
    const sum1 = (x = 0) => (y = 0) => console.log(x + y);
    sample()();
    function myModule() {
        let value = 0;
        const increase = () => value++;
        const decrease = () => value--;
        return {
            increase, decrease
        };
    }
    const module = myModule();
    module.increase();
    module.increase();
    function sum(x = 0, y = 0) {
        return x + y;
    }
    LC03.sum = sum;
    function pow(x, y) {
        return Math.pow(x, y);
    }
    LC03.pow = pow;
    function testCall() {
        let result = null;
        const originalFunc = Math.pow;
        Math.pow = function (x, y) {
            result = [x, y];
            return originalFunc(x, y);
        };
        pow(5, 7);
        Math.pow = originalFunc;
        return result;
    }
    LC03.log = (...rest) => console.log(rest);
    const date = new Date();
    const timestamp = Date.now();
    const sameDate = new Date(timestamp);
    date.valueOf();
    const options = {
        era: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        weekday: 'long',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
    };
    date.toLocaleString();
    function meow() {
        console.log('meow!');
    }
    const timer = setTimeout(meow, 2 * 1000);
    setTimeout(clearTimeout, 1 * 1000, timer);
})(LC03 || (LC03 = {}));
var HW02;
(function (HW02_1) {
    class HW02 {
        task01(cb, ms) {
            function delay(cb, ms) {
                return setTimeout(cb, ms);
            }
            return delay(cb, ms);
        }
        task02(cb, ms) {
            function throttle(cb, ms) {
                let callFuncTime = 0;
                return function () {
                    let nowCallFunc = new Date().getTime();
                    if (nowCallFunc - callFuncTime < ms)
                        return;
                    callFuncTime = nowCallFunc;
                    return cb();
                };
            }
            return throttle(cb, ms);
        }
        task03(separator, ...args) {
            function joinBy1sArg(separator, ...args) {
                return args.join(separator);
            }
            return joinBy1sArg(separator, ...args);
        }
        task04(arr) {
            function sortDate(arr) {
                const normalize = (obj) => {
                    return obj.date.split('.').reverse().join();
                };
                return arr.sort((prev, next) => {
                    const nPrev = normalize(prev);
                    const nNext = normalize(next);
                    return nPrev > nNext ? 1 : nPrev < nNext ? -1 : 0;
                });
            }
            return sortDate(arr);
        }
        task05() {
            const obj = {
                x: 'bar'
            };
            function f() {
                console.log(this.x);
            }
            obj.f = f;
            obj.f();
            f.call(obj);
            const objF = f.bind(obj);
            objF();
        }
        task06(arr) {
            function arrToFlat(arr) {
                const result = [];
                for (let val of arr) {
                    Array.isArray(val) ?
                        result.push(...arrToFlat(val)) :
                        result.push(val);
                }
                return result;
            }
            return arrToFlat(arr);
        }
        execute() {
            console.log(this.task01(() => console.log('nope!'), 2000));
            console.log(this.task03('*', '1', 'b', '1c'));
            console.log(this.task04([
                { date: '10.01.2017' },
                { date: '05.11.2016' },
                { date: '21.10.2002' }
            ]));
            this.task05();
            console.log(this.task06([[1], 2, 3, [4, [5, 6]], 7, 8]));
        }
    }
    HW02_1.execute = new HW02().execute();
})(HW02 || (HW02 = {}));
var LC04;
(function (LC04) {
    class User {
        constructor(name) {
            this.name = name;
            this.sayHi = () => console.log('Hi');
        }
    }
    class Employee extends User {
        name;
        _job;
        constructor(name, _job) {
            super(name);
            this.name = name;
            this._job = _job;
        }
        get job() {
            return this._job;
        }
        set job(job) {
            this._job = job;
        }
    }
    const alice = new User('Alice');
    const max = new Employee('Max', 'developer');
    max.job = 'frontend';
    console.log(alice, max);
    class Car {
        fuel;
        static _waste = 1;
        drive;
        constructor(fuel) {
            this.fuel = fuel;
            this.drive = function (km = 1) {
                console.log('Frrrrrrrrrrrr!');
                this.calculateWaste(km);
            };
        }
        calculateWaste(km) {
            this.fuel -= Car._waste * km;
        }
    }
    class SportCar extends Car {
        color = 'red';
    }
    const car = new Car(10);
    console.log(car);
    car.drive();
    console.log(car);
    const scar = new SportCar(20);
    console.log(scar);
    const animal = {
        walks: true,
        eats: true
    };
    class Cat {
        meows;
        static Cat;
        constructor(meows = true) {
            this.meows = meows;
        }
    }
    const somecat = new Cat();
    console.log(somecat);
    class Person {
        name;
        email;
        password;
        constructor(name, email, password) {
            this.name = name;
            this.email = email;
            this.password = password;
        }
    }
    class Customer extends Person {
        ballance;
        constructor(name, email, password, ballance) {
            super(name, email, password);
            this.ballance = ballance;
            this.ballance = ballance;
        }
        login() { }
        acceptContract() { }
        pay() { }
    }
    class Contactor extends Person {
        contracts;
        constructor(name, email, password, contracts) {
            super(name, email, password);
            this.contracts = contracts;
            this.contracts = contracts;
        }
        proposeContract() { }
    }
    class Furniture {
        material;
        price;
        constructor(material, price) {
            this.material = material;
            this.price = price;
        }
        calculatePrice(count) {
            return this.price * count;
        }
    }
    class KitchenFurniture extends Furniture {
        buitInAllpience(n) {
            this.price += n;
        }
    }
})(LC04 || (LC04 = {}));
var LC05;
(function (LC05) {
    const str = 'Test string to find something!';
    const search = 'find';
    const regExp = new RegExp(search, 'gi');
    LC05.execute = function () {
        console.log(str.match(search));
        console.log(str.match(/t/gi));
        console.log(str.replace(/e/gi, 'E'));
        console.log(regExp.test(str));
        console.log(/t/.exec(str));
        const p = new Promise((res, rej) => {
            const random = Math.random();
            const cb = random < 0.5 ? res : rej;
            const result = random < 0.5 ? "OK" : "Err";
            setTimeout(cb, 1 * 1000, result);
        });
        console.log(p);
        p.then(console.log).catch(console.log);
        let ReadyState;
        (function (ReadyState) {
            ReadyState[ReadyState["Unsended"] = 0] = "Unsended";
            ReadyState[ReadyState["Opened"] = 1] = "Opened";
            ReadyState[ReadyState["Sended"] = 2] = "Sended";
            ReadyState[ReadyState["ChankContent"] = 3] = "ChankContent";
            ReadyState[ReadyState["Done"] = 4] = "Done";
        })(ReadyState || (ReadyState = {}));
        const xhr = new XMLHttpRequest();
    };
})(LC05 || (LC05 = {}));
LC05.execute();
