"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = require("http");
const simpleServer = new http_1.Server((req, res) => {
    console.log(`Request is comming...`);
    res.write('hello World!');
    res.end();
});
simpleServer.listen(3000, 'localhost', () => {
    console.log(`Server listen on localhost:3000`);
});
