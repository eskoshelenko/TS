"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const { random, floor } = Math;
class MyClass {
    id;
    constructor() {
        this.id = floor(random() * 100500);
    }
}
exports.default = new MyClass();
