"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Task = void 0;
class Task {
    name;
    status;
    constructor(name) {
        this.name = name;
        this.status = 'Not started';
    }
    execute() {
        this.status = 'Done';
    }
}
exports.Task = Task;
