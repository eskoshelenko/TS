"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ObservableTask = void 0;
const Task_1 = require("./Task");
class ObservableTask extends Task_1.Task {
    observables;
    constructor(name) {
        super(name);
        this.observables = [];
    }
    addObservable(observer) {
        if (typeof observer === 'function') {
            this.observables.push(observer);
        }
        return;
    }
    removeObserver(observer) {
        this.observables = this.observables
            .filter(item => item !== observer);
    }
    notify() {
        for (let observable of this.observables) {
            observable();
        }
        super.execute();
    }
    execute() {
        this.notify();
        super.execute();
    }
}
exports.ObservableTask = ObservableTask;
