"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updater = exports.logger = exports.notifier = void 0;
const notifier = function () {
    console.log('Task is executing');
};
exports.notifier = notifier;
const logger = function () {
    console.log('Task is executing and logging');
};
exports.logger = logger;
const updater = function () {
    console.log('Task is updating');
};
exports.updater = updater;
