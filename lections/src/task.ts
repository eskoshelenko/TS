/*Ділан і Кейт хочуть подорожувати між кількома містами А, В, С.... Кейт має на аркуші паперу список відстаней між цими містами. ls = [51, 56, 58, 59, 61]. Ділан втомився їздити, і він каже Кейт, що не хоче їхати більше t = 174 милі, і він відвідає лише 3 міста. Які відстані, а отже, які міста вони оберуть, щоб сума відстаней була якомога більшою, щоб догодити Кейт та Ділану?

Приклад:
Маючи список ls та 3 міста для відвідування, вони можуть зробити вибір між: [51,56,58], [51,56,59], [51,56,61], [51,58,59], [51, 58,61], [51,59,61], [56,58,59], [56,58,61], [56,59,61], [58,59,61].

Тоді суми відстаней складають: 165, 166, 168, 168, 170, 171, 173, 175, 176, 178.

Найбільшою можливою сумою з урахуванням обмеження в 174 є 173, а відстані до 3 відповідних міст - [56, 58, 59].

Функція chooseOptimalDistance приймає параметри:
t (максимальна сума відстаней, ціле число >= 0),
k (кількість міст, які потрібно відвідати, k> = 1),
ls (список відстаней, всі відстані є додатними або нульовими цілими числами, і цей список містить принаймні один елемент). 

Функція повертає "найкращу" суму, тобто найбільшу можливу суму k відстаней, менших або рівних заданій межі t, якщо ця сума існує, або якщо не існує - null. 
Примітка: не змінюйте змінну ls. 

Важливо: треба вирішити завдання для будь-якого k, а не тільки 3.

Початковий код*/
// const chooseOptimalDistance = (t, k, ls) => {
//     // твій код
//     return null;
// }

// chooseOptimalDistance(174, 3, [51, 56, 58, 59, 61]); //173
// chooseOptimalDistance(163, 3, [50]); // null

// function getCombs(
//   arr: number[], 
//   combs: number
// ): number[][] {
//   const result = []
  
//   for (let i = 0; i <= arr.length - combs; i += 1) {
//     if (combs == 1) {
//       return arr.map(el => [el])
//     }
//     else {
//       const nextCombs = getCombs(arr.slice(i + 1), combs - 1)

//       for (let j = 0; j < nextCombs.length; j += 1) {
//         result.push(nextCombs[j].concat([arr[i]]))
//       }
//     }
//   }

//   return result
// }
// const chooseOptimalDistance = (
//   t: number, 
//   k: number, 
//   ls: number[]
// ): number | null => {
//   if (!ls.length || k < 1 || k > ls.length) {
//     return null
//   }
  
//   const ways = getCombs(ls, k)  
//     .map(combArr => combArr.reduce((acc, el) => acc + el))
//     .filter(el => el < t)
//     .sort((prev, next) => next - prev)

//   return ways[0] || null
// }

// const a = chooseOptimalDistance(174, 3, [51, 56, 58, 59, 61])
// const b = chooseOptimalDistance(163, 3, [50])
// console.log(a, b)

// ======================================

const arr = [1, 2, 3, 4]
let k = 3

// const res = arr
//   .map(el1 => arr                              // k = 1
//     .map(el2 => arr                            // k = 2
//       .map(el3 => arr                          // k = 3
//         .map(el4 => [el1, el2, el3, el4]))))   // k = 4

/* 
k = 1 
res1 = arr.map(el1 => [].conct(el1))    
[[1], [2], [3]]
k = 2 
res2 = res1.map(el1 => arr.map(el2 => el1.concat(el2)))
[
  [1, 1], [1, 2], [1, 3], 
  [2, 1], [2, 2], [2, 3],
  [3, 1], [3, 2], [3, 3]
]
k = 3 
res3 = res2.map(el2 => arr.map(el3 => el2.concat(el3)))
[
  [1, 1, 1], [1, 1, 2], [1, 1, 3], 
  [1, 2, 1], [1, 2, 2], [1, 2, 3],
  [1, 3, 1], [1, 3, 2], [1, 3, 3],
  [2, 1, 1], [2, 1, 2], [2, 1, 3], 
  [2, 2, 1], [2, 2, 2], [2, 2, 3],
  [2, 3, 1], [2, 3, 2], [2, 3, 3],
  [3, 1, 1], [3, 1, 2], [3, 1, 3], 
  [3, 2, 1], [3, 2, 2], [3, 2, 3],
  [3, 3, 1], [3, 3, 2], [3, 3, 3]
]

*/

//  к каждому крайнему должен конкатится весь массив предидущих значений

function getAllCombs(arr: any[], combs: number) {
  let result: any[] = []

  if(combs = 1) result = arr.map(el1 => [].concat(el1))
  
  while(combs > 1) {
    result = result.map(el1 => {
      console.log(arr.map(el2 => el1.concat(el2)))
      return arr.map(el2 => el1.concat(el2))
    })

    combs--
  }

  return result
}

console.log(getAllCombs(arr, 2))
