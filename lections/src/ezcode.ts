// namespace LC01{
//   // ====================== DOM ==============================
//   type Element = HTMLElement | null
//   type Img = HTMLImageElement | null

//   const body: Element = document.body
//   const div: Element = document.getElementById('div')!
//   const lis: HTMLCollection = document.getElementsByTagName('li')
//   const form: NodeList = document.getElementsByName('myForm')
//   const ul: HTMLCollection = document.getElementsByClassName('ulClass')
//   // document.querySelector[All] finds not live el's[collections]
//   const secondLi = document.querySelector('ul>li:nth-child(2)')!
//   const input: Element = document.getElementById('myInput')
  
//   const img: Img = document.querySelector('img')

//   export const execute = function (): void {
//     console.log(body.firstChild)
//     console.log(body.lastChild)

//     div.appendChild(document.createElement('span'))
//     console.log(lis)
//     console.dir(div)
//     console.dir(form)
//     console.log(ul)
//     console.dir(secondLi)
//     console.log(div.matches('#div'))
//     console.log(secondLi?.closest('ul.ulClass')) // return null or closet el. Can use when user clicks out of modal window to close the modal window
//     secondLi.textContent = 'Hi'
//     console.log(img?.hasAttribute('src'))
//     console.log(img?.getAttribute('src'))
//     img?.setAttribute('src', 'img/кбк.завод.2.jpg')
//     setTimeout(() => img?.removeAttribute('src'), 3 * 1000)

//     // data-attribs uses to validate form or etc.
//     console.log(input?.dataset)

//     console.log(div.className)
//     div.classList.add('1', '2')
//     div.classList.remove('1', '2')
//     div.classList.toggle('d-block')
//     console.log(div.classList)
//     console.log(div.classList.item(0))
//     console.log(div.classList.contains('db'))

//     div.style.color = 'green'

//     console.log(window.getComputedStyle(div))
//     console.log(div.offsetHeight, div.offsetWidth)

//     console.log(div.clientTop, div.clientLeft)

//     console.log(div.clientHeight, div.clientWidth)

//     console.log(div.scrollHeight, div.scrollWidth)
    
//     // .outerHTML .innerHtml .innerText - used to change el value

//     // ======================== Cycles ============================

//     // while, do while, for, for in (obj), for of (arr), forEach (collections)
//   }
// }

// LC01.execute()

namespace LC02 {
  type Key = string | number| symbol
  interface User {
    name: string
    lastname: string
    age: number
    password: string
  }
  interface IPerson {
    name: string
    talk: () => void
  }
  interface ExtendedObj extends Object {
    [key: Key]: any
    talk?: () => void
  }

  // ====================== Functions ======================

  // func declaratoin has hoisting
  function someName(x:number, y: number): number {
    console.log(`x = ${x}, y = ${y}`)
    return x + y
  }

  // func expression has no hiosting
  const pow = function (num: number, pow: number): number {
    console.log(`num = ${num}, pow = ${pow}`)
    return num ** pow
  }

  // arrow func no hoisting no context no arguments 
  const average = (...args: number[]): number => {
    return args.reduce((result, el) => result += el, 0) / args.length
  }
  const averageShort = (...args: number[]): number =>  args
    .reduce((result, el) => result += el, 0) / args.length

  // recoursive func
  function arrToFlat (arr: any[]): any[] {
    return arr.reduce((acc, el) => acc.concat(Array.isArray(el) ? arrToFlat(el) : el), [])
  }

  // func with default params & overload func
  function getBlock(): string
  function getBlock(height: number): string
  function getBlock(height: number, width: number): string
  function getBlock(height: number = 0, width: number = 0): string {
    return `<div style"height:${height},width:${width}"></div>`
  }

  // using rest & spread operators ...name in func params - rest, other spread
  const max = (...args: number[]): number => Math.max(...args)

  function concat<T, R> (arr1: T[], arr2: R[]): (T | R)[] {
    return [...arr1, ...arr2]
  }

  // destructure example
  const greeting = ({name, lastname}: User): string => `Buenos Diaz, ${name} ${lastname}` 

  // clousure func
  function makeCounter() {
    let count = 0

    // value count are closed in this case we have no access to it
    return function() {
      return count++
    }
  }

  // func constructors
  class Person implements IPerson {
    name: string
    talk: () => void
    
    constructor(name: string) {
      this.name = name
      this.talk = function(): void {
        console.log(`Hi, I am ${this.name}`)
      }
    }
  }

  export const execute = function (): void {
    const arr1 = [1,2,[3,4,[5,6,7]],[8,9]]
    const arr2: number[] = [1,2,3,4,5,6,7,8,9,0]
    const user: User = {
      name: 'John',
      lastname: 'Konnor',
      age: 16,
      password: '1234'
    }

    console.log(someName(4, 5))
    console.log(pow(3, 4))

    // immidiately executed func no hoisting
    ;(function sqrt(num: number): number {
      const result = Math.sqrt(num)

      console.log(`Sqrt from ${num} is ${result}`)

      return result
    })(9)

    console.log(arrToFlat(arr1))
    console.log(getBlock(), getBlock(10), getBlock(12,11))
    // console.log(Math.max(str)) // in lect this works
    console.log(concat(arr1, arr2))
    console.log(greeting(user))

    // ======================= Objects =======================

    const he: ExtendedObj = {
      name: 'This name'
    }

    he.talk = function (): void {
      console.log(`Hi, I am ${this.name}`)
    }

    const anotherHe = {
      name: 'New name',
      talk(): void {
        return he.talk?.bind(this)()
      }
    }

    he.talk()
    he.talk.call(anotherHe) // same as apply
    anotherHe.talk() 
    
    const person = new Person('Ivan')

    console.log(person)
    person.talk()

    // while ref counter 1 & counter2 defined count will stored in RAM until refs are corrupted
    const counter1 = makeCounter()
    const counter2 = makeCounter()
    
    console.log(counter1(), counter2())
    console.log(counter1(), counter2())
    console.log(counter1())

    // chaining
    const obj = {
      a: 5,
      first() {
        this.a += 5
        return this
      },
      second() {
        this.a -= 2
        return this
      }
    }

    console.log(obj.first().second())
  }
}

// LC02.execute()

namespace HW01 {
  interface Calculator {
    plus: (num: number) => Calculator, 
    minus: (num: number) => Calculator,
    getResult: () => Calculator
  }
  interface User {
    [key: string]: string
  }
  interface IPerson {
    firstName: string
    lastName: string
  }
  interface AgeUser {
    name: string
    age: number
    profession?: string
  }
  const str: string = 'A dummy string for loops practice'
  const arr: string[] = str.split(' ')
  const user: User = {
    name: 'someName',
    surname: 'surname'
  }
  const agedUser: AgeUser = {
    name: 'someName',
    age: 7
  }

  class Vagabond {
    private left: number = 0
    private top: number = 0

    public goLeft(): Vagabond {
      this.left ? this.left-- : 0
      return this
    }

    public goRight(): Vagabond {
      this.left++
      return this
    }

    public goTop(): Vagabond {
      this.top ? this.top-- : 0
      return this
    }

    public goBottom(): Vagabond {
      this.top++
      return this
    }
  }

  class Person implements IPerson {
    constructor(public firstName: string, public lastName: string){}
  }

  // использовать каждый цикл, использовать каждый вид функций, экспр, декл, эрроу
  class HW01 {
    //Написать функцию, принимающую строку, циклом сделать все первые буквы большими каждого слова, вернуть новую строку
    private task1(str: string): string {
      const upper1stLetter = function (str: string): string {
        let newStr: string = str[0], idx: number = 1
        const length: number = str.length

        while(idx < length) {
          newStr += str[idx - 1] === ' ' ? 
            str[idx].toUpperCase() :
            str[idx]
          idx++
        }
        return newStr
      }

      return upper1stLetter(str)
    }
    //Написать функцию, принимающую строку и циклом изменить все пробелы строки на подчеркивания, вернуть новую строку
    private task2(str: string): string {
      const spaceToUnderscore = (str: string):string => {
        let newStr: string = '', idx: number = 0
        const length: number = str.length

        if(length) do {
          newStr += str[idx] === ' ' ? '_' : str[idx]
          idx++
        } while(idx < length)

        return newStr
      }

      return spaceToUnderscore(str)
    }
    // Написать функцию, принимающую массив, циклом составить строку из каждого второго элемента массива
    private task3(arr: string[]): string {
      function arrToStrOddIdxs (arr: string[]): string {
        let newStr = ''
        const length: number = arr.length

        for(let i = 0; i < length; i += 1) {
          if(i % 2) newStr += arr[i]
        }

        return newStr
      }

      return arrToStrOddIdxs(arr)
    }
    //Написать функцию, принимающую объект, возвращающую строку с именем и фамилией с больших букв
    private task4(user: User): string {
      let newStr = ''

      for(let key in user) {
        newStr += user[key][0].toUpperCase() + user[key].slice(1) + ' '
      }

      return newStr.trim()
    }
    //Написать функцию принимающую объект, через свитч проверить возраст, вернуть объект где будет добавлено свойство профешн если 14 - hacker, 30 - resistance leader, 40 - president default - no information avaliable for this age
    private task5(user: AgeUser): AgeUser {
      let profession: string
      const {age} = user

      switch (age) {
        case 14:
          profession = 'hacker'
          break
        case 30:
          profession = 'resistance leader'
          break
        case 40:
          profession = 'president'
          break      
        default:
          profession = 'no information avaliable for this age'
          break
      }

      return {...user, profession}
    }
    // Функция, принимающая любое кол-во аргументов, циклом проверить совпадающие аргументы, вернуть строку, где через дефис будут выведены все совпадающие аргументы, между отличающимися запятая. 18-18, 33-33, ...
    private task6(...args: Array<string | number>): string {
      let newStr: string = ''
      let length: number = args.length

      for(let i = 0; i < length; i += 1) {
        let isUniq: boolean = true

        for(let j = i + 1; j < length; j += 1) {
          if(args[i] === args[j]) {
            isUniq = false
            break
          }
        }

        newStr += `${isUniq ? args[i] : args[i] + '-' + args[i]},`
      }

      return newStr
    }
    //Функция,принимает строку, слова разделены пробелами, возвращает первые 5 слов этой строки, попытатся решить методами массива в 1 строку
    private task7(str: string): string {
      return str.split(' ', 5).join(' ')
    }
    //Функция принимает массив, возвращает тру если все элементы четные
    private task8(arr: number[]): boolean {
      return arr.every(el => el % 2 === 0)
    }
    //Функция принимает массив и возвращает тру если хотябы один из жлементов массива число
    private task9(arr: any[]): boolean {
      return arr.some(el => typeof el === 'number')
    }
    //Написать функцию, принимающую массив и функцию колбек и возвращает тру, когда функция колбек вернула тру для всех элементов массива. (не юзать методо эвери)
    private task10<T>(cb: (el: T, idx?: number, arr?: T[]) => boolean, arr: T[]): boolean {
      const length: number = arr.length

      for(let i = 0; i < length; i += 1 ) {
        if(!cb(arr[i], i, arr)) return false
      }

      return true
    }
    //Фнукция принимает массив объектов пользователей вида нейм эйдж, получить сумму всех возрастов пользователей в массиве
    private task11(arr: AgeUser[]): number {
      return arr.reduce((acc, {age}) => acc + age, 0)
    }
    //Функция принимает массив объектов типа найм эйдж, вернуть совершеннолетних пользователей
    private task12(arr: AgeUser[]): AgeUser[] {
      return arr.filter(({age}) => age > 18)
    }
    //функция принимает массив объектов типа нейм эйдж, выводит строку из <li>Name ${name}, Age ${age}<li>
    private task13(arr: AgeUser[]): string {
      return arr
        .map(({name, age}) => `<li>Name ${name}, Age ${age}<li>`).join('')
    }
    //Испльзуя рэст оператор дважды, напишити функцию, которая принимает любое исло аргументов чисел и возврщает наименьшее из них. Math.min
    private task14(...args: number[]): number {
      return Math.min(...args)
    }
    //используя деструктуризацию напишите функцию, принимающую объект со свойствами, среди которых будет свойство name и age возвращает объект только с параметрами name age
    private task15({name, age}: AgeUser): {name:string, age: number} {
      return {name, age}
    }
    //Функция принимающая объект и возвращающая объект с методом export, {name,age,export()}
    private task16({name, age}: AgeUser): {name:string, age: number, export: () => {name:string, age: number}} {
      return {
        name,
        age,
        export() {
          return {
            name: this.name,
            age: this.age
          }
        }
      }
    }
    //Создать объект vagabond, который содержит положение объекта, свойства left top, кол-во клеток отсчет сверху, методы goLeft,goRight, goDown, goTop увеличивающий, уменьшающий соответствующие параметры. Реализовать возможность использования цепочек каждый метод должен возвращать сам объект при инициализации лефт и топ нули
    private task17(): Vagabond {
      return new Vagabond()
    }
    //Создать функцию конструктор User принимающую firstName, lastName и возвращающую новый объект со этими свойствами. создать пользователя.
    private task18({firstName, lastName}: IPerson): Person {
      return new Person(firstName, lastName)
    }
    //Написать модуль калькулятор, который будет хранить текущее значение вычислений(на момент инициализации 0), возвращать объект с методами plus(num), minus(num), getResult(num)
    private task19(): Calculator {
      function calculator(): Calculator {
        let result: number = 0
        return {
          plus(num: number): Calculator {
            result += num
            return this
          },
          minus(num: number): Calculator {
            result -= num
            return this
          },
          getResult(): Calculator {
            console.log(result)
            return this
          }
        }
      }

      return calculator()
    }
    //Написать функция applyFunc(cb, arr) => 'result: res(cb(el1)...)' applyFunc(cb,arr) => 'result: el1,el2,el3...', applyFunc(cb,arr) => 'result: ${name} from ${uiverse}, ...', applyFunc(cb,arr) => 'result: el1*100,el2*100...
    private task20<T>(cb: (el: T, idx: number, arr: T[]) => any, arr: T[]): string {
      function applyFunc<T>(cb: (el: T, idx: number, arr: T[]) => any, arr: T[]): string {
        let newStr: string = 'Result: '
        const length = arr.length
        const newArr: T[] = []

        for(let i = 0; i < length; i += 1) {
          newArr[i] =  cb(arr[i], i, arr)
        }

        return newStr + newArr.join()
      }

      return applyFunc(cb, arr)
    }

    public execute(): void {
      const agedArr: AgeUser[] = [
        {
          age: 14, 
          name: '1'
        }, 
        {
          age: 20, 
          name: '2'
        }
      ]
      console.log(this.task1(str))
      console.log(this.task2(str))
      console.log(this.task3(arr))
      console.log(this.task4(user))
      console.log(this.task5(agedUser))
      console.log(this.task6(18,33,55,18,150,33,17,55,98, 18))
      console.log(this.task7(str))
      console.log(this.task8([2,4,6,8,0, 9]))
      console.log(this.task9(['7', 2]))
      console.log(this.task10(el => el % 2 === 0, [2,4,6,8,0]))
      console.log(this.task11(agedArr))
      console.log(this.task12(agedArr))
      console.log(this.task13(agedArr))

      const vagabond = this.task17()
      console.log(vagabond.goBottom().goLeft().goLeft())

      const person = this.task18({
        firstName: 'FirstName',
        lastName: 'LastName'
      })
      console.log(person)

      const calculator = this.task19()
      calculator.plus(10).minus(5).getResult()

      console.log(this.task20(el => el, [1,2,3,4,5]))
      console.log(this.task20(({name, universe}) => `${name} from ${universe}`, [{name: 'Batman', universe: 'DC'}]))
      console.log(this.task20(el => el * 100, [1,2,3,4,5]))
    }
  }

  export const execute = function (): void {
    return new HW01().execute()
  }
}

// HW01.execute()

namespace LC03 {
// export namespace LC03 {
  function sample (): () => void {
    const x: number = 5

    function one(this: any): void {
      console.log(this)
    }

    // const one = (): void => {
    //   console.log(this)
    // }

    return one
  }

  const sum1 = (x: number = 0): (y: number) => void => (y: number = 0): void => console.log(x + y)

  sample()()
  //sum1(1)(2)

  function myModule() {
    let value: number = 0

    const increase = (): number => value++
    const decrease = (): number => value--

    return {
      increase, decrease
    }
  }

  const module = myModule()
  module.increase()
  module.increase()
  //console.log(module.decrease())

  // TDD - test driven development => BDD
  // There are already libs with tests mocha, chai
  type Sum = number | string
  
  export function sum(x: any = 0, y: any = 0) {
    return x + y
  }

  export function pow (x: number, y: number): number {
    return Math.pow(x, y)
  }

  function testCall(): number[] | null {
    let result = null
    const originalFunc = Math.pow
    
    Math.pow = function (x: number, y: number): number {
      result = [x, y]
      return originalFunc(x, y)
    }
    pow(5,7)

    Math.pow = originalFunc

    return result
  }

  // mock - делает ту же работу что и подменяемая функция и параллельно еще чтото

  // spy - оборачивает функцию в более сложный объект, всегда вызывает оригинальную функцию

  // stub - заменяет настоящую функцию, стаб не должен давать отрабатывать  оригинальной функции никогда.

  export const log = (...rest: any[]): void => console.log(rest)

  // ======================== Date ========================

  const date: Date = new Date() // current date
  const timestamp: number = Date.now() // number of ms from 1970

  const sameDate: Date = new Date(timestamp)
  // const sameDate: Date = new Date(YYYY: number, MM: number,DD, h, m, s, ms)
  /* enum MM = {
    January = 0, 
    February = 1, 
    March = 2, 
    April = 3, 
    May = 4, 
    June = 5, 
    July = 6, 
    Augest = 7,
    September = 8,
    October = 9,
    November = 10,
    Desember = 11
  }*/
  // const sameDate: Date = new Date("YYYY-MM-DD")

  // date.get[Year, Month, Date, Day, Time, Hours, Minutes, Seconds, Milliseconds] => [Year, Month, Date, Day, timestamp, Hours, Minutes, Seconds, Milliseconds] 
  /* enum Date {
    Sunday = 0,
    Monday = 1,
    Tuesday = 2,
    Wednesday = 3
    Thursday = 4,
    Friday = 5,
    Saturday = 6
  }
   */

  // date.set[FullYear, Month, ...]
  // date.setFullYear(YYYY, MM, DD)

  date.valueOf() // returns timestamp

  type longShortNarrow = "long" | "short" | "narrow"
  type number2Dgts = "numeric" | "2-digit"

  interface Options {
    era?: longShortNarrow
    year?: number2Dgts
    month?: longShortNarrow
    day?: number2Dgts
    weekday?: longShortNarrow
    hour?: number2Dgts
    minute?: number2Dgts
    second?: number2Dgts
  }
  
  const options: Options = {
    era: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    weekday: 'long',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric'
  }

  date.toLocaleString() // YYYY-MM-DD hh-mm-ss
  // console.log(date.toLocaleString('ru-RU', options))
  // Monday, April 18, 2022 Anno Domini, 11:30:08 AM
  // понедельник, 18 апреля 2022 г. от Рождества Христова, 11:35:31

  function meow(): void {
    console.log('meow!')
  }

  const timer = setTimeout(meow, 2 * 1000)
  setTimeout(clearTimeout, 1 * 1000, timer) // del timer before 1s

  //const interval = setInterval(meow, 1 * 1000)
  //setTimeout(clearInterval, 5 * 1000, interval)

  // events
}


namespace HW02 {
  interface DateObj {
    date: string
  }
  
  class HW02 {
    //Написать функцию откладывающую колбэк на определенное время
    private task01(cb: (...args: any[]) => any, ms: number): any {
      function delay (cb: (...args: any[]) => any, ms: number): any {
        return setTimeout(cb, ms)
      }

      return delay(cb, ms)
    }
    //Функция, которая позволяет вызывать колбек, не чаще чем в милисикунд
    private task02(cb: (this: any, ...args: any[]) => any, ms: number): any | void {
      function throttle(cb: (...args: any[]) => any, ms: number): any {
        let callFuncTime = 0

        return function () {
          let nowCallFunc = new Date().getTime()

          if(nowCallFunc - callFuncTime < ms) return
          callFuncTime = nowCallFunc
          return cb()
        }
      }

      return throttle(cb, ms)
    }
    //Дана функция, она принимает в качестве агрументов строки '*', '1', 'b', '1c', реализуйте ее так, чтобы она вернула 1*b*1с
    private task03(separator: string, ...args: string[]): string {
      function joinBy1sArg (separator: string, ...args: string[]): string {
        return args.join(separator)
      }
      
      return joinBy1sArg(separator, ...args)
    }
    // Есть массив в котором лежат объекты с датами, отсортировать по датам, [ {date: '10.01.2017'}, {date: '05.11.2016'}, {date: '21.10.2002'} ]
    private task04(arr: DateObj[]): DateObj[] {
      function sortDate (arr: DateObj[]): DateObj[] {
        const normalize = (obj: DateObj): string => {
          return obj.date.split('.').reverse().join()
        }

        return arr.sort((prev, next) => {
          const nPrev = normalize(prev)
          const nNext = normalize(next)

          return nPrev > nNext ? 1 : nPrev < nNext ? -1 : 0
        })
      }

      return sortDate(arr)
    }
    // Есть функция и объект, напишите все известные вам способы, чобы вывести в консоли значение х из объекта используя функцию
    private task05() {
      interface ObjWithF {
        x: string 
        f?: () => void
        prototype?: any
      }
      const obj: ObjWithF = {
        x: 'bar'
      }
      function f(this: any): void {
        console.log(this.x)
      }
      
      obj.f = f
      obj.f()
      f.call(obj) // f.apply(obj)
      const objF = f.bind(obj)
      objF()
      // obj.prototype.f = f

    }
    // Сделать матрицу одномерной из многомерной
    private task06(arr: any[]): any[] {
      function arrToFlat (arr: any[]): any[] {
        const result: any[] = []

        for(let val of arr) {
          Array.isArray(val) ? 
            result.push(...arrToFlat(val)) :
            result.push(val)
          
          // result.concat(Array.isArray(val) ? arrToFlat(val) : val)
        }

        return result
      }

      return arrToFlat(arr)
    }

    public execute () {
      console.log(this.task01(() => console.log('nope!'), 2000))
      console.log(this.task03('*', '1', 'b', '1c'))
      console.log(this.task04([
        {date: '10.01.2017'},
        {date: '05.11.2016'},
        {date: '21.10.2002'}
      ]))
      this.task05()
      console.log(this.task06([[1], 2, 3, [4, [5, 6]], 7, 8]))
      
      
    }
  }

  export const execute = new HW02().execute()
}

// HW02.execute

namespace LC04 {
  interface User {
    name: string
    sayHi: () => void
  }
  class User {
    constructor(name: string) {
      this.name = name
      this.sayHi = (): void => console.log('Hi')
    }
  }

  class Employee extends User {
    constructor( public name: string, private _job: string ) {
      super(name)
    }

    get job(): string {
      return this._job
    }

    set job(job: string) {
      this._job = job
    }
  }

  const alice = new User('Alice')
  const max = new Employee('Max', 'developer')
  
  max.job = 'frontend'

  console.log(alice, max)

  class Car {
    static _waste: number = 1
    drive: (km?: number) => void

    constructor(public fuel: number) {
      this.drive = function (km: number = 1) {
        console.log('Frrrrrrrrrrrr!')
        this.calculateWaste(km)
      }
    }

    private calculateWaste(km: number) {
      this.fuel -= Car._waste * km
    }
  }

  class SportCar extends Car {
    public color: string = 'red'
  }

  const car = new Car(10)

  console.log(car)

  car.drive()

  console.log(car)

  const scar = new SportCar(20)

  console.log(scar)

  const animal = {
    walks: true,
    eats: true
  }

  class Cat {
    static Cat: { walks: boolean; eats: boolean } // Cat.prototype = animal
    constructor(public meows: boolean = true){}
  }

  const somecat = new Cat()
  console.log(somecat)

  // function Person1(name, email, password) {
  class Person {
    // this.name = name
    // this.email = email
    // this.password = password                
    constructor(
      public name: string,
      public email: string,
      public password: string
    ) {}
  }

  // function Customer1(name, email, password, ballance) {
  class Customer extends Person {
    // this.ballance = ballance
    constructor(
      name: string,
      email: string, 
      password: string, 
      public ballance: number
    ) {
      // Person1.apply(this, [name, email, password])
      // Customer1.prototype = Object.create(Person1)
      // Customer1.prototype.constructor = Customer1
      super(name, email, password)

      this.ballance = ballance
    }

    // Customer1.prototype.login = function () {}
    // Customer1.prototype.acceptContract = function () {}
    // Customer1.prototype.pay = function () {}
    login() {}
    acceptContract() {}
    pay() {}
  }

  // function Customer1(name, email, password, ballance) {
  class Contactor extends Person {
    // this.contracts = contracts
    constructor(
      name: string,
      email: string, 
      password: string, 
      public contracts: number[]
    ) {
      // Person1.apply(this, [name, email, password])
      // Contractor1.prototype = Object.create(Person1)
      // Contractor1.prototype.constructor = Contractor1
      super(name, email, password)

      this.contracts = contracts
    }

    // Contactor1.prototype.proposeContract = function () {}
    proposeContract() {}
  }

  // ================== Function style ==================

  // function Person1(name, email, password) {
  //   this.name = name
  //   this.email = email
  //   this.password = password
  // }

  // function Customer1(name, email, password, ballance) {
  //   Person1.apply(this, [name, email, password])
  //   this.ballance = ballance
  // }

  // Customer1.prototype = Object.create(Person1)
  // Customer1.prototype.constructor = Customer1

  // Customer1.prototype.login = function () {}
  // Customer1.prototype.acceptContract = function () {}
  // Customer1.prototype.pay = function () {}

  // function Contactor1(name, email, password, contracts) {
  //   Person1.apply(this, [name, email, password])
  //   this.contracts = contracts
  // }

  // Contactor1.prototype = Object.create(Person1)
  // Contactor1.prototype.constructor = Contactor1

  // Contactor1.prototype.proposeContract = function () {}

  class Furniture {
    constructor(
      public material: string[],
      public price: number
    ){}

    calculatePrice (count: number): number {
      return this.price * count
    }
  }

  class KitchenFurniture extends Furniture {
    buitInAllpience(n: number) {
      this.price += n
    }
  }

  // function Furniture1 (material, price) {
  //   this.material = material
  //   this.price = price
  // }

  // Furniture1.prototype.calculatePrice = function (n) {
  //   return this.price * n
  // }

  // function KitchenFurniture1(material, price) {
  //   Furniture.apply(this, [material, price])
  // }

  // KitchenFurniture1.prototype = Object.create(Furniture1)
  // KitchenFurniture1.prototype.constructor = KitchenFurniture1

  // KitchenFurniture1.prototype.buitInAllpience = function (n) {
  //   this.price += n
  // }
}

namespace LC05 {
  const str: string = 'Test string to find something!'
  const search: string = 'find'
  const regExp = new RegExp(search, 'gi')

  export const execute = function (): void {
    console.log(str.match(search))
    // [
    //   'find',
    //   index: 15,
    //   input: 'Test string to find something!',
    //   groups: undefined
    // ]
    console.log(str.match(/t/gi))
    // [ 'T', 't', 't', 't', 't' ]
    console.log(str.replace(/e/gi, 'E'))
    // TEst string to find somEthing!
    console.log(regExp.test(str)) // true
    console.log(/t/.exec(str))
    // [
    //   't',
    //   index: 3,
    //   input: 'Test string to find something!',
    //   groups: undefined
    // ]

    // ================= Location ================

    // location.reload() - reload page
    // location.assign('Target url') or .replace() - target location

    // =================== Ajax ===================
    
    const p = new Promise<string> ((res, rej) => {
      const random: number = Math.random()
      const cb = random < 0.5 ? res : rej
      const result = random < 0.5 ? "OK" : "Err"

      setTimeout(cb, 1 * 1000, result)
    })

    console.log(p)
    p.then(console.log).catch(console.log) // p.then(data => console.log(data)).catch(e => console.log(e))

    // Promise.all([]) => []  Promise.race([]) => data
    // Promise.resolve(data) => data

    enum ReadyState {
      Unsended = 0, // created xhr but not sended
      Opened = 1, // sended smth by using xhr.open(method, url)
      Sended = 2, // request sended & u get back response headers and status
      ChankContent = 3, // U recieved chank of response data, can appears many times
      Done = 4 // connection closed, all data recieved
    }

    const xhr: XMLHttpRequest = new XMLHttpRequest()

    // xhr.open('GET', 'https://google.com')
    // xhr?.onreadystatechange((): any => {
    //   if(xhr.readyState === 3){
    //     console.log('Data recieved')
    //   }

    //   if(xhr.readyState === 4) return
    // })



  }
}

LC05.execute()