// // namespace Ubtv{
// //   const START: number = Date.now()
// //   interface Tree {
// //     v: number
// //     c?: Tree[]
// //   }
// //   interface SimpleObj {
// //     [key:string]: string
// //   }

// //   Function.prototype.delay = function (delay: number) {
// //     return (...args: any[]) => setTimeout(() => {
// //       this.apply(this, args)
// //     }, delay * 1000)
// //     // return function (...args: any[]) {
// //     //   setTimeout(() => {
// //     //     this(...args)
// //     //   }, delay * 1000)
// //     // }.bind(this)
// //   }

// //   function someFn (): void {
// //     console.log('time', Date.now() - START)
// //     console.log(arguments)
// //   }
  
// //   const f = someFn.delay(2)

// //   f(1,2,3,4)

// //   const tree = [
// //     {
// //       v: 6,
// //       c: [
// //         {
// //           v: 7,
// //           c: [
// //             {
// //               v: 8
// //             }
// //           ]
// //         },
// //         {
// //           v: 9,
// //           c: [
// //             {
// //               v: 10
// //             }
// //           ]
// //         },
// //         {
// //           v: 11,
// //           c: [
// //             {
// //               v: 12,
// //               c: [
// //                 {
// //                   v: 13
// //                 },
// //                 {
// //                   v: 14
// //                 }
// //               ]
// //             }
// //           ]
// //         }
// //       ]
// //     }
// //   ]

// //   function recoursive (tree: Tree[]): number {
// //     let sum: number = 0
    
// //     tree.forEach(({v, c}) => {
// //       sum += v

// //       if(!c) return

// //       sum += recoursive(c) 
// //     })

// //     return sum
// //   }

// //   function iteration (tree: Tree[]): number {
// //     // 1st step if arr is empty
// //     if(!tree.length) return 0

// //     // 2nd step create stack
// //     const stack: Tree[] = []
// //     let sum = 0

// //     // 3rd fullfill stack 1st lvl arr
// //     tree.forEach( node => {
// //       stack.push(node)
// //     })

// //     // 4th if stack doesn't empty
// //     while (stack.length) {
// //       // get last item from stack
// //       const node: Tree = stack.pop()!

// //       // increase sum
// //       sum += node.v

// //       // fullfill stack if we have childrens in tree
// //       node.c?.forEach(n => {
// //         stack.push(n)
// //       })
// //     }

// //     return sum
// //   }

// //   console.log(recoursive(tree))
// //   console.log(iteration(tree))

// //   /**
// //    * write func
// //    * 
// //    * result log
// //    * 5
// //    * 4
// //    * 11
// //    * ...
// //    * 
// //    * sum(5)(-1)(7)...
// //    */

// //   function sum(n: any) {
// //     console.log(n)
// //     return function (a: any) {
// //       return sum(n + a)
// //     }
// //   }

// //   sum(5)(-1)(7)(8)

// //   const obj1: SimpleObj = {
// //     foo: 'foo',
// //     bar: 'bar'
// //   }
// //   const obj2: SimpleObj = {
// //     foo: 'foo1',
// //     some: 'some'
// //   }
// //   const obj3: SimpleObj = {
// //     foo: 'foo',
// //     bar: 'foo'
// //   }

// //   const mergeSameKeyOfObjs1 = (obj1: SimpleObj, obj2: SimpleObj) => {
// //     const keysOfObj1 = Object.keys(obj1)
// //     const keysOfObj2 = Object.keys(obj2)
// //     const length1 = keysOfObj1.length, length2 = keysOfObj2.length

// //     for(let i = 0; i < length1; i += 1) {
// //       let key1 = keysOfObj1[i]

// //       for(let j = 0; j < length2; j += 1) {
// //         let key2 = keysOfObj2[i]

// //         if(key1 === key2) obj1[key1] = obj2[key2]
// //       }
// //     }

// //     return obj1
// //   }
// //   const mergeSameKeyOfObjs2 = (obj1: SimpleObj, obj2: SimpleObj) => {
// //     for(let key in obj1) {
// //       if(obj2.hasOwnProperty(key)) {
// //         obj1[key] = obj2[key]
// //       }
// //     }

// //     return obj1
// //   }

// //   console.log(mergeSameKeyOfObjs1(obj1, obj2))
// //   console.log(mergeSameKeyOfObjs2(obj1, obj2))

// //   // Функция группировки по результату колбек функции
// //   function groupBy<T>(arr: T[], cb: (el: T, idx: number, arr: T[]) => any) {
// //     let result: {[key:string]: T[]} = {}

// //     const length = arr.length
// //     for(let i = 0; i < length; i += 1) {
// //       const key: string = cb(arr[i], i, arr).toString()
       
// //       result[key] ? result[key].push(arr[i]) : result[key] = [arr[i]]
// //     }

// //     return result
// //   }

// //   console.log(groupBy([6.2, 4.2, 6.3], Math.floor))
// // }

// namespace Minin {
//   function bind(cb: (...args: any[]) => any, cobtext: object, ...args: any[]) {
//     return (): any => cb.apply(context, args)
//   }

//   const person = Object.create(
//     <{ birthYear?: any }>{
//       calculateAge() {
//         console.log('Age: ', new Date().getFullYear() - this.birthYear)
//       }
//     }, 
//     {
//       name: {
//         value: 'SomeName',
//         configurable: true, // can we del prop
//         writable: true, // can we rewrite prop
//         enumerable: true // can we see prop in for(in)
//       },
//       birthYear: {
//         value: 1999,
//         configurable: true,
//         writable: true,
//         enumerable: true
//       },
//       age: {
//         get() {
//           return new Date().getFullYear() - this.birthYear
//         },
//         set(value) {
//           console.log('Set age: ', value)
//         }
//       }
//     }
//   )

//   // =================== Proxy =================

//   // ------------------ Objet ------------------
//   interface Obj { [k: string]: any }
//   const obj: Obj = {
//     name: 'Name',
//     age: 25,
//     job: 'SomeJob'
//   }

//   const op = new Proxy(obj, {
//     get(target, prop) {
//       console.log(`Getting prop ${<string>prop}`)

//       if (!(prop in target)) {
//         prop
//           .toString()
//           .split('_')
//           .map(p => target[p])
//           .join(' ')
//       }

//       return target[<string>prop]
//     },
//     set(target, prop, value, reciever): boolean {
//       if(prop in target) {
//         target[<string>prop] = value
//       }
//       else {
//         throw new Error(`No ${<string>prop} field in target`)
//       }

//       return true
//     },
//     has(target, prop) {
//       return ['age', 'name', 'job'].includes(<string>prop)
//     },
//     deleteProperty(target, prop): boolean {
//       console.log('Deleting...', prop)
//       delete target[<string>prop]

//       return true
//     }
//   })

//   op.name // 'Name'
//   // name in op => true
//   // op.name_age_job => Name 25 SomeJob

//   // ---------------- Functions ----------------
//   const log = (text: any): string => `Log: ${text}`

//   const fp = new Proxy(log, {
//     apply(target, thisArg, args) {
//       console.log('Calling fn...')

//       return target.apply(thisArg, [args])
//     }
//   })

//   // fp('dkakd') => 'Calling fn...' \n\r `Log: dkakd`

//   // ------------------ Class ------------------
//   interface IPerson {
//     [key: string | symbol]: any
//   }

//   class Person implements IPerson {
//     constructor(
//       public name: string, 
//       public age: number
//     ) {}
//   }

//   const PersonProxy = new Proxy(Person, {
//     construct(target, args) {
//       const [name, age] = args

//       console.log('construct')
      
//       // return new target(name, age)
//       return new Proxy(new target(name, age), {
//         get(target, prop) {
//           console.log(`Getting prop "${<string>prop}"`)

//           return (target as IPerson)[prop]
//         }
//       })
//     }
//   })

//   const p = new PersonProxy('Maxim', 30)
//   // ---------------- Usecases -----------------

//   // Wrapper
//   const withDefaultValue = (
//     target: any, 
//     defaultValue: number = 0
//   ) => {
//     return new Proxy(target, {
//       get: (obj, prop) => (prop in obj) ? obj[prop] : defaultValue
//     })
//   }

//   const position = withDefaultValue({ x: 24, y: 42 }, 0)

//   console.log(position.x, position.y, position.z)
//   // 24 42 0(by default)

//   // Hidden properties
//   const withHiddenProps = (target: any, prefix: string = '_') => {
//     return new Proxy(target, {
//       has: (obj, prop) => {
//         return (prop in obj) && prop.toString().startsWith(prefix)
//       },
//       ownKeys: obj => Reflect.ownKeys(obj)
//         .filter(p => !p.toString().startsWith(prefix)),
//       get: (obj, prop, receiver) => !(prop in receiver) ? obj[prop] : undefined
//     })
//   }

//   const data = withHiddenProps({
//     name: 'SomeName',
//     age: 25,
//     _uuid: '123123123'
//   })

//   // Optimisation
//   const userData = [
//     {id: 11, name: 'Name1', job: 'Job1', age: 22},
//     {id: 22, name: 'Name2', job: 'Job2', age: 22},
//     {id: 33, name: 'Name3', job: 'Job3', age: 22},
//     {id: 44, name: 'Name4', job: 'Job4', age: 22},
//   ]

//   // const index: {[n: number]: any} = {}
//   // userData.forEach(i => (index[i.id] = i))

//   const IndexArray = new Proxy(Array, {
//     construct(target, [args]) {
//       const index: {[n: number]: any} = {}

//       args.forEach((i: any) => index[i.id] = i)

//       return new Proxy(new target(...args), {
//         get(arr, prop) {
//           switch(prop) {
//             case 'push':
//               return (item: any) => {
//                 index[item.id] = item

//                 arr[prop].call(arr, item)
//               }
//             case 'findById':
//               return (id: any) => index[id]
//             default:
//               return arr[prop]
//           }
//         }
//       })
//     }
//   })

//   const users = new IndexArray(userData)

//   // ================= Generators ================

//   // function* strGenerator() {
//   //   yield 'H'
//   //   yield 'e'
//   //   yield 'l'
//   //   yield 'l'
//   //   yield 'o'
//   //   yield ' '
//   //   yield 'w'
//   //   yield 'o'
//   //   yield 'r'
//   //   yield 'l'
//   //   yield 'd'
//   //   yield '!'
//   // }

//   // function* numberGen(n: number = 20) {
//   //   for (let i = 0; i < n; i += 1){
//   //     yield i
//   //   }
//   // }

//   // const num = numberGen(7)

//   const iterator = {
//     [Symbol.iterator](n = 10) {
//       let i = 0

//       return {
//         next() {
//           const condition = i < n

//           return  { 
//             value: condition ? i++ : undefined, 
//             done: !condition
//           }
//         }
//       }
//     }
//   }
  
//   function* iter(n: number = 10) {
//     for (let i = 0; i < n; i += 1) {
//       yield i
//     }
//   }

//   // ========= Map, Set, WeakMap, WeakSet =========

//   // ---------------------- Map -------------------
//   const standardObj = {
//     name: 'SomeName',
//     age: 22,
//     job: 'Halfstack'
//   }

//   const entries: any[] = [
//     ['name', 'SomeName'],
//     ['age', 22],
//     ['job', 'Halfstack']
//   ]

//   //  Object.entries(standardObj) => entries
//   //  Object.fromEntries(entries) => standardObj
//   const map = new Map(entries)
//   // Map(3) { 'name' => 'SomeName', 'age' => 22, 'job' => 'Halfstack' }
//   map
//     .get('job') // Halfstack

//   map  
//     .set('newField', 42)
//     .set(standardObj, 'Value of object')
//     .set(NaN, 'NaN ??')

//   // for(let entry of map.entries())
//   // for(let entry of map) {
//   //   console.log(entry)
//   // }
//   // for(let entry of map.values())
//   // for(let entry of map.keys())

//   map.forEach((val, key, m) => {
//     console.log(val, key)
//   })

//   /**
//    * SomeName name
//    *  22 age
//    *  Halfstack job
//    *  42 newField
//    *  Value of object { name: 'SomeName', 
//    *  age: 22, job: 'Halfstack' }
//    *  NaN ?? NaN
//    */

//   const arr = Array.from(map)
//   // [
//   //   [ 'name', 'SomeName' ],
//   //   [ 'age', 22 ],
//   //   [ 'job', 'Halfstack' ],
//   //   [ 'newField', 42 ],
//   //   [
//   //     { name: 'SomeName', age: 22, job: 'Halfstack' },
//   //     'Value of object'
//   //   ],
//   //   [ NaN, 'NaN ??' ]
//   // ]

//   const myObj = Object.fromEntries(map.entries())
//   // {
//   //   name: 'SomeName',
//   //   age: 22,
//   //   job: 'Halfstack',
//   //   newField: 42,
//   //   '[object Object]': 'Value of object',
//   //   NaN: 'NaN ??'
//   // }

//   const myUsers = [
//     { name: 'Name1' },
//     { name: 'Name2' },
//     { name: 'Name3' },
//   ]

//   const visits = new Map()
//   visits
//     .set(myUsers[0], new Date())
//     .set(myUsers[1], new Date(Date.now() + 1000 * 60))
//     .set(myUsers[2], new Date(Date.now() + 5000 * 60))

//   function lastVisits(user: any) {
//     return visits.get(user)
//   }

//   console.log(lastVisits(myUsers[1]))

//   // ---------------------- Set -------------------
//   const set = new Set([1,1,1,2,3,3,4,5,6,7,8,99,9,9])

//   console.log(set)
//   // Set(10) { 1, 2, 3, 4, 5, 6, 7, 8, 99, 9 }

//   // set.has(42)
//   // set.size
//   // set.delete(1)
//   // set.clear()
//   // set.keys == set.values

//   // ------------------- WeakMap -----------------
//   let testObj: {} | null = { name: '1231'}

//   const weakMap = new WeakMap([
//     [testObj, 'some data']
//   ])
//   // methods get set delete has

//   testObj = null
  
//   weakMap.get(testObj)

//   const cache = new WeakMap() 
  
//   function casheUser (user: any) {
//     if(!cache.has(user)) {
//       cache.set(user, Date.now())
//     }
//     return cache.get(user)
//   }
  

//   let user1 = {name: 'Name1'}
//   let user2 = {name: 'Name2'}

//   casheUser(user1)
//   casheUser(user2)

//   user1 = null

//   console.log(cache.has(user1))
//   console.log(cache.has(user2))

//   // ------------------- WeakSet -----------------
//   // works with keys type obj works like weakMap

//   // ================ XHR, fetch ==================

//   // less 14

//   export function execute(): void {
//     // console.log(data)
//     // console.log(data.name)
//     // console.log(data.age)
//     // console.log(data._uuid)
//     // // console.log(index)
//     // console.log(users)

//     // console.log(num.next())
//     // for (const k of iterator)  console.log(k)
//     // for (const k of iter(6))  console.log(k)

//     // console.log(map)

//     // map.forEach((val, key, m) => {
//     //   console.log(val, key)
//     // })
    
//     // console.log(arr)
//     // console.log(myObj)
//     const arr1: number[] = [51, 56, 58, 59, 61]
//     let k = 2
//     // const newArr = arr1.reduce((acc, el, idx, arr) => {
    

//     //   for(let i = idx + 1; i < arr.length - k; i += k) {
//     //     res.push(el)
//     //     for(let j = 0; j < k; j += 1) {
//     //       res.push(arr[j])
//     //     }
//     //   }

//     //   return acc.push([...res])
//     // }, [])

//     // var facts: any = [];

//     // function fact(N: number) {
//     //   if (N == 0 || N == 1) return 1;
//     //   if (facts[N]) return facts[N];
//     //   facts[N] = N * fact(N - 1);
//     //   return facts[N];
//     // }

//     // function C(n: number, k: number) {
//     //   return fact(n) / fact(k) / fact(n - k);
//     // }

//     // function combination(index, k, A) {
//     //   var res = [0];
//     //   var n = A.length;
//     //   var s = 0;
//     //   for (var t = 1; t <= k; t++) {
//     //     var j = res[t - 1] + 1;
//     //     while ((j < (n - k + t)) && ((s + C(n - j, k - t)) <= index)) {
//     //       s += C(n - j, k - t);
//     //       j++;
//     //     }
//     //     res.push(j);
//     //   }
//     //   res.splice(0, 1);
//     //   return res;
//     // }

//     // console.log(arr1.reduce((acc, el) => {
//     //   const result: any[] = []
//     //   result.push([el])

//     //   for(let i = idx + 1; i < arr.length; i += k) {

//     //   }

//     //   return [...acc, ...result]
//     // }, []))

//   }
// }

// Minin.execute()

namespace Tasks {
  class Tasks {
    // Написать функцию, которая определяет уникален ли каждый элемент строки.
    private task1() {
      function isCharUniq (str: string): boolean {
        // return new Set(string).size === string.length

        const strArr = str.split('')
        const setArr = [...new Set(strArr)]

        return strArr.length === setArr.length
      }
      console.log(
        isCharUniq('abcdef'),
        isCharUniq('1234567'),
        isCharUniq('abcABC'),
        isCharUniq('abcadef')
      )
    }

    // Написать функцию, которая принимает массив с вложенными массивами, сделать из него одномерный массив
    private task2() {
      function flatten (arr: any[]): any[] {
       return arr.reduce((acc, el) => acc.concat(Array.isArray(el) ? flatten(el) : el), [])
      }

      console.log( flatten([[1],[[2,3]],[[[[4]]]]])
        
      )
    }

    // Написать функцию, которая удаляет все повторяющиеся элементы в строке.
    private task3() {
      function noDublications (str: string): string {
        // return [...new Set(str)].join('')
        let newStr = ''
        
        for(let i = str.length; i; i -= 1) {
          const idx = i - 1

          if(!str.slice(0, idx).includes(str[idx])) {
            newStr = str[idx] + newStr
          }
          else continue
        }

        return newStr
      }

      console.log(
        noDublications('abcd'),
        noDublications('aabbcccdd'),
        noDublications('abcddbca'),
        noDublications('abababcdcdcd'),
      )
    }

    // Написать функцию, которая определяет какая строка встречается чаще всего в массиве.
    private task4() {
      function highestFrequency (arr: string[]): string {
        const map = new Map()

        arr.forEach(el => map.set(el, map.has(el) ? map.get(el) + 1 : 1))

        return Array.from(map)
          .sort(([_1, prev], [_2, next]) => next - prev)[0][0]

      }

      console.log(
        highestFrequency(['a', 'b', 'c', 'c', 'd', 'e']),
        highestFrequency(['abc', 'def', 'abc', 'def', 'abc']),
        highestFrequency(['abc', 'def']),
        highestFrequency(['abc', 'abc', 'def', 'def', 'def', 'ghi', 'ghi', 'ghi'])
      )
    }

    // Написать функцию, которая определяет повернута ли строка.
    private task5() {
      function isReversed (str1: string, str2: string): boolean {
        // return (str1 + str1).includes(str2) && str1.length === str2.length
        const normalizeAtr1 = str1.split('').sort().join('')
        const normalizeAtr2 = str2.split('').sort().join('')

        return normalizeAtr1 === normalizeAtr2
      }

      console.log(
        isReversed('javascript', 'scriptjava'),
        isReversed('javascript', 'iptjavascr'),
        isReversed('javascript', 'java')
      )
    }

    // Написать функцию, которая определяет является ли массив подмножеством другого массива.
    private task6() {
      

      function arrSubset (arr1: any[], arr2: any[]): boolean {
        let result = true

        arr2.forEach(el => {
          const idx = arr1.indexOf(el)

          if(idx === -1) result = false

          delete arr1[idx]
        })

        return result
      }

      console.log(
        arrSubset([1,2,3], [3,2,1]),
        arrSubset([2,1,1,3], [3,2,1]),
        arrSubset([1,1,1,3], [1,3,3]),
        arrSubset([1,2], [1,2,3])
      )
    }

    // Написать функцию, которая определяет является ли каждый элемент массива анаграммной других.
    private task7() {
      function allAnagrams (arr: any[]): boolean {
        const anagram = arr[0].split('').sort().join('')

        return arr
          .every(el => el.split('').sort().join('') === anagram)
      }

      console.log(
        allAnagrams(['abcd', 'bdac', 'cabd']),
        allAnagrams(['abcd', 'bdXc', 'cabd']),
      )
    }

    // Написать функцию, которая перевернет матрицу 3х3 на 90 градусов.
    private task8() {
      type Degrease = 0 | 90 | 180 | 270 | 360

      function rotate <T>(arr: T[][]): T[][] {
        const result: T[][] = []
        
        arr.forEach((subArr) => {
          subArr.forEach((el, idx) => {
            result[idx] ?
              result[idx].push(el) :
              result[idx] = [el]
          })
        })

        return result.map(el => el.reverse())
      } 

      function rotateBy <T>(arr: T[][], deg: Degrease): T[][] {
        let rotates = deg / 90
        let result: T[][] = arr

        while(rotates) {
          result = rotate(result)
          rotates--
        }

        return result 
      }

      const matrix = [
        [1, 2, 3],  // 0
      // 0  1  2
        [4, 5, 6],  // 1
      // 0  1  2
        [7, 8, 9]   // 2
      // 0  1  2
      ]

      console.log(
        rotate(matrix),
        rotateBy(matrix, 360),
      )
    }

    // =================== Algoritms ====================

    // Написать функцию, которая принимает отсортированный массив с числами и число. Необходимо вернуть индекс числа, если оно есть в массиве или -1 если нет.
    private task9() {
      function search (arr: number[], target: number): number {
        // return arr.indexOf(target)           // *1* O(n)

        // const length = arr.length            //  *2* O(n)

        // for(let i = 0; i < length; i += 1) {
        //   if(arr[i] === target) return i
        // }

        // return -1

        let start = 0, end = arr.length - 1   // *3* O(log(n))

        if (target < arr[start] || target > arr[end]) {
          return -1
        }

        while(true) {
          if(target === arr[start]) {
            return start
          }
          if(target === arr[end]) {
            return end
          }
          // checks is elements between start, end
          if(end - start <= 1) {
            return -1
          }

          const middle = Math.floor((start + end) / 2)

          if (target > arr[middle]) {
            start = middle + 1
          } else if (target < arr[middle]) {
            end = middle - 1
          } else {
            return middle
          }

        }
      }

      console.log(
        search([1, 3, 6, 13, 17], 13),
        search([1, 3, 6, 13, 17], 12),
      )
    }

    // Написать функцию, которая проверяет сбаллансированны ли скобки (){}[]. Возвращает булеану.
    private task10() {
      // function isBallanced (str: string): boolean {
      //   // const length = str.length
      //   // const increase = ['{', '(', '[']
      //   // const decrease = ['}', ')', ']']
      //   // const obj = { 
      //   //   '{}': 0, 
      //   //   '()': 0, 
      //   //   '[]': 0, 
      //   //   increase(char: string) {
      //   //     const prop = Object
      //   //       .keys(this)
      //   //       .find(el => el.includes(char))
            
      //   //     if(prop) this[prop]++
      //   //   },
      //   //   decrease(char: string) {
      //   //     const prop = Object
      //   //       .keys(this)
      //   //       .find(el => el.includes(char))
            
      //   //     if(prop) this[prop]--
      //   //   },
      //   //   isNegative(): boolean {
      //   //     return Object.values(this)
      //   //       .filter(el => typeof el === 'number')
      //   //       .some(el => el < 0)
      //   //   },
      //   //   isZero(): boolean {
      //   //     return Object.values(this)
      //   //       .filter(el => typeof el === 'number')
      //   //       .every(el => el == 0)
      //   //   }
      //   // }

      //   // for(let i = 0; i < length; i += 1) {
      //   //   const char = str[i]

      //   //   if(increase.includes(char)) obj.increase(char)

      //   //   if(decrease.includes(char)) obj.decrease(char)

      //   //   if(obj.isNegative()) return false
      //   // }

      //   // return obj.isZero()
      //   // const length = str.length
      //   // const start: string = '([{'
      //   // const end: string = ')]}'
      //   // const map = {
      //   //   '{': '}',
      //   //   '(': ')',
      //   //   '[': ']'
      //   // }
      //   // const queue: string[] = []

      //   // for(let i = 0; i < length; i += 1) {
      //   //   const char: string = str[i]

      //   //   if(start.includes(char)) {
      //   //     queue.push(char)
      //   //   } else if (end.includes(char)) {
      //   //     const last = queue.pop()

      //   //     if(map[char] !== last) {
      //   //       return false
      //   //     }
      //   //   }
      //   // }

      //   // return true
      // }

      // console.log(
      //   isBallanced(']()['),
      //   isBallanced('[()]{}[]'),
      // )
    }

    // Очередь с О(1) сложностью операций, создайте очередь в которой будут реализованы операции на добавление элементов в конец очереди, удаление первого элемента и  вычисление размера очереди О(1) сложностью
    private task11() {
      class Queue {
        private storage: {[key:number]: any} = {}
        private last: number = 0
        private first: number = 0


        constructor() {}

        enqueue(item: any) {
          this.storage[this.last] = item
          this.last++
        }

        dequeue() {
          if (!this.size) return

          const value = this.storage[this.first]

          delete this.storage[this.first]
          this.first++

          return value
        }

        get size() {
          return this.last - this.first
        }
      }

      class LinkedList {
        protected length: number = 0
        protected head: any
        protected tail: any

        addToTail(value: any) {
          const node = {
            value,
            next: null
          }

          if (!this.length) {
            this.head = node
            this.tail = node
          }
          else {
            this.tail = node
          }
        }

        removeFromHead() {
          if(!this.length) return

          const value = this.head.value
          this.head = this.head.next

          this.length--

          return value
        }

        protected _size() {
          return this.length
        }
      }

      class List extends LinkedList {
        public enqueue: any
        public dequeue: any

        constructor() {
          super()

          this.enqueue = this.addToTail
          this.dequeue = this.removeFromHead
        }

        get size() {
          return super._size()
        }
      }
    }

    // Напишите функцию, котораяю будет проверять на глубокое равенство 2 входящих параметра.
    private task12() {
      function deepEqual(el1?: any, el2?: any): boolean {
        // return JSON.stringify(el1) === JSON.stringify(el2)

        if(isNaN(el1) && isNaN(el2)) return true

        if(typeof el1 !== typeof el2) {
          return false
        }

        if(typeof el1 !== 'object' || el1 === null || el2 === null) {
          return el1 === el2
        }

        if(Object.keys(el1).length !== Object.keys(el2).length) {
          return false
        }

        for (const key of Object.keys(el1)) {
          if(!deepEqual(el1[key], el2[key])) {
            return false
          }
        }

        return true
      }
      const source = {a: 1, b: {c: 1}}
      const test1 = {a: 1, b: {c: 1}}
      const test2 = {a: 1, b: {c: 2}}
      console.log(
        deepEqual(source, test1),
        deepEqual(source, test2),
        deepEqual(NaN, NaN),
        deepEqual('test', 'test!'),
        deepEqual()
      )
    }

    // Напишите функцию, которая будет генерировать массив Фибоначчи, заданной длинны
    private task13() {
      function fibonacci (n: number): number[] {
        const oneArr = (n: number): number[] => {
          const result: number[] = []

          while(n) {
            result.push(1)
            n--
          }

          return result
        }

        if(n <= 2) return oneArr(n)

        let result: number[] = oneArr(2)
        
        while(n > 2) {
          const length = result.length

          result.push(result[length - 1] + result[length - 2])
          n--
        }

        return result
      }

      const cachedFibonacci = (function() {
        const seq = [1,1]

        return function(n: number): number[] {
          if(seq.length >= n) {
            return seq.slice(0, n)
          }
          while(seq.length < n) {
            seq.push(seq[seq.length - 1] + seq[seq.length - 2])
          }

          return seq
        }
      })()

      console.log(fibonacci(8))
    }

    // Написать свою функцию байнд
    private task14() {
      Function.prototype.myBind = function(context, ...args) {
        // const self = this

        return (...rest: any) => {
          return this.call(context, ...args.concat(rest))
        }
      }
    }

    public execute () {
      this.task1()
      this.task2()
      this.task3()
      this.task4()
      this.task5()
      this.task6()
      this.task7()
      this.task8()
      this.task9()
      this.task10()
      this.task12()
      this.task13()
    }
  }

  const execute = new Tasks().execute()
}