type Sort = 'asc' | 'desc'
interface Function {
  delay(delay: number): any
  log(): void
  myBind(context, ...args): any
}
interface Array {
  getUsersByYear(year: number): arr[]
  getCondidatesByUnreadMsg(messages: number): arr[]
  getCondidatesByGender(gender: string): arr[]
  searchCandidatesByPhoneNumber(serchPhone: string): arr[]
  getCandidateById(searchId: string): any
  sortCandidatesArr(sortBy?: Sort): any[]
  getEyeColorMap(): {[key:string]: any[]}
}

interface Socket {
  id?: number
}
