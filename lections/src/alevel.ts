import { condidateArr } from './candidates'

function log(...args: any): void {
  console.log(...args)
}
function delay (cb: (...args: any[]) => any, sec: number): void {
  setTimeout(cb, sec * 1000)
}
Function.prototype.log = function (...args: any[]): void {
  const result = this.call(this, ...args)

  console.log(result)

  return result
}
// =============== HW01 ===============
namespace HW01 {
  interface OptionsHW01 {
    length: number
    factorial: number
    week: number
    room: number
    num: number
  }
  
  const optionsHW01: OptionsHW01 = {
    length: 10,
    factorial: 10,
    week: 8,
    room: 150,
    num: 8
  }
  
  class HW01 {
    static round(num: number): number {
      const [integer, fractional] = num
        .toString().split('.').map(el => Number(el))
  
      return fractional ? integer + 1 : integer
    }
  
    // Создать цикл на 10 итераций. На каждой итерации если i четное, то вывести в консоль слово Fiz, если i не четное, то вывести в консоль слово Buz, если i кротное цифре 3, то вывести FizBuz.
    private task1(length: number): void {
      let count: number = 0
  
      while(count < length) {
        let result: string
  
        if (count % 3 === 0) result = 'FizBuz'
        else if (count % 2) result = 'Buz'
        else result = 'Fiz'
  
        log(result)
        count++
      }
    }
  
    //Написать логику нахождения факториала числа 10.
    private task2(num: number): number {
      let result: number = 1
  
      while(num) {
        result *= num
        num--
      }
  
      return result
    }
  
    //В пачке бумаги 500 листов. За неделю в офисе расходуется 1200 листов. Какое наименьшее количество пачек бумаги нужно купить в офис на 8 недель
    private task3(weeks: number): number {
      return HW01.round(weeks * 1200 / 500)
    }
  
    //Создать функцию, которая выведет в консоль номер этажа и номер подъезда по номеру квартиры. Этажей у нас 9, квартир на этаже по 3
    private task4(room: number): void {
      const enterance: number = HW01.round(room / 3 / 9)
      const floor: number = HW01.round(room / enterance / 3)
  
      log(enterance, floor)
    }
  
    //Вывести в консоль пирамиду. Переменная указывает количество строк из которых построится пирамида. Пирамида должна строится в одинаковом визуально виде между собой, но строго учитывая кол-во строк
    private task5(num: number): void {
      const char1: string = '-'
      const char2: string = '#'
  
      for(let i = 0; i < num; i += 1) {
        const part1: string = char1.repeat(num - i - 1)
        const part2: string = char2.repeat(1 + 2 * i)
  
        log(part1 + part2 + part1)
      }
    }
  
    public execute (obj: OptionsHW01): void {
      const {length, factorial, week, room, num } = obj
  
      this.task1(length)
      log(this.task2(factorial))
      log(this.task3(week))
      this.task4(room)
      this.task5(num)
    }
  }
  
  export const result = function (): void {
    return new HW01().execute(optionsHW01)
  }
}

// HW01.result()

// =============== HW02 ===============
namespace HW02 {
  type Days = 1 | 2 | 3 | 4| 5 | 6 | 7
  type ZeroOne = 0 | 1
  interface NamesOfDays {
    ru: string[]
    en: string[]
  }
  interface LangDay {
    lang: keyof NamesOfDays
    day: Days
  }
  interface Capitals {
    [key: string]: string
  }
  interface OptionsHW02 {
    capitals: Capitals
    length: number
    langDay: LangDay,
    numsArr: number[],
    zeroOneArr: ZeroOne[]
  }
  
  const capitals: Capitals = {
    'Киев': 'Украина',
    'Нью-Йорк': 'США',
    'Амстердам': 'Нидерланды',
    'Берлин': 'Германия',
    'Париж': 'Франция',
    'Лиссабон': 'Португалия',
    'Вена': 'Австрия'
  }
  
  const namesOfDays: NamesOfDays = {
    ru: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница' , 'Суббота' , 'Воскресенье'],
    en: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday' , 'Satyrday' , 'Sunday']
  }
  
  const optionsHW02: OptionsHW02 = {
    capitals,
    length: 6,
    langDay: {
      lang: 'en',
      day: 1
    },
    numsArr: [19, 5, 42, 2, 77],
    zeroOneArr: [1, 1, 1, 1]
  }
  
  class HW02 {
    // Дан объект с городами и странами. Вывести масив значения в котором будут строки преобразованные в данный формат: <Столица> - это <Страна>.
    private task1(obj: Capitals): Array<string> {
      return Object.entries(obj).map(([key, value]): string => `${key} - это ${value}`)
    }
  
    //Создать функцию которая выведет многомерный массив A. Данный массив содержит в себе список других массивов B. Массивы B должны содержать по 3 значения. Максимальное значение (в примере это переменная amount) должно делится на 3 нацело.
    private task2(length: number): Array<Array<number>> {
      const result = []
  
      for(let i = 0; i < length; i += 3 ) {
        result.push([i + 1, i + 2, i + 3])
      }
      return result
    }
  
    //Cоздать объект с названиями дней недели. Где ключами будут ru и en, a значением свойства ru будет массив с названиями дней недели на русском, а en - на английском. После написать функцию которая будет выводить в консоль название дня недели пользуясь выше созданным объектом. Все дни недели начинаются с 1 и заканичаются цифрой 7 (1- понедельник, 7 - воскресенье). Функция хранит переменную lang - название языка дня недели и переменную day - число дня недели. 
    private task3(lang: keyof NamesOfDays, day: Days): string {
      const obj: NamesOfDays = Object.assign({}, namesOfDays)
  
      return obj[lang][day - 1]
    }
  
    // Создайте функцию, которая возвращает сумму двух наименьших положительных чисел из массива минимум 4 положительных целых чисел. Не передаются числа с плавающей запятой или неположительные целые числа.
    private task4(arr: number[]): number | string {
      const cloneArr: number[] = [...arr]
      const length: number = arr.length
  
      for (let i: number = 0; i < length - 1; i++) { 
        for (let j: number = 0; j < length - 1 - i; j++) { 
          if (cloneArr[j+1] < cloneArr[j]) { 
            let t: number = cloneArr[j + 1]
            cloneArr[j + 1] = cloneArr[j]
            cloneArr[j] = t
          }
        }
      }
  
      for(let i: number = 0; i < length - 1; i += 1) {
        for(let j: number = 1; j < length; j += 1) {
          if (cloneArr[i] !== cloneArr[j]) {
            return cloneArr[i] + cloneArr[j]
          }
        }
      }
  
      return 'Values are same'
    }
  
    // Дан массив единиц и нулей, преобразуйте эквивалентное двоичное значение в целое число. Например: [0, 0, 0, 1]рассматривается как 0001двоичное представление 1. Однако массивы могут иметь разную длину, не ограничиваясь только ими 4
    task5(arr: ZeroOne[]): number {
      const length: number = arr.length
      let result: number = 0
  
      arr.forEach((el, idx) => {
        result += el * 2 ** (length - 1 - idx)
      })
  
      return result
    }
  
    public execute (obj: OptionsHW02): void {
      const {capitals, length, langDay: {lang, day}, numsArr, zeroOneArr} = obj
  
      log(this.task1(capitals))
      log(this.task2(length))
      log(this.task3(lang, day))
      log(this.task4(numsArr))
      log(this.task5(zeroOneArr))
    }
  }
  
  export const result = function (): void {
    return new HW02().execute(optionsHW02)
  }
}

// HW02.result()
// =============== HW03 ===============

namespace HW03 {
  type Gender = 'male' | 'female'
  type Test = IEmployee | string
  interface IEmployee {
    id: number
    name: string
    surname: string
    salary: number 
    workExperience: number
    isPrivileges: boolean
    gender: Gender
  }
  interface Emploee {
    getFullInfo(value?: IEmployee): void | Emploee
  }

  class Employee implements IEmployee {
    id: number
    name: string
    surname: string
    
    constructor(
      id: number, 
      name: string, 
      surname: string, 
      public salary: number, 
      public workExperience: number, 
      public isPrivileges: boolean, 
      public gender: Gender
    ) {
      this.id = id
      this.name = name
      this.surname = surname
    }
  }

  class EmployeeWithFullName extends Employee {
    constructor(
      public id: number, 
      public name: string, 
      public surname: string, 
      public salary: number, 
      public workExperience: number, 
      public isPrivileges: boolean, 
      public gender: Gender
    ){
      super(id, name, surname, salary, workExperience, isPrivileges, gender)
    }
    getFullName():string {
      return `${this.surname} ${this.name}`
    }
  }

  const emplyeeArr: IEmployee[] = [
    {
      id: 1,
      name: 'Денис',
      surname: 'Хрущ',
      salary: 1010, 
      workExperience: 10, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: 'male'
    },
    {
      id: 2,
      name: 'Сергей',
      surname: 'Войлов',
      salary: 1200, 
      workExperience: 12, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: 'male'
    },
    {
      id: 3,
      name: 'Татьяна',
      surname: 'Коваленко',
      salary: 480, 
      workExperience: 3, /// стаж работы (1 = один месяц)
      isPrivileges: true, /// льготы
      gender: 'female'
    },
    {
      id: 4,
      name: 'Анна',
      surname: 'Кугир',
      salary: 2430, 
      workExperience: 20, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: 'female'
    },
    {
      id: 5,
      name: 'Татьяна',
      surname: 'Капустник',
      salary: 3150, 
      workExperience: 30, /// стаж работы (1 = один месяц)
      isPrivileges: true, /// льготы
      gender: 'female'
    },
    {
      id: 6,
      name: 'Станислав',
      surname: 'Щелоков',
      salary: 1730, 
      workExperience: 15, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: 'male'
    },
    {
      id: 7,
      name: 'Денис',
      surname: 'Марченко',
      salary: 5730, 
      workExperience: 45, /// стаж работы (1 = один месяц)
      isPrivileges: true, /// льготы
      gender: 'male'
    },
    {
      id: 8,
      name: 'Максим',
      surname: 'Меженский',
      salary: 4190, 
      workExperience: 39, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: 'male'
    },
    {
      id: 9,
      name: 'Антон',
      surname: 'Завадский',
      salary: 790, 
      workExperience: 7, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: 'male'
    },
    {
      id: 10,
      name: 'Инна',
      surname: 'Скакунова',
      salary: 5260, 
      workExperience: 49, /// стаж работы (1 = один месяц)
      isPrivileges: true, /// льготы
      gender: 'female'
    },
    {
      id: 11,
      name: 'Игорь',
      surname: 'Куштым',
      salary: 300, 
      workExperience: 1, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: 'male'
    },
  ]  

  class HW03 {
    //Создать функцию - конструктор, которая будет иметь внутри все свойства обьекта emplyee из массива emplyeeArr
    private task1(obj: IEmployee): Employee {
      const {id, name, surname, salary, workExperience, isPrivileges, gender} = obj

      return new Employee(id, name, surname, salary, workExperience, isPrivileges, gender)
    }

    //Добавить функции - конструктору метод (помним про prototype): getFullName который вернет полное имя начиная с фамилии в виде строки
    private task2(obj: IEmployee): EmployeeWithFullName {
      const {id, name, surname, salary, workExperience, isPrivileges, gender} = obj

      return new EmployeeWithFullName(id, name, surname, salary, workExperience, isPrivileges, gender)
    }

    //Создать новый массив на основе emplyeeArr в котором будут содержаться те же обьекты, но созданные функцией - конструктором Emploee. Новый массив должен содержать имя emplyeeConstructArr.
    private task3(arr: IEmployee[]): EmployeeWithFullName[] {
      return arr.map(obj => this.task2(obj))
    }

    //Создать функцию которая вернет массив со всеми полными именами каждого employee, содержащегося в emplyeeConstructArr
    private task4(arr: EmployeeWithFullName[]): Array<string> {
      return arr.map(employee => employee.getFullName())
    }

    //Создать функцию которая вернет среднее значение зарплаты всех employee
    private task5(arr: EmployeeWithFullName[]): number {
      return arr.reduce((acc, {salary}) => acc + salary, 0) / arr.length
    }

    //Создать функцию которая выберет наугад работника из массива emplyeeConstructArr. Вы должны учитывать в функции длинну массива, так как если работников 7, а рандомное число будет равно больше 7, то результат будет undefined. Вы можете использовать обьявленную функцию в сомой же себе. Подсказка Math.random
    private task6(arr: EmployeeWithFullName[]): EmployeeWithFullName {
      return arr[Math.floor(Math.random() * arr.length)]
    }

    private task7(arr: EmployeeWithFullName[]): EmployeeWithFullName {
      return arr[Math.floor(Math.random() * arr.length)]
    }

    private task8(arr: EmployeeWithFullName[]): EmployeeWithFullName {
      return arr[Math.floor(Math.random() * arr.length)]
    }

    public execute(): void {
      const emplyeeConstructArr: EmployeeWithFullName[] = this.task3(emplyeeArr)

      log(this.task1(emplyeeArr[0]))
      log(this.task2(emplyeeArr[1]).getFullName())
      log(emplyeeConstructArr)
      log(this.task4(emplyeeConstructArr))
      log(this.task5(emplyeeConstructArr))
      log(this.task6(emplyeeConstructArr))
    }
  }

  export const result = function (): void {
    return new HW03().execute()
  }
}

// HW03.result()
// =============== HW04 ===============
namespace HW04 {
  interface IStudent {
    name: string
    surname: string
    ratingPoint: number
    schoolPoint: number
    course: number
  }
  interface ExtendStudent extends IStudent {
    id?: number
    isSelfPayment?: boolean
  }

  const studentArr: IStudent[] = [
    {
      name: 'Сергей',
      surname: 'Войлов',
      ratingPoint: 1000,
      schoolPoint: 1000,
      course: 2,
    },
    {
      name: 'Татьяна',
      surname: 'Коваленко',
      ratingPoint: 880,
      schoolPoint: 700,
      course: 1,
    },
    {
      name: 'Анна',
      surname: 'Кугир',
      ratingPoint: 1430,
      schoolPoint: 1200,
      course: 3,
    },
    {
      name: 'Станислав',
      surname: 'Щелоков',
      ratingPoint: 1130,
      schoolPoint: 1060,
      course: 2,
    },
    {
      name: 'Денис',
      surname: 'Хрущ',
      ratingPoint: 1000,
      schoolPoint: 990,
      course: 4,
    },
    {
      name: 'Татьяна',
      surname: 'Капустник',
      ratingPoint: 650,
      schoolPoint: 500,
      course: 3,
    },
    {
      name: 'Максим',
      surname: 'Меженский',
      ratingPoint: 990,
      schoolPoint: 1100,
      course: 1,
    },
    {
      name: 'Денис',
      surname: 'Марченко',
      ratingPoint: 570,
      schoolPoint: 1300,
      course: 4,
    },
    {
      name: 'Антон',
      surname: 'Завадский',
      ratingPoint: 1090,
      schoolPoint: 1010,
      course: 3
    },
    {
      name: 'Игорь',
      surname: 'Куштым',
      ratingPoint: 870,
      schoolPoint: 790,
      course: 1,
    },
    {
      name: 'Инна',
      surname: 'Скакунова',
      ratingPoint: 1560,
      schoolPoint: 200,
      course: 2,
    }
  ]

  class Student {
    static _id: number = 1
    static listOfStudents: ExtendStudent[] = []

    constructor(student: IStudent) {
      this.addStudent({
        id: Student._id++,
        ...student,
        isSelfPayment: false
      })
    }

    private addStudent (student: ExtendStudent): void {
      Student.listOfStudents.push(student)

      Student.listOfStudents = Student.listOfStudents
        .sort((prev, next) => {
          let result: number = next.ratingPoint - prev.ratingPoint

          if(result) return result
          else {
            return next.schoolPoint - prev.schoolPoint
          }
        })
        .map((student, idx) => {
          return {
            ...student,
            isSelfPayment: idx < 5 ? true : false
          }
        })
        .sort((prev, next) => prev.id! - next.id!)
    }
  }

  class CustomString {
    reverse(str: string): string {
      let newStr: string = ''

      for(let i = str.length; i; i -= 1) newStr += str[i - 1]

      return newStr
    }

    ucFirst(str: string): string {
      return str[0].toUpperCase() + str.slice(1)
    }
    ucWords(str: string): string {
      return str
        .split(' ')
        .map(word => this.ucFirst(word))
        .join(' ')
    }
  }

  class Validator {
    static email: RegExp = /^[a-zA-z._]+@.\w{3,4}.\w{2,3}$/
    static domain: RegExp = /^[a-zA-z._]+.\w{2,3}$/
    static date: RegExp = /^\d{1,2}.\d{1,2}.\d{4}$/
    static phone: RegExp = /^[+]38\s[(]\d{3}[)]\s\d{3}-\d\d-\d\d$/

    checkIsEmail(str: string): boolean {
      return Validator.email.test(str)
    }
    checkIsDate(str: string): boolean {
      return Validator.date.test(str)
    }
    checkIsDomain(str: string): boolean {
      return Validator.domain.test(str)
    }
    checkIsPhone(str: string): boolean {
      return Validator.phone.test(str)
    }
  }

  class HW04 {
    /*
    Задача - создать класс Student который принимает аргументом в конструкторе объект enrollee (абитурент). У экземпляра класса Student должны быть поля:
    id - уникальный идентификатор студента (генерируется при создании экземпляра и начинается с 1);
    name - имя студента (передаем в объекте enrollee);
    surname - фамилия студента (передаем в объекте enrollee);
    ratingPoint - рейтинг студента по результатам вступительных экзаменов (передаем в объекте enrollee);
    schoolPoint - рейтинг студента по результатам ЗНО (передаем в объекте enrollee);
    isSelfPayment - если true, то студент на контракте, если false - на бюджете (генерируется по логике указанной ниже).
    Id генерируется автоматически при создании экземпляра Student. isSelfPayment определяется по параметру "ratingPoint". Если ratingPoint больше или равен 800, то студент может быть на бюджет, но бюджет он может получить только если его "ratingPoint" не меньше чем у других студентов в массиве. Студентов которые на бюджете не должно быть больше чем 5 в массиве. То есть если "ratingPoint" больше чем хоть у одного из 5 бюджетников то мы присваиваем isSelfPayment = false. И в этот момент студент из массива который имел isSelfPayment = false, но его ratingPoint меньше чем у остальных 5 бюджетников, с нашим включительно, то ему делаем isSelfPayment = true, то есть переводим этого неудачника на контракт. Что делать если у 6-рых студентов баллы 1000? Ну имеется ввиду, если 2 человека с одинаковыми баллами ratingPoint борются за 5 бюджетное место? В таком случае смотрим на schoolRating, у кого он больше тот и на бюджете.
    При каждом новом вызове конструктора, все логика должна отрабатывать таким образом, что пересчет будет изменяться с новым студентом включительно.
    const studentsArr = [... массив со студентами].
    ​
    class Student {
        constructor(enrollee){
        
    }; 
    ​
    Student.listOfStudents 
    /// [] - пустой изначально
    ​
    // Вызвал класс
    new Student({ name: 'Valeriy', ...}) 
    ​
    ​
    Student.listOfStudents 
    /// [{ id: 1,  name: 'Valeriy', ...},  и т.д.]
    ​
    // Снова вызвал 
    new Student({ name: 'Maks', ...}) 
    ​
    Student.listOfStudents 
    /// [{ id: 1,  name: 'Valeriy', ...}, { id: 2,  name: 'Maks', ...},  и т.д.]
    ​
    etc
    */
    private task01 (): void {
      studentArr.forEach(student => new Student(student))

      log(Student.listOfStudents)
    }
    /*
    Реализуйте класс CustomString, который будет иметь следующие методы: метод reverse(), который параметром принимает строку, а возвращает ее в перевернутом виде, метод ucFirst(), который параметром принимает строку, а возвращает эту же строку, сделав ее первую букву заглавной и метод ucWords, который принимает строку и делает заглавной первую букву каждого слова этой строки.
    const myString = new CustomString();
    ​
    myString.reverse('qwerty'); //выведет 'ytrewq'
    myString.ucFirst('qwerty'); //выведет 'Qwerty'
    myString.ucWords('qwerty qwerty qwerty'); //выведет 'Qwerty Qwerty Qwerty
    */
    private task02 () {
      const myString = new CustomString()

      log(myString.reverse('qwerty'))
      log(myString.ucFirst('qwerty'))
      log(myString.ucWords('qwerty qwerty qwerty'))
    }
    /*
    Реализуйте класс Validator, который будет проверять строки. К примеру, у него будет метод checkIsEmail() параметром принимает строку и проверяет, является ли она емейлом или нет. Если является - возвращает true, если не является - то false. Кроме того, класс будет иметь следующие методы: метод checkIsDomain для проверки домена, метод checkIsDate для проверки даты и метод checkIsPhone для проверки телефона:
    var validator = new Validator();
    ​
    validator.checkIsEmail('vasya.pupkin@gmail.com'); // true
    validator.checkIsDomain('google.com'); // true
    validator.checkIsDate('30.11.2019'); // true
    validator.checkIsPhone('+38 (066) 937-99-92'); // если код страны Украинский, то возвращаем true иначе false
    */
    private task03 () {
      const validator = new Validator()

      log(validator.checkIsEmail('vasya.pupkin@gmail.com'))
      log(validator.checkIsDomain('google.com'))
      log(validator.checkIsDate('30.11.2019'))
      log(validator.checkIsPhone('+38 (066) 937-99-92'))
    }

    public execute () {
      this.task01()
      this.task02()
      this.task03()
    }
  }

  export const result = function (): void {
    return new HW04().execute()
  }
}

// HW04.result()

// =============== HW05 ===============
namespace HW05 {
  class HW05 {
    /*
    Напиши функцию, которая принимает 1 параметр. При первом вызове, она его запоминает, при втором - суммирует переданый параметр с тем, что передали первый раз и тд. Запрещается использовать глобальные переменные как хранилище результатов счетчика.
    counter(3) // 3
    counter(5) // 8
    counter(228) // 236
    etc
    */
    private task01 (): (num: number) => number {
      function increaseBy (): (num: number) => number {
        let count = 0

        return function (num: number): number {
          return count += num
        }

      }

      let counter: ReturnType<typeof increaseBy> | null = increaseBy()

      counter(3)
      counter(5)
      log(counter(228))

      counter = null

      return increaseBy()
    }
    /*
    Создать функцию которая будет возвращать массив в котором будут лежать все значения - аргументы переданные в данную функцию. Но если мы ничего не передадим в параметрах, то массив очистится. Запрещается использовать глобальные переменные как хранилище значений.
    getUpdatedArr(3) // [3]
    getUpdatedArr(5) // [3, 5]
    getUpdatedArr({name: 'Vasya'}) // [3, 5, {name: 'Vasya'}]
    getUpdatedArr() // []
    getUpdatedArr(4) // [4]
    ​
    etc.
    */
    private task02 () {
      function updateArr (): (data?: any) => any[] {
        let arr: any[] = []

        return function (data?: any): any[] {
          data ? arr.push(data) : arr = []

          return arr
        }
      }

      const getUpdatedArr = updateArr()

      getUpdatedArr(3) // [3]
      getUpdatedArr(5) // [3, 5]
      log(getUpdatedArr({name: 'Vasya'}))
      getUpdatedArr() // []
      log(getUpdatedArr(4))
    }
    /*
    Содать функцию , которая при каждом вызове будет показывать разницу в секундах между временем когда ее вызывали последний раз и теперешним. Вызываем первый раз, то ретерним строку 'Enabled'. Запрещается использовать глобальные переменные как хранилище значений. 
    // Запускаем первый раз
    getTime() // 'Enabled'
    // Запускаем через 2 сек
    getTime() // 2
    // Запускаем через 3 сек
    getTime() // 3
    // Запускаем через 7 сек
    getTime() // 7
    // Запускаем через 60 сек
    getTime() // 60
    // Запускаем через 1 сек
    getTime() // 1
    */
    private task03 () {
      function rememberTime () {
        let time: number

        return function () {
          const diffTime = Math.round((Date.now() - time) / 1000)          
          const result = time ? diffTime : 'Enabled'
          

          time = Date.now()

          return result
        }
      }

      const getTime = rememberTime()
      const counter = this.task01()
      
      getTime.log()
      delay(() => getTime.log(), counter(2))
      delay(() => getTime.log(), counter(3)) 
      delay(() => getTime.log(), counter(7))
      delay(() => getTime.log(), counter(60))
      delay(() => getTime.log(), counter(1))
    }
    /*
    Создать таймер обратного отсчета, который будет в console выодить время в формате MM:SS. Где MM - количество минут, SS - количество секунд. При этом когда закончится время, нужно вывести в console строку "Timer End".
    const time = 60; 
    ​
    const timer = time => {
        ...Your code
    }
    ​
    Вариант если время минута или больше
    timer(120) // в аргументе передаем кол-во секунд
    // 02:00
    // 00:59
    // 00:58
    ...
    // 00:01
    // Time End
    ​
    ​
    Вариант если время меньше минуты
    timer(59) // в аргументе передаем кол-во секунд
    // 00:59
    // 00:58
    // 00:57
    ...
    // 00:01
    // Time End
    */
    private task04 (time: number): void {
      function timer (time: number): void {
        const normilize = (time: number): string => {
          return time < 10 ? `0${time}` : `${time}`
        }

        const interval = setInterval(() => {
          const minutes = Math.floor(time / 60)
          const min = normilize(minutes)
          const sec = normilize(time - minutes * 60)

          if(time) {
            console.log(`${min}:${sec}`)
            time--
          }
          else {
            console.log('Time End')
            clearInterval(interval)
          }
          
        }, 1000)
      }

      return timer(time)
    }

    public execute () {
      this.task01()
      this.task02()
      this.task03()
      this.task04(30)
    }
  }

  export const result = function (): void {
    return new HW05().execute()
  }
}

// HW05.result()

// =============== HW06 ===============
namespace HW06 {
  
  type MyObj = { [k: string]: any };
  type Key = keyof MyObj
  interface CandidateShort {
    id: number
    name: string
  }
  interface Candidate {
    [key: string]: any
    _id: string
    index: number
    guid: string
    isActive: boolean
    balance: string
    picture: string
    age: number
    eyeColor: string
    name: string
    gender: string
    company: string
    email: string
    phone: string
    address: string
    about: string
    registered: string
    latitude: number
    longitude: number
    tags: string[]
    friends: CandidateShort[]
    greeting: string
    favoriteFruit: string
  }
  type CandidateNoState = Omit<Candidate, 'state'>

  class Candidate {
    constructor(obj: CandidateNoState) {
      Object.keys(obj).forEach(key => this[key] = obj[key])
    }

    get state(): string {
      return this.address.split(', ')[2]
    }
  }

  class HW06 {
    /*
    Создать функцию которая будет удалять людей из массива по индексу, который мы передадим параметром. 
    const arr = ['Vasya', 'Petya', 'Alexey']
    removeUser(arr, 1)
    console.log(arr) /// ['Vasya', 'Alexey']
    ​
    etc.
    */
    private task01<T> (arr: T[], index: number): void {
      // return arr.splice(index, 1)
      const length = arr.length

      for(let i = index; i < length - 1; i += 1){
        arr[i] = arr[i + 1]
      }
    }
    /*
    Создать функцию которая вернет все ключи обьекта переданного параметром
    const obj = { name: 'Vasya', age: 1}
    getAllKeys(obj) /// ["name", "age"]
    ​
    etc.
    */
    private task02 (obj: MyObj): Key[] {
      // return Object.keys(obj)
      const newArr: Key[] = []

      for(let key in obj) newArr.push(key)

      return newArr
    }
    /*
    Создать функцию которая вернет все значения объекта переданного параметром
    const obj = { name: 'Vasya', age: 1}
    getAllValues(obj) /// ["Vasya", 1]
    etc.
    */
    private task03 (obj: MyObj): any[] {
      // return Object.values(obj)
      const newArr: any[] = []

      for(let key in obj) newArr.push(obj[key])

      return newArr
    }
    /*
    Содать функцию,где мы первым параметром передадим объект с новым кандидатом, а вторым параметром - id любого кондидата в массиве candidatearr
    . Функция должна будет вставить кандидата переданного через первый параметр в массив перед кондидатом у которого id равно тому что передали во-втором параметре
    const obj = {
        id: 3,
        name: 'Vasya'
    }
    ​
    const secondObj = {
        id: 4,
        name: 'Katya'
    }
    ​
    const arr = [
        {
            id: 1,
            name: 'Kolya'
        },
        {
            id: 2,
            name: 'Petya'
        },
    ];
    ​
    insertIntoarr(obj, 2)
    console.log(arr) 
    /// [ {id: 1,name: 'Kolya'}, {id: 3, name: 'Vasya'}, {id: 2, name: 'Petya'} ]
    ​
    insertIntoarr(secondObj, 1)
    console.log(arr) 
    /// [ {id: 4,name: 'Katya'}, {id: 1,name: 'Kolya'}, {id: 3, name: 'Vasya'}, {id: 2, name: 'Petya'} ]
    */
    private task04 (): void {
      const obj = {
        id: 3,
        name: 'Vasya'
      }
      const secondObj = {
        id: 4,
        name: 'Katya'
      }
      const arr = [
        {
          id: 1,
          name: 'Kolya'
        },
        {
          id: 2,
          name: 'Petya'
        }
      ]

      const  insertIntoarr = (obj: CandidateShort, candidateId: number): void => {
        // const idx = arr.findIndex(({id}) => id === candidateId)

        // if(!idx) arr.unshift(obj)
        // else if (idx > 0) arr.splice(idx - 1, 0, obj)

        const length = arr.length
        let idx: number = -1

        for(let i = 0; i < length; i += 1) {
          if(arr[i].id === candidateId) {
            idx = i
            break
          }
        }
        
        if(idx >= 0) {
          for(let i = length; i > idx; i -= 1) {
            arr[i] = arr[i - 1]
          }
          arr[idx] = obj
        }
      }

      insertIntoarr(obj, 2)
      log(arr)
      insertIntoarr(secondObj, 1)
      log(arr)
    }
    /*
    Создайте класс Condidate который будет принимать параметром объект из массива candidatearr. Добавить метод с именем state который вернет шатат в котором живает наш кондидат. Информация о штате находится в свойстве address и это третья запись после запятой.
    const condidate = new Condidate(condidateArr[0])
    condidate.state /// Colorado
    etc.
    */
    private task05 () {
      const condidate = new Candidate(condidateArr[0])

      log(condidate.state)
    }
    /*
    Создать функцию которая выведет массив с названиями фирм взятыми из св-ва company. Если фирмы повторяются в массиве, то удалить дубликаты. Все так же используем массив candidatearr
​
      getCompanyNames() /// [""EZENT, "JASPER" ... ]
    ​
    etc
    */
    private task06 () {
      function getCompanyNames(arr: CandidateNoState[]): string[] {
        const obj: {[k: string]: boolean} = {}

        arr.forEach(({company}) => obj[company] = true)

        return Object.keys(obj)
      }

      log(getCompanyNames(condidateArr))
    }
    /*
    Создать функцию которая выведет мне массив id всех кондидатов, которые были зарагестрированны в том же году что и год указанный в параметре.
    getUsersByYear(2017) /// ["e216bc9cab1bd9dbae25637", "5e216bc9e51667c70ee19f4f" ...]
    ​
    etc
    */
    private task07 () {
      Array.prototype.getUsersByYear = function (year: number): string[] {
        return this
          .filter(({registered}) => {
            const getDate: string = registered.split(' ')[0]

            return year === new Date(getDate).getFullYear()
          })
          .map(({_id}) => _id)
      }

      log(condidateArr.getUsersByYear(2017))
    }
    /*
    Создать функцию которая вернет массив с экземплярами - кандидатами, отфильтрованных по кол-ву непрочитанных сообщений. Смотрим св-во greeting, там указанно это количество в строке, Вам надо достать это число из строки и сверять с тем что в параметер вашей функции. 
    Все так же используем массив candidatearr
    ​
    getCondidatesByUnreadMsg(8) /// [Condidate, Condidate ...]
    ​
    etc
    */
    private task08 () {
      Array.prototype.getCondidatesByUnreadMsg = function (messages: number): CandidateNoState[] {
        return this.filter(({greeting}) => {
            return greeting.match(new RegExp(` ${messages} `))
          })
      }

      log(condidateArr.getCondidatesByUnreadMsg(8))
    }
    /*
    Создать функцию которая вернет массив по свойству gender.
    Все так же используем массив 
    ​
    getCondidatesByGender('male') /// [Condidate, Condidate ...]
    ​
    etc
    */
    private task09 () {
      Array.prototype.getCondidatesByGender = function (genderStr: string): CandidateNoState[] {
        return this.filter(({gender}) => gender === genderStr)
      }

      log(condidateArr.getCondidatesByGender('male'))
    }
    /*
    Создать функцию reduce
    , join
    самому как на занятии создавали forEach, map, find, filter и т.д.
    */
    private task10 () {
      function reduce<T, R> (
        arr: T[], 
        cb: (prev: T | R, next: T, idx?: number, arr?: T[]) => R, 
        initialVal?: R
      ): R {
        const length = arr.length
        const start: number = initialVal ? 1 : 2
        let result: R = initialVal ? 
          cb(initialVal, arr[0], 0, arr) :
          cb(arr[0], arr[1], 1, arr)

        for(let i = start; i < length; i += 1) {
          result = cb(result, arr[i], i, arr)
        }

        return result
      }

      function forEach<T> (
        cb: (el: T, idx?: number, arr?: T[]) => void, 
        arr: T[]
      ): void {
        const length = arr.length

        for(let i = 0; i < length; i += 0) {
          cb(arr[i], i, arr)
        }
      }

      function map<T, R> (
        cb: (el: T, idx?: number, arr?: T[]) => R, 
        arr: T[]
      ): R[] {
        const length = arr.length
        const result: R[] = []

        for(let i = 0; i < length; i += 0) {
          result.push(cb(arr[i], i, arr))
        }

        return result
      }

      function find<T> (
        cb: (el: T, idx?: number, arr?: T[]) => boolean, 
        arr: T[]
      ): T | undefined {
        const length = arr.length

        for(let i = 0; i < length; i += 0) {
          if(cb(arr[i], i, arr)) return arr[i]
        }

        return undefined
      }

      function findIndex<T> (
        cb: (el: T, idx?: number, arr?: T[]) => boolean, 
        arr: T[]
      ): number {
        const length = arr.length

        for(let i = 0; i < length; i += 0) {
          if(cb(arr[i], i, arr)) return i
        }

        return -1
      }

      function filter<T> (
        cb: (el: T, idx?: number, arr?: T[]) => boolean, 
        arr: T[]
      ): T[] {
        const length: number = arr.length
        const result: T[] = []

        for(let i = 0; i < length; i += 0) {
          if(cb(arr[i], i, arr)) result.push(arr[i])
        }

        return result
      }

      function every<T> (
        cb: (el: T, idx?: number, arr?: T[]) => boolean, 
        arr: T[]
      ): boolean {
        const length = arr.length

        for(let i = 0; i < length; i += 0) {
          if(!cb(arr[i], i, arr)) return false
        }

        return true
      }

      function some<T> (
        cb: (el: T, idx?: number, arr?: T[]) => boolean, 
        arr: T[]
      ): boolean {
        const length = arr.length

        for(let i = 0; i < length; i += 0) {
          if(cb(arr[i], i, arr)) return true
        }

        return false
      }

      function reverse<T> ( 
        arr: T[]
      ): T[] {
        const result: T[] = []

        for(let i = length; i; i -= 1) {
          result.push(arr[i - 1])
        }

        return result
      }
      
      log(reduce([1,2,3,4,5], (acc, el) => acc + el, 0))

    }

    public execute () {
      this.task04()
      this.task05()
      this.task06()
      this.task07()
      // this.task08()
      // this.task09()
      this.task10()
    }
  }

  export const result = function (): void {
    return new HW06().execute()
  }
}

// HW06.result()

// =============== HW07 ===============
namespace HW07 {
  interface CandidateShort {
    id: number
    name: string
  }
  interface Candidate {
    [key: string]: any
    _id: string
    index: number
    guid: string
    isActive: boolean
    balance: string
    picture: string
    age: number
    eyeColor: string
    name: string
    gender: string
    company: string
    email: string
    phone: string
    address: string
    about: string
    registered: string
    latitude: number
    longitude: number
    tags: string[]
    friends: CandidateShort[]
    greeting: string
    favoriteFruit: string
  }
  type CandidateNoState = Omit<Candidate, 'state'>

  class Candidate {
    constructor(obj: CandidateNoState) {
      Object.keys(obj).forEach(key => this[key] = obj[key])
    }

    get state(): string {
      return this.address.split(', ')[2]
    }
  }
  class HW07 {
    /*
    Создать поиск кандидатов в массиве 
    по номеру телефона. Номер телефона может быть указан не полностью и в любом формате.
    const searchCandidatesByPhoneNumber = phone => {
        Your code...
    }
    ​
    searchCandidatesByPhoneNumber('43') 
    /// [Candidate, Candidate, Candidate ...]
    ​
    searchCandidatesByPhoneNumber('+1(869) 40') 
    /// [Candidate, Candidate ...]
    ​
    searchCandidatesByPhoneNumber('43') 
    /// [Candidate, Candidate, Candidate ...]
    ​
    searchCandidatesByPhoneNumber('+1(869)408-39-82') 
    /// [Candidate]
    */
    private task01 () {
      Array.prototype.searchCandidatesByPhoneNumber = function (serchPhone: string): string[] {
        return this.filter(({phone}) => {
          phone.match(new RegExp(serchPhone))
        })
      }
    }
    /*
    Создать функию которая найдет кандидата по  _id и вернет его из массива 
    c отформатированной датой регистрации (поле registred). Дата должна иметь формат DD/MM/YY.
    const getCandidateById = id => {
        Your code...
    }
    ​
    getCandidateById('5e216bc9a6059760578aefa4') 
    // {
    //    _id: '5e216bc9a6059760578aefa4',
    //    name: 'Bernice Walton',
    //    registred: '05/11/2015',
    //    ... other properties in candidate 
    // }
    */
    private task02 () {
      Array.prototype.getCandidateById = function (searchId: string): Candidate | undefined {
        let buff = ''
        const result = this.find(({_id, registred}) => {
          if (_id === searchId) {
            const d = new Date(registred.split(' ')[0])

            buff = `${d.getDate()}/${d.getMonth()}/${d.getFullYear()}`

            return true
          }
          
          return false
        })

        return result ? {...result, registred: buff} : undefined
      }
    }
    /*
    Написать функцию которая будет сортировать массив 
    по количеству денег лежащих на балансе (смотрим свойство balance)   в том порядке, в котором ей укажут в аргементе sortBy. Если параметр не был передан, то вернет массив в первоначальном состоянии.
    const sortCandidatesArr = sortBy => {
        Your code...
    }
    ​
    sortCandidatesArr('asc') 
    // отсортирует по-возростанию и вернет отсортированный массив
    ​
    sortCandidatesArr('desc') 
    // отсортирует по-убыванию и вернет отсортированный массив
    ​
    sortCandidatesArr() 
    // не будет сортировать, а просто вернет первоначальный массив
    */
    private task03 () {
      Array.prototype.sortCandidatesArr = function (sortBy?: Sort): Candidate[] {
        return this.sort((prev, next) => {
          if(sortBy === 'asc') 
            return prev.ballance - next.ballance
          else if(sortBy === 'desc') 
            return next.ballance - prev.ballance
          else 
            return 0
        })
      }
    }
    /*
    Написать функцию, которая вернет объект в котором название ключей будут цвета глаз, а значением - массив с кандидатами имеющие такой цвет глаз. При этом нельзя самому указывать первоначальный объект с возможными вариантами цветами глаз, а сгенерировать их на основе массива с кандидатами, то есть пройтись по массиву 
    и брать смотреть на свойство eyeColor и добавлять значение цвета глаз кандидата как ключ объекта, если такого ключа не существует, ну и не добавлять - если  одноименный ключ уже существует
    const getEyeColorMap = () => {
        Your code...
    }
    ​
    getEyeColorMap()
    // {
    //    grey:  [Candidate, Candidate, Candidate, Candidate ...],
    //    blue:  [Candidate, Candidate, Candidate, ...],
    //    green: [Candidate, Candidate, Candidate, Candidate, Candidate ...]
    //    ... etc.
    // }
    */
    private task04 () {
      Array.prototype.getEyeColorMap = function (): {[key: string]: Candidate[]} {
        const obj:{[key: string]: Candidate[]} = {}

        this.forEach(candidate => {
          obj[candidate.eyeColor] ? 
            obj[candidate.eyeColor].push(candidate) :
            obj[candidate.eyeColor] = [candidate]
        })

        return obj
      }

      console.log(condidateArr.getEyeColorMap())
    }    

    public execute () {
      this.task04()
    }
  }

  export const result = function (): void {
    return new HW07().execute()
  }
}

// HW07.result()

namespace LC01 {
  // ========== Composition ==========
  class Wheel {
    tirePressure: number
    constructor(tirePressure: number){
        this.tirePressure = tirePressure;
    }
  }

  class Car {
    name: string
    dateOfManufacture: number
    wheels: {}

    constructor(name: string, dateOfManufacture: number){
      this.name = name;
      this.dateOfManufacture = dateOfManufacture;
      this.wheels = {
        frontWheels: {
            left: new Wheel(2), // создаем левое переднее колесо
            right: new Wheel(2) // создаем правое переднее колесо
        },
        backWheels: {
            left: new Wheel(2.5), // создаем левое заднее колесо
            right: new Wheel(2.5) // создаем правое заднее колесо 
        }
      }
    }
    
    getAge(): number {
      // На момент написания был 2020 год
      return new Date().getFullYear() - this.dateOfManufacture
    }
  }

  // создаем авто, передавая ему колеса
  const car = new Car('Audi', 2010)

  // ========== Agregation ==========
  class Wheel1 {
    constructor(public tirePressure: number){}
  }
  
  class Car1 {
    get color (): string{
      return this.color
    }

    set color (value: string){
        this.color = value;
    }

    constructor(
      public name: string, 
      public dateOfManufacture: number, 
      public wheels: {}
    ) {}


      
    getAge(): number {
      // На момент написания был 2020 год
      return new Date().getFullYear() - this.dateOfManufacture
    }
  }
  
  const wheels = {
      frontWheels: {
          left: new Wheel1(2), // создаем левое переднее колесо
          right: new Wheel1(2) // создаем правое переднее колесо
      },
      backWheels: {
          left: new Wheel1(2.5), // создаем левое заднее колесо
          right: new Wheel1(2.5) // создаем правое заднее колесо 
      }
  }
  
  // создаем авто, передавая ему колеса
  const car1 = new Car1('Audi', 2010, wheels)

  /**Можно воспользоваться популярной шпаргалкой: спросите себя, сущность А является сущностью Б? Если да, то скорее всего, тут подойдет наследование. Если же сущность А является частью сущности Б */

  // =========== Inheritance ============

  //родительский класс Transport
  class Transport1{
    constructor(
      public name: string, 
      public passengersSeats: number
    ){}
  }

  //дочерний класс Car
  class Car2 extends Transport1{
    constructor(
      public mark: string, 
      public model: string, 
      public year: number, 
      public passengersSeats: number
    ){
      super('Car', passengersSeats)
    }
  }

  //дочерний класс Plane
  class Plane extends Transport1{
    constructor(public mark: string, 
      public model: string, 
      public maxHeight: number, 
      public passengersSeats: number
    ){
      super('Plane', passengersSeats)
    }
  }
  
  let carObj = new Car2('BMW', 'X5', 2020, 4);
  let planeObj = new Plane('Boeing', '787-9', 13100, 250);

  console.log(carObj);
  /*
  {
    name: "Car"
    passengersSeats: 4
    mark: "BMW"
    model: "X5"
    year: 2020
  }
  */
 
  console.log(planeObj);
  /*
  {
    name: "Plane"
    passengersSeats: 250
    mark: "Boeing"
    model: "787-9"
    maxHeight: 13100
  }
  */

  // ================ Promises ================

  let _promise = new Promise<string>((resolve, reject) => {    
    resolve('значение_при_успешном_выполнении')    
    reject('значение_при_ошибке')
  })
  
  _promise
    .then(console.log)
    .catch(console.log)
    .finally(() => console.log('Always appears1'))

  // JavaScript Promise может быть в одном из трех состояний:
  // в ожидании - начальное состояние, устанавливается в момент создания объекта;
  // исполнен - устанавливается вызовом аргумента resolve;
  // отклонен - устанавливается вызовом аргумента reject.

  let result = new Promise<number>((resolve, reject) => {
    let value = Math.round(Math.random());
    
    setTimeout(() => {
      if(value)
        resolve(value);
      else
        reject(value);
    }, 1000);
  })
  
  result
    .then(result => console.log('Success: ', result))
    .catch(error => console.log('Error: ', error))
    .finally(() => console.log('JavaScript Promise finished'))

  let result1 = new Promise<string>((resolve, reject) => {
    console.log('Promise executing');
    
    resolve('webdraftt');
  })
  
  result1.then(result => console.log('Result: ', result))
  
  setTimeout(() => result.then(result => console.log('Again: ', result)), 1000)
  
  /* Результат в консоли:
  ​
  Promise executing
  Result: webdraftt
  Again: webdraftt
  */

  // Класс Promise для расширения возможностей при работе с его экземплярами предоставляет набор статических методов:
  // Promise.resolve() - возвращает уже исполненный объект Promise с результатом, переданным ему в качестве параметра;
  // Promise.reject() - возвращает уже отклоненный объект Promise с результатом, переданным ему в качестве параметра;
  // Promise.all() - принимает массив объектов Promise, ожидает их исполнения и возвращает новый Promise, результатом которого будет массив значений переданных объектов;
  // Promise.allSettled() - принимает массив объектов Promise ожидает их исполнения или отклонения и возвращает новый Promise c массивом объектов, каждый из которых содержит состояние и значение соответствующего ему исходного Promise;
  // Promise.race() - принимает массив объектов Promise и возвращает тот из них, который будет исполнен или отклонен первым

  let promise1 = new Promise((resolve, reject) => setTimeout(() => resolve(1), 1000));
  let promise2 = new Promise((resolve, reject) => setTimeout(() => resolve(2), 1500));
  let promise3 = new Promise((resolve, reject) => setTimeout(() => resolve(3), 500))
  
  //выведет [1, 2, 3] через 1500 мс
  Promise.all([promise1, promise2, promise3]).then(result => console.log(result))

  let promise4 = new Promise((resolve, reject) => setTimeout(() => resolve(1), 1000));
  let promise5 = new Promise((resolve, reject) => setTimeout(() => resolve(2), 1500));
  let promise6 = new Promise((resolve, reject) => setTimeout(() => reject(3), 500))

  Promise.allSettled([promise4, promise5, promise6]).then(result => console.log(result));
  // В приведенном примере значением переменной result будьте следующий массив.
  // [
  //   {status: "fulfilled", value: 1}
  //   {status: "fulfilled", value: 2}
  //   {status: "rejected", reason: 3}
  // ]

  let promise7 = new Promise((resolve, reject) => setTimeout(() => resolve(1), 1000));
  let promise8 = new Promise((resolve, reject) => setTimeout(() => resolve(2), 1500));
  let promise9 = new Promise((resolve, reject) => setTimeout(() => resolve(3), 500))
  
  //выведет 3 через 500 мс
  Promise.race([promise7, promise8, promise9]).then(result => console.log(result))

  // ---------------- async await --------------
  // стандартная функция
  async function asyncFunc(){
    return new Promise((resolve, reject) =>  setTimeout(() => resolve(3), 500));
  }

  // стрелочная функция
  let asyncFuncExpression = async () => new Promise((resolve, reject) =>  setTimeout(() => resolve(3), 500))

  let _promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log(1);
      resolve(1);
    }, 3000);
  })
  
  async function asyncFunc1(){
    let value = await _promise1;
    
    console.log(2);
    
    return 3;
  }
  
  asyncFunc1().then(result => console.log(result))
  
  /* Результат в консоли:
  1
  2
  3
  */

  // ============== Exeptions =============

  try{
    // выполняемый код
  } catch(e){
    // код обработки исключения
  }finally{
    // код, выполняемый независимо от того, было ли сгенерировано исключение
  }

  let obj: {name: any}, name
  
  try{
    name = obj!.name;
    alert(name); // не выполнится
  }catch(e){
    name = 'Unknown';
    alert(name); // Unknown
  }

  // На практике часть кода, в которой может возникнуть ошибка, гораздо больше, поэтому чтобы понять что именно спровоцировало генерацию исключения, необходимо обратиться к объекту JavaScript Error, который передается аргументом блоку catch и содержит следующие свойства:
  // name - наименование ошибки, может быть TypeError, RangeError, EvalError и т.д;
  // message - текст сообщения ошибки

  // try{
  //   let a = b * b; 
  // }catch(e){
  //   console.log(e.name); // ReferenceError
  //   console.log(e.message); // b is not defined
    
  //   console.log(e); //ReferenceError: b is not defined at window.onload ...
  // }

  try{
    let result = division(3, 0);
  }catch(e: any){
    console.log(e.name); // Error
    console.log(e.message); // Division by zero not allowed.
  }

  function division(dividend: number, divider: number): number {
    if(divider == 0)
      throw new Error('Division by zero not allowed');
    else return dividend / divider;
  }

  class TimeError extends Error{
    public name: string = 'Time error'

    constructor(message: string, public whatWrong: string){
      super(message)
    }
  }
  
  function checkTime(hours: number, minutes: number, seconds: number){
    if(hours < 0 || hours > 23)
      throw new TimeError('Hours value must be between 0 and 23', 'hours');
    else if(minutes < 0 || minutes > 59)
      throw new TimeError('Minutes value must be between 0 and 59', 'minutes');
    else if(seconds < 0 || seconds > 59)
      throw new TimeError('Seconds value must be between 0 and 59', 'seconds');
      
    return true;
  }
  
  try{
    checkTime(12, 65, 7);
  }catch(e: any){
    console.log(e.name); // Time error
    console.log(e.message); // Minutes value must be between 0 and 59
    console.log(e.whatWrong); // minutes
  }

  // ==================== Modules ====================

  export let weekDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
  
  export function printMessage(message: string){
    alert(message);
  }

  let weekDays1 = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
  function printMessage1(message: string){
    alert(message);
  }

  // export { weekDays1, printMessage1 }

  // export default function(message){
  //   alert(message);
  // }

  // import {что} from 'откуда'

  //import * as m from './module'
  
  // m.printMessage(m.weekDays[4]); // 'Friday'

  let modulePath = './module'

  import(modulePath)
    .then(m => m.printMessage(m.weekDays[1])) // 'Tuesday'
    .catch(err => console.log(err))
}

namespace LC02 {
  // =============== DOM ==============

  // Кроме children и querySelectorAll есть другие способы поиска DOM-элементов:
  // getElementsByTagName(tag) — находит все элементы с заданным тегом,
  // getElementsByClassName(className) — находит все элементы с заданным классом,
  // getElementsByName(name) — находит все элементы с заданным атрибутом name.

  // removeChild() - вызывается применительно к элементу, у которого необходимо удалить прямой дочерний элемент, аргументом принимает удаляемый HTML-тег и возвращает ссылку на него;
  // remove() - вызывается применительно к элементу, который необходимо удалить из DOM.

  // Для создания нового HTML-тега в JavaScript используется метод document.createElement(), который единственным параметром принимает наименование тега и возвращает объект типа HTMLElement.
  // let p = document.createElement('p')

  // appendChild() - принимает элемент, добавляет его прямым последним дочерним элементом к элементу, применительно к которому был вызван метод и возвращает ссылку на привязанный HTML-тег
  // let h1 = document.createElement('h1');
  // h1.textContent = 'Title'

  // document.getElementById('wrapper').appendChild(h1);
  // insertBefore() - принимает в качестве аргументов два элемента: который необходимо вставить в DOM и после которого необходимо вставить первый, а сам метод вызывается применительно прямого родителя переданных в качестве параметров элементов
  // let child_1 = document.createElement('div');
  // child_1.setAttribute('id', 'child_1')

  // let parent = document.getElementById('parent'),
  //     child_2 = document.getElementById('child_2');
      
  // parent.insertBefore(child_1, child_2);
  // after() - принимает в качестве аргумента элемент, который необходимо вставить после элемента, относительно которого вызывается сам метод
  // let child_2 = document.createElement('div');
  // child_2.setAttribute('id', 'child_2')

  // let child_1 = document.getElementById('child_1');
      
  // child_1.after(child_2)

  // parentNode - прямой родитель;
  // firstChild - первый прямой дочерний элемент;
  // lastChild - последний прямой дочерний элемент;
  // nextSibling - следующий элемент в структуре, находящийся на одном уровне иерархии с текущим элементом;
  // previousSibling - предыдущий элемент в структуре, находящийся на одном уровне иерархии с текущим элементом

  // getAttribute() - принимает наименование атрибута и возвращает его значение;
  // setAttribute() - принимает наименование и значение атрибута и устанавливает его для элемента, относительно которого вызывается, если атрибут уже существует, то просто перезаписывает его значение;
  // hasAttribute() - принимает наименование атрибута и возвращает значение логического типа: true - указанный атрибут задан у элемента, false - не задан;
  // removeAttribute() - принимает наименование атрибута и удаляет его у элемента, относительно которого вызывается.

  // <div id="block"></div>
  
  // let block = document.getElementById('block')
  
  // block.setAttribute('style', 'background-color: #f00;height: 100px;width: 100px;');
  // Пример стилизация через свойство style.
  // let block = document.getElementById('block')
  
  // block.style.backgroundColor = '#f00';
  // block.style.height = '100px';
  // block.style.width = '100px'

  // ------------------- Events ------------------

  // focus - элемент, чаще всего поле формы, становится активным (на нем устанавливается фокус);
  // blur - элемент, который был активным, стал неактивным (потерял фокус);
  // input - в текстовое поле были введены данные;
  // change - значение элемента управления изменилось;
  // cut - данные вырезаны (сочетание клавиш ctrl + x или через контекстное меню мыши);
  // copy - данные скопированы (сочетание клавиш ctrl + c или через контекстное меню мыши);
  // paste - данные вставлены (сочетание клавиш ctrl + v или или через контекстное меню мыши);
  // submit - отправка данных формы.

  // <form>
  //   <input id="field1" type="text" />
  //   <input id="field2" type="text" />
  // </form>
  
  // let field1 = document.getElementById('field1'),
  //     field2 = document.getElementById('field2')
      
  // field1.addEventListener('focus', event => console.log(event.relatedTarget));
  // field2.addEventListener('focus', event => console.log(event.relatedTarget))

  // <form>
  //   <input type="text" id="field" />
  // </form>
  
  // let field = document.getElementById('field')
  
  // field.addEventListener('focus', event => console.log('Focus set'));
  // field.addEventListener('blur', event => console.log('Focus lost')

  // field.addEventListener('input', event => console.log(`Input event, inputType: ${event.inputType}`));
  // field.addEventListener('change', event => console.log('Change event'))

  // <form id="form">
  //   <input type="text" />
    
  //   <button type="submit">Save</button>
  // </form>
  // ​
  // let form = document.getElementById('form')
  
  // form.addEventListener('submit', event => console.log(event))

  // <button id="btn">Click me!</button>
  
  // <button id="cancel-btn">Remove click handler from "Click me!"</button>

  // let button = document.getElementById('btn')

  // let clickHandler = (event) => alert('Button clicked')

  // button.addEventListener('click', clickHandler)

  // let cancelButton = document.getElementById('cancel-btn')

  // cancelButton.addEventListener('click', (event) => {
  //   button.removeEventListener('click', clickHandler);
  // }

  // document.addEventListener('mousemove', event => console.log(event))
  // document.addEventListener('keydown', event => console.log(event))

  // preventDefault() - предотвращает поведение браузера, определенное для события по умолчанию;
  // stopPropagation() - предотвращает распространение события вверх по DOM-дереву

  // ------------------ Window ------------------

  // DOMContentLoaded - структура DOM построена;
  // load - структура DOM построена и загружены все файлы скриптов и стилей;
  // resize - размеры окна браузера были изменены;
  // scroll - прокручивается документ (или элемент);
  // beforeunload - вызывается перед закрытием окна (или вкладки);
  // unload - вызывается в момент закрытия окна (или вкладки);
  // beforeprint - вызывается перед печатью;
  // afterprint - вызывается после печати

  // window.addEventListener('resize', event => console.log('Window size changed'))
  // body{height: calc(100vh + 100px);} //необходимо для появления полосы прокрутки
  
  // window.addEventListener('scroll', event => console.log('Scrolling'))
  // window.addEventListener('beforeunload', () => {
  //   console.log('Window is about to be closed');
  //   return true;
  // })

  // window.addEventListener('unload', event => console.log('Window closed'))

  // ------------------ Keyboard ------------------

  // keydown - клавиша нажата;
  // keyup - нажатая клавиша отпущена;
  // keypress - ввод символа

  // <form>
  //   <input id="field" type="text" />
  // </form>

  // let field = document.getElementById('field')

  // field.addEventListener('keydown', event => alert(`
  //   Key: ${event.key}
  //   Code: ${event.code}
  //   KeyCode: ${event.keyCode}
  //   Alt Key pressed: ${event.altKey}
  // `))
  // field.addEventListener('keyup', event => console.log(`Key ${event.key} is up`))
  // field.addEventListener('keypress', event => console.log(`Symbol key pressed`))

  // ------------------ Mouse ------------------

  // click - клик левой кнопкой;
  // contextmenu - клик правой кнопкой;
  // dblclick - двойной клик левой кнопкой;
  // wheel - прокрутка колесика;
  // mousedown - нажатие любой кнопки;
  // mouseup - нажатая кнопки была отпущена;
  // mousemove - перемещение курсора;
  // mouseover - наведение курсора на элемент;
  // mouseout - наведенный курсор покинул пределы элемента

  // button.addEventListener('contextmenu', event => event.preventDefault())
  // button.addEventListener('dblclick', event => alert('Double click on button detected'))

  // button.addEventListener('mousedown', event => alert('Down'));
  // button.addEventListener('mouseup', event => alert('Up'));
  // button.addEventListener('click', event => alert('Click'))

  // button.addEventListener('mousemove', event => console.log(`X: ${event.clientX}, Y: ${event.clientY}`))
  // button.addEventListener('mouseover', event => alert('Inside button'))
  // button.addEventListener('mouseout', event => alert('Outside button'))

  // ------------------- create events ------------------

  // Встроенные события JavaScript создается с использованием конструктора класса необходимого события, который принимает два параметра:
  // наименование события (обязательно);
  // объект конфигурации (необязательно).
  // В объекте конфигурации в свою очередь можно задать следующие общие свойства:
  // bubbles - указывает, будет ли событие распространено вверх по DOM (по умолчанию false);
  // cancelable - (указывает, можно ли отменить событие вызовом у его объекта метода preventDefault() (по умолчанию false)

  // <form>
  //   <input id="field" type="text" />
  // </form>

  // let keydownEvent = new KeyboardEvent('keydown', {
  //   key: 'a',
  //   cancelable: true
  // })
  // let field = document.getElementById('field')
  // field.addEventListener('keydown', event => console.log(event))
  // field.dispatchEvent(keydownEvent);

  // Также JavaScript предоставляет механизм в виде экземпляров класса CustomEvent для создания и программного вызова пользовательских событий. Конструктор CustomEvent принимает два аргумента:
  // наименование события (обязательно);
  // объект конфигурации (необязательно)

  // <p id="message"></p>
  
  // let userMessageEvent = new CustomEvent('userMessage', {
  //   detail: 'JavaScript Custom Event'
  // });

  // let messageWrapper = document.getElementById('message')
  
  // messageWrapper.addEventListener('userMessage', event => event.target.textContent = event.detail)
  
  // messageWrapper.dispatchEvent(userMessageEvent)

  // =================== Handle Forms ==================

  // В структуре DOM элемент формы представлен объектом HTMLFormElement, который содержит все данные о ней и реализовывает ряд полезных методов.
  // Свойства объекта HTMLFormElement:
  // name - значение атрибута name;
  // action - значение атрибута action;
  // method - значение атрибута method;
  // novalidate - true, если у формы указан атрибут novalidate;
  // elements - содержит объект HTMLFormControlsCollection со всеми полями формы;
  // length - количество полей формы

  // -------------------- Methods --------------------

  // submit() - инициирует событие submit относительно формы и отправляет ее значение на заданный в атрибуте action URL-адрес указанным в method HTTP-методом;
  // reset() - сбрасывает значение формы в начальное состояние;
  // checkValidity() - возвращает true, если все поля формы валидны, в противном случае возвращает false;
  // reportValidity() - возвращает true, если все поля формы валидны, иначе инициирует у каждого невалидного элемента поля формы событие invalid

  // <form name="loginForm">
  //   <input name="login" type="text" />
  //   <input name="password" type="password" />
    
  //   <button type="submit">Log In</button>
  // </form>
  
  // document.forms.loginForm.addEventListener('submit', event => {
  //   console.log('Elements count: ', event.target.length)
    
  //   console.log('Login: ', event.target.login.value); // значение поля login
  //   console.log('Password: ', event.target.password.value); // значение поля password
    
  //   event.target.reset(); // сброс значения формы в начальное состояние
    
  //   event.preventDefault();
  // })

  // document.forms.loginForm; // получение формы loginForm
  // document.forms.loginForm.password; // получение поля password формы loginForm

  // <form name="orderForm"  novalidate>
  //   <input name="product" type="text" required />
  //   <input name="count" type="text" min="1" />
    
  //   <button type="submit">Order</button>
  // </form>
  
  // document.forms.orderForm.addEventListener('submit', event => {
  //   if(!event.target.checkValidity()){
  //     alert('Form invalid');
  //     event.preventDefault();
  //   }
  // })

  // -------------------- Validation -------------------

  // <form name="orderForm"  novalidate>
  //   <input name="product" type="text" required />
  //   <input name="count" type="text" pattern="[0-9]{1,}" />
    
  //   <button type="submit">Order</button>
  // </form>
  
  // document.forms.orderForm.addEventListener('submit', event => {
  //   if(!event.target.reportValidity())
  //     event.preventDefault();
  // })
  
  // document.forms.orderForm.product.addEventListener('invalid', event => {
  //   alert('Product field is invalid');
  //   event.preventDefault();
  // });
  // document.forms.orderForm.count.addEventListener('invalid', event => {
  //   alert('Count field is invalid');
  //   event.preventDefault();
  // })

  // Свойства объекта JavaScript ValidityState:
  // valueMissing - true, если установлен атрибут required и поле не заполнено;
  // patternMismatch - true, если значение поля не соответствует заданному атрибутом pattern регулярному выражению;
  // typeMismatch - true, если значение поля не соответствует заданному атрибутом type типу;
  // stepMismatch - true, если значение не соответствует заданному атрибутом step ограничению;
  // tooLong - true, если значение поля по количеству символов превышает заданное атрибутом maxlength ограничение;
  // tooShort - true, если значение поля по количеству символов меньше заданного атрибутом minlength ограничения;
  // rangeUnderflow - true, если значение поля больше заданного атрибутом max значения;
  // rangeOverflow - true, если значение поля меньше заданного атрибутом min значения;
  // valid - true, если значение поля соответствует всем заданным для него ограничениям;
  // customError - true, если для поля задана пользовательская валидация.
  // document.forms.orderForm.addEventListener('submit', event => {
  //   if(!event.target.reportValidity()){
  //     if(event.target.product.validity.valueMissing)
  //       alert('Product field is empty');
  //     else if(event.target.count.validity.patternMismatch)
  //       alert('Count value must be a number')
        
  //     event.preventDefault();
  //   }
  // });

  // document.forms.orderForm.addEventListener('submit', event => {
  //   if(!event.target.product.value){
  //     event.target.product.setCustomValidity('Product must be filled')
      
  //     alert(event.target.product.validationMessage); // Product must be filled
      
  //     console.log(event.target.product.validity.customError); // true
      
  //     event.preventDefault();
  //   }
  // })

  // ============ Local, Session storage ============

  // В Local Storage данные хранятся в браузере постоянно, а в Session Storage - только в рамках сессии до закрытия вкладки. Все сохраняемые данные принудительно приводятся к строковому формату.
  // Объекты JavaScript localStorage и sessionStorage являются производными объекта Storage, от которого наследуют следующие методы:
  // setItem() - устанавливает в качестве значения для заданного первым аргументом ключа переданные вторым параметром данные;
  // getItem() - возвращает значение указанного аргументом ключа, или undefined, если такого ключа не существует;
  // removeItem() - удаляет пару ключ/значение по заданному ключу;
  // key() - возвращает наименование
  // clear() 

  // Пример работы с объектом localStorage
  // запись ключа SHOW_NOTIFICATIONS со значением true
  // localStorage.setItem('SHOW_NOTIFICATIONS') = true;
    
  // проверка наличия ключа и чтение его значения
  // if(localStorage.getItem('SHOW_NOTIFICATIONS'))
  //   alert(`SHOW_NOTIFICATIONS value: ${localStorage.getItem('SHOW_NOTIFICATIONS')}`);
    
  // удаление ключа и его значения
  // localStorage.removeItem('SHOW_NOTIFICATIONS')
  
  // удаление всех пар ключ/значение из хранилища
  // localStorage.clear();
  // Для чтения и записи данных имеется альтернативный вариант
  // запись ключа SHOW_NOTIFICATIONS со значением true
  // localStorage.SHOW_NOTIFICATIONS = true;
    
  // проверка наличия ключа и чтение его значения
  // if(localStorage.SHOW_NOTIFICATIONS)
  //   alert(`SHOW_NOTIFICATIONS value: ${localStorage.SHOW_NOTIFICATIONS}`)

  // В объекте sessionStorage данные хранятся до закрытия вкладки браузера или пока не будут удалены явно.
  // Все методы для работы с Session Storage идентичны тем, которые используются при работе с Local Storage.
  // запись ключа TOKEN со значением true
  // localStorage.TOKEN = 'A1B2C3D4E5F6G7';
    
  // проверка наличия ключа и чтение его значения
  // if(localStorage.TOKEN)
  //   alert(`TOKEN value: ${localStorage.TOKEN}`);
    
  // удаление ключа и его значения
  // localStorage.removeItem('TOKEN')
  
  // удаление всех пар ключ/значение из хранилища
  // localStorage.clear()

  // ===================== Fetch ====================
  fetch('https://example.com/api/data')
    .then(response => console.log(response.json()))
    .catch(err => console.log(err))

  // Метод запроса (GET, POST, PUT, DELETE), HTTP-заголовки и другие параметры запроса задаются в виде объекта, который передается вторым аргументом методу fetch() и имеет следующий набор свойств:
  // method - метод запроса;
  // headers - HTTP-заголовки (задаются в виде объекта);
  // body - тело запроса (указывается в POST и PUT запросах);
  // cache - значение заголовка Cache-Control;
  // mode - режим запроса (cors, no-cors или same-origin), управляет заголовком Access-Control-Allow-Origin;
  // redirect - указывает, как обрабатывать перенаправления (редиректы);
  // referrer - устанавливает значение заголовка Referer;
  // referrerPolicy - устанавливает значение заголовка;
  // signal - объект класса AbortSignal, который позволяет отменить запрос уже в процессе его выполнения (примере приведен далее в этой статье).
  let body = {name: 'Jake'}
  
  fetch('https://example.com/api/data', {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    },
    mode: 'no-cors'
  })
    .then(response => console.log(response.json()))
    .catch(err => console.log(err))

  // ------------------- File upload -------------------

  // <input type="file" id="fileUpload" />
  
  let fileUpload = <HTMLInputElement>document.getElementById('fileUpload')
  
  fileUpload.addEventListener('change', event => {
    let formData = new FormData();
    formData.append('file', fileUpload.files![0])
    
    upload(formData);
  })
  
  function upload(formData: FormData){
    fetch('https://example.com/api/file', {
      method: 'POST',
      body: formData,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
    .then(async response => console.log(await response.json()))
    .catch(err => console.log(err))
  }

  // Выполнение возвращаемого методом fetch() Promise завершается объектом класса Response, который имеет следующие свойства:
  // status - код ответа (200, 405, 500 и т.д.);
  // statusText - соответствующее коду ответа текстовое сообщение;
  // ok - содержит логическое значение указывающее, является ли код ответа успешным (200-299);
  // headers - объект с заголовками ответа, в котором ключ - наименование заголовка, а значение ключа - значение соответствующего ключу заголовка;
  // url - URL, на который был отправлен запрос;
  // body - данные ответа в формате ReadableStream;
  // bodyUsed - содержит логическое значение, указывающее, были ли прочитаны данные из свойства body.
  // Поскольку экземпляры класса Response содержат переданные сервером данные в формате ReadableStream, применительно к ним можно вызвать один из следующих методов, преобразующие и возвращающие Promise с данными в определенном формате:
  // text - возвращает данные в виде строки;
  // json - преобразовывает данные в формат JSON;
  // blob - возвращает данные как объект класса Blob;
  // formData - конвертируется ответ в экземпляр FormData;
  // arrayBuffer - возвращает ответ в виде бинарных данных, представленных в JavaScript классом ArrayBuffer

  fetch('https://example.com/api/data', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json'
  }
  }).then(async response => {
    console.log(response.bodyUsed); // false
    let result = await response.json();
    console.log(response.bodyUsed); // true
    return result;
  })

  // JavaScript Fetch API предоставляет инструмент в виде объекта класса AbortController для отмены уже выполняющихся HTTP-запросов. Для отмены запроса необходимо выполнить следующее:
  // Создать экземпляр класса AbortController;
  // При выполнении запроса в объекте конфигурации свойству signal необходимо задать значение свойства signal созданного объекта AbortController;
  // Выполнить запрос и еще до его завершения вызвать у созданного объекта AbortController метод abort().
  // <button id="send">Send request</button>
  
  // <button id="cancel">Cancel request</button>
  
  let abortCtrl = new AbortController()
  
  let send = document.getElementById('send');
  let cancel = document.getElementById('cancel')
  
  send?.addEventListener('click', event => sendRequest());
  cancel?.addEventListener('click', event => abortCtrl.abort())
  
  function sendRequest(){
    fetch('https://example.com/api/data', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
      signal: abortCtrl.signal
    })
    .then(async response => console.log(await response.json))
    .catch(err => console.log(err));
  }

  // ================== Window ==================

  // Рассмотрим наиболее распространенные и специфичные только для JavaScript window свойства:
  // document - ссылка на DOM
  // ;
  // location - объект, содержащий содержащий свойства и методы для работы с URL текущего окна;
  // navigator - объект, который позволяет получить данные о браузере пользователя или даже узнать его местоположение;
  // history - объект, содержащий свойства и методы для работы с историей браузера в рамках текущего окна;
  // ​
  // - объекты локального и сессионного хранилищ соответственно;
  // scrollX / scrollY - отступы в пикселях от левого края и самого верха страницы соответственно;
  // innerHeight / innerWidth - высота и ширина внутренней части окна, включая горизонтальные и вертикальные полосы прокрутки при их наличии;
  // alert("innerHeight: " + window.innerHeight); // 682
  // alert("innerWidth: " + window.innerWidth); // 1536
  // outerHeight / outerWidth - высота и ширина окна с учетом внешней его части (адресной строки, панели закладок и прочего);
  // alert("outerHeight: " + window.outerHeight); // 824
  // alert("outerWidth: " + window.outerWidth); // 1536
  // frames - массив дополнительных окон (элементов <iframe>) в текущем окне;
  // top - ссылка на объект самого верхнего окна, при отсутствии дополнительных окон равен самого объекту window.untitled.js
  // alert(window.top === window)

  // ------------------ Window methods ------------------

  // Рассмотрим методы объекта window:
  // open() - принимает URL-адрес ресурса, загружает его в новом окне и возвращает ссылку на это окно;
  // let newWindow = window.open('https://webdraftt.com');
  // ​
  // newWindow.addEventListener('load', () => alert(newWindow.location.href)); // 'https://webdraftt.com/'
  // stop() - останавливает загрузку окна;
  // close() - закрывает окно;
  // scroll() - прокручивает страницу к заданным координатам, первый параметр - отступ в пикселях от левого края, второй - отступ в пикселях от самого верха;
  // window.scroll(0, 120);
  // resizeTo() - изменяет размер на заданный, первый параметр - ширина, второй - высота;
  // window.resizeTo(400, 300);
  // print() - вызывает диалоговое окно печати страницы

  // ---------------- Window events ---------------

  // Рассмотрим события объекта window:
  // load - страница загружена, включая построение DOM;
  // beforeunload - возникает перед самым закрытием окна, при этом само действие может быть отменено;
  // close - возникает при закрытии окна, действие отменить невозможно;
  // resize - изменение размеров окна;
  // scroll - вертикальная или горизонтальная прокрутка страницы;
  // popstate - нажатие кнопки "Назад";
  // error - возникновение необработанного исключения, объект события содержит текст ошибки;
  // beforeprint / afterprint - открытие / закрытие диалогового окна печати страницы;
  // online / offline - установка / потеря соединения с интернетом

  // ================ Location ================
  // ----------------- Methods ----------------
  // href - содержит полный URL, при присвоении ему нового значения сразу же осуществляется переход на указанный адрес;
  // window.location.href; // "https://naydyonovdanil.gitbook.io/"
  // ​
  // // переход на https://naydyonovdanil.gitbook.io/javascript
  // window.location.href = "https://naydyonovdanil.gitbook.io/javascript";
  // protocol - возвращает протокол вместе с символом : ;
  // window.location.protocol; // "https:"
  // host - содержит имя хоста вместе с портом;
  // window.location.host; // "naydyonovdanil.gitbook.io"
  // hostname - содержит только имя хоста;
  // window.location.hostname; // "naydyonovdanil.gitbook.io"
  // port - содержит порт текущего домена, если это порт по умолчанию (80 для http и 443 для https) - возвращает пустую строку;
  // window.location.port; // ""
  // pathname - содержит часть URL, которая следует сразу за доменным именем;
  // // для страницы https://naydyonovdanil.gitbook.io
  // window.location.pathname; // ""
  // ​
  // // для страницы https://naydyonovdanil.gitbook.io/javascript
  // window.location.pathname; // "/javascript"
  // search - содержит параметры запроса адресной строки вместе с символом ?;
  // // для страницы https://naydyonovdanil.gitbook.io
  // window.location.search; // ""
  // ​
  // // для страницы https://naydyonovdanil.gitbook.io?a=1
  // window.location.search; // "?a=1"
  // hash - содержит идентификатор текущего URL вместе с символом #.
  // // для страницы https://naydyonovdanil.gitbook.io
  // window.location.hash; // ""
  // ​
  // // для страницы https://naydyonovdanil.gitbook.io#name
  // window.location.hash; // "#name"

  // reload() - перезагружает текущую страницу, необязательным аргументом принимает логическое значение, если true - страница будет загружена с сервера, если false - может быть получена из кэша;
  // assign() - осуществляет переход по адресу, заданному аргументом в виде строки;
  // replace() - идентичен assign(), различие лишь в том, что replace() не сохраняет переход в истории браузера

  // ===================== Navigator ===================
  // ---------------------- Props ----------------------

  // appName - официальное наименование браузера;
  // // для браузера Google Chrome
  // window.navigator.appName; // "Netscape"
  // appVersion - версия браузера;
  // window.navigator.appVersion; // "5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36"
  // language - текущий язык интерфейса браузера в виде строки;
  // window.navigator.language; // "ru-RU"
  // languages - массив доступных пользователю языков, расположенных по предпочтению;
  // window.navigator.languages; // ["ru-RU", "ru", "en-US", "en"]
  // platform - содержит платформу браузера в строковом формате;
  // window.navigator.platform; // "Win32"
  // userAgent - строка агент браузера пользователя.
  // window.navigator.userAgent; // "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36"

  // online - логическое значение, указывающее, подключено ли устройство к интернету (если true - подключено);
  // window.navigator.online; // true
  // connection - подробные данные сетевого подключения пользователя.
  // window.navigator.connection;
  
  /* 
  { 
    onchange: null, 
    effectiveType: "4g", 
    rtt: 100, 
    downlink: 9.45,
    saveData: false
  }
  */

  // getCurrentPosition() - возвращает данные текущего местоположения устройства пользователя;
  // watchPosition() - регистрирует функцию, которая будет вызываться каждый раз при смене местоположения.
  // Оба указанных метода принимают один и тот же набор аргументов:
  // функция, которая будет вызвана после получения информации о текущем местоположении;
  // необязательная функция, вызываемая при возникновении ошибки;
  // необязательный объект конфигурации, задающие следующий свойства:
  // enableHighAccuracy - логическое значение, указывающее необходимо ли использовать расширенную локацию, если она поддерживается устройством, для получения максимально точного результата, по умолчанию равно false;
  // timeout - количество миллисекунд, в течение которых должны быть получены данные о местоположении, по умолчанию равно Infinity;
  // maximumAge - время кэширования последнего полученного значения в миллисекундах, по умолчанию равно 0.

  window.navigator.geolocation.getCurrentPosition(data => console.log(data))

  // {
  //   coords: {
  //     latitude: <value>, (широта)
  //     longitude: <value>, (долгота)
  //     altitude: <value>, (высота над уровнем моря)
  //     accuracy: <value>, (точность широты и долготы, м)
  //     altitudeAccuracy: <value>, (точность высоты над уровнем моря, м)
  //     heading: <value>, (направление, градусы)
  //     speed: <value> (скорость, м/с)
  //   }, 
  //   timestamp: <value> - время получения данных
  // }

  // ===================== History ===================

  // https://naydyonovdanil.gitbook.io/
  // https://naydyonovdanil.gitbook.io/javascript
  // https://naydyonovdanil.gitbook.io/javascript/window (текущий адрес)
  // https://naydyonovdanil.gitbook.io/javascript/moduli

  window.history.length; // 4

  // back() - загружает последний адрес из истории, относительно места, занимаемого в истории текущим URL-адресом;
  // переход на https://naydyonovdanil.gitbook.io/javascript
  window.history.back();
  // forward() - загружает следующий адрес из истории, относительно места, занимаемого в истории текущим URL-адресом;
  // переход на https://naydyonovdanil.gitbook.io/javascript/moduli
  window.history.forward();
  // go() - принимает значением целое число и использует его в качестве порядкового номера сохраненного в истории URL-адреса, на который должен быть осуществлен переход; если число отрицательное - отсчет ведется назад, если положительное - вперед;
  // переход на https://naydyonovdanil.gitbook.io
  window.history.go(-2);
  
  // переход на https://naydyonovdanil.gitbook.io/javascript/moduli
  window.history.go(1);
  // pushState() - добавляет в историю новую запись и принимает три параметра: объект состояния, title в виде строки и URL-адрес; сам по себе вызов метода не инициирует новый переход, но после перехода на новую страницу и последующего нажатия кнопки "Назад" произойдет переход на добавленный методом адрес, а объект состояния будет доступен через свойство window.history.state;
  window.history.pushState({data: "JavaScript History"}, '', "https://naydyonovdanil.gitbook.io/javascript/moduli")
  
  // переход на https://naydyonovdanil.gitbook.io/javascript/window и нажатие кнопки "Назад"
  
  window.history.state; // {data: "JavaScript History"}
  // replaceState() - обновляет последнюю (текущую) запись в истории и принимает идентичный pushState() набор параметров.

}