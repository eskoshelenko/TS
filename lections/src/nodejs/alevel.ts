// namespace Presentation {
//   // ================== Events ======================
//   const EventEmitter = require('events')

//   const emitter = new EventEmitter()

//   emitter.on('message', (message: string) =>
//     console.log('Message: ', message)
//   );
//   emitter.on('error', (error: Error) =>
//     console.log('Error: ', error)
//   )

//   emitter.emit('message', 'Node js EventEmitter in action.');

//   class EmitterOne extends EventEmitter {}
//   class EmitterTwo extends EventEmitter {}

//   const emitterOneInstance = new EmitterOne();
//   const emitterTwoInstance = new EmitterTwo()

//   emitterOneInstance.on('message', (message: string) =>
//     console.log('Emitter one message: ', message)
//   );
//   emitterOneInstance.on('error', (error: Error) =>
//     console.log('Emitter one error: ', error)
//   )

//   emitterTwoInstance.on('message', (message: string) =>
//     console.log('Emitter two message: ', message)
//   );
//   emitterTwoInstance.on('error', (error: Error) =>
//     console.log('Emitter two error: ', error)
//   )

//   emitterOneInstance.emit(
//     'message',
//     'Node js EventEmitter in action.'
//   );

//   // emitter.setMaxListeners(1)
//   // console.log(EventEmitter.listenerCount(emitter, 'message'))

//   const messageListener = (message: string) =>
//   console.log('Message: ', message)
  
//   emitter.on('message', messageListener)

//   emitter.emit('message', 'First')

//   emitter.removeListener('message', messageListener)

//   emitter.emit('message', 'Second'); //на этом этапе обработичка уже нет

//   const messageListener1 = (message: string) =>
//   console.log('Message listener 1: ', message);
//   const messageListener2 = (message: string) =>
//     console.log('Message listener 2: ', message);
//   const errorListener = (error: Error) =>
//     console.log('Error: ', error)
    
//   emitter.on('message', messageListener1);
//   emitter.on('message', messageListener2);
//   emitter.on('error', errorListener)
  
//   emitter.emit('message', 'First')
  
//   emitter.removeAllListeners(['message'])
  
//   emitter.emit('message', 'Second'); //на этом этапе обработичков уже нет

//   // ================== Process ======================

//   // -------------------- Props ----------------------

//   // arch - архитектура процессора, на котором запущен Node.js процесс, возможные значения: arm, ia32, x64;
//   // argv - массив с параметрами запуска процесса, причем вслед за элементом массива, который содержит параметр, будет элемент со значением этого параметра;
//   // connected - булевое значение, значение установленное в true означает, что Node.js процесс был создан с IPC каналом;
//   // env - объект с пользовательским окружением;
//   // pid - уникальный идентификатор процесса;
//   // platform - платформа операционной системы, возможные значения: freebsd, linux, win32 и т. д.;
//   // version - текущая версия Node.js

//   console.log('Processor architecture: ', process.arch) // arm | ia32 | x62
//   console.log('Platform: ', process.platform) //win32 | linux | freebsd

//   // -------------------- Methods --------------------

//   // cpuUsage() - возвращает объект с пользовательским и системным временем использования процесса в микросекундах, на основе которого можно рассчитать загрузку процессора в процентах, необязательным параметром может принимать свое предыдущее значение;
//   // hrtime() - возвращает высоко точное время в виде массива с двумя элементами: секунды и наносекунды;
//   // cwd() - возвращает рабочую директорию Node.js процесса;
//   // disconnect() - отключает IPC канал от главного процесса и завершает дочерний;
//   // exit() - завершает процесс при первой же возможности с заданным кодом (по умолчанию код завершения равен 0);
//   // memoryUsage() - возвращает объект с данными об использовании Node.js процессом оперативной памяти, объект содержит свойства rss (отведенный под процесс объем памяти), heapTotal (объем памяти, отведенный под V8), heapUsed (объем памяти, используемый V8 в данный момент) и external (использование памяти объектами C++), все значения в байтах;
//   // on() - метод обработки событий;
//   // send() - используется для обмена сообщениями между основным и дочерними процессами 
//   // uptime() - возвращает количество миллисекунд, прошедшее с момента запуска процесса.

//   const ns2ms = (ns: number) => ns * 1000000
  
//   let startTime = process.hrtime()
//   let startCpuUsage = process.cpuUsage()
  
//   for (let i = 0; i < 10000000000; i++){}
  
//   let diffTime = process.hrtime(startTime)
//   let diffUsage = process.cpuUsage(startCpuUsage)
  
//   let diffTimeMs = diffTime[1] * 1000 + ns2ms(diffTime[1])
//   let diffUsageUserMs = ns2ms(diffUsage.user)
//   let diffUsageSystemMS = ns2ms(diffUsage.system)
//   let cpuUsage = Math.round(
//     (100 * (diffUsageUserMs + diffUsageSystemMS)) / diffTimeMs
//   )
  
//   console.log('CPU usage, %: ', cpuUsage)

//   // -------------------- Events --------------------
//   // beforeExit - инициируется, когда полностью заканчивается цикл событий;
//   // disconnect - генерируется в дочернем процессе при закрытии канала IPC;
//   // exit - инициируется при завершении процесса вызовом метода process.exit() или по завершению цикла событий;
//   // message - может возникнуть только в главном процессе, когда в одном из дочерних процессов вызывается метод message();
//   // uncaughtException - генерируется в случае возникновения необработанного исключения, но процесс при этом не завершается.

//   process.on('uncaughtException', (err, origin) => {
//     console.log('Error: ', err)
//     console.log('Origin: ', origin)
//   })
  
//   // nonExistingFunction() /// throw err

//   // ------------------ Exit codes ------------------

//   // 1 - исключение, которое не обработано ни JavaScript, ни обработчиком события uncaughtException;
//   // 3, 4, 10 - означают ошибки исходного кода JavaScript, обычно встречаются только при разработке самой платформы Node.js;
//   // 5 - критическая ошибка V8;
//   // 6 - необработанное исключение не попало в обработчик критических исключений;
//   // 7 - возникновение ошибки в обработчике критических исключений;
//   // 9 - используется, если были неправильно заданы аргументы;
//   // 12 - возникает в режиме отладки при некорректном указании порта.

//   // =============== Child process =================

//   // Для создания дочернего процесса в Node.js используется встроенный модуль child_process и его методы spawn() и fork(), каждый из которых возвращает экземпляр класса ChildProcess

//   // Метод child_process.spawn() создает новый дочерний процесс (не обязательно Node.js) командой, переданной ему в качестве параметра. Вторым параметром можно передать массив аргументов команды.

//   const child_process = require('child_process');

//   //Переход в директорию /srv/app
//   const cd = child_process.spawn('cd /srv/app');

//   cd.on('error', (error: Error) =>
//     console.log('Cannot change dir: \n', error)
//   );

//   //Получение списка файлов и директорий для Linux
//   const ls = child_process.spawn('ls');

//   ls.stdout.on('data', (data: string) =>
//     console.log('Files list: \n', data)
//   );
//   ls.stderr.on('error', (error: Error) =>
//     console.log('Error: \n', error)
//   );

//   // Метод child_process.fork() является частным случаем метода child_process.spawn(), только вместо команды он принимает путь к модулю, который должен быть запущен как новый процесс. Вторым параметром передается массив аргументом для запуска модуля.


//   const child_3 = child_process.fork('child.js', [3]);
//   const child_9 = child_process.fork('child.js', [9]);

//   child_3.on('close', (code: string) =>
//     console.log(`Child process 3 exited. Code: ${code}`)
//   );
//   child_9.on('close', (code: string) =>
//     console.log(`Child process 9 exited. Code: ${code}`)
//   )

//   // child.js

//   console.log(`Child process ${process.argv[2]} is running`)

//   // Child process 3 is running
//   // Child process 9 is running
//   // Child process 3 exited. Code: 0
//   // Child process 9 exited. Code: 0

//   // Для взаимодействия основного и дочернего процессов используется метод send(), который устанавливает между процессами IPC канал для обмена сообщениями и параметром принимает данные для отправки другому процессу. Переданные данные являются аргументами для callback-функции, определяемой для обработки сообщения.

//   const child = child_process.fork('child.js');

//   child.send('Ping child');

//   child.on('message', (code: string) =>
//     console.log(`Message to parent: ${code}`)
//   );

//   child.on('close', (code: string) =>
//     console.log(`Child process exited. Code: ${code}`)
//   );

//   // main.js

//   console.log(`Child process is running`);

//   process.on('message', (message) =>
//     console.log('Message to child: ', message)
//   );

//   // process.send('Ping parent')

//   // Child process is running
//   // Message to child:  Ping child
//   // Message to parent: Ping parent

//   // Завершение дочернего процесса можно инициировать вызовом метода disconnect() или exit(). Вызов disconnect() закроет IPC канал для обмена сообщениями, сгенерирует событие disconnect и завершит процесс после выполнения всех его операций.

//   console.log(`Child process is running`);

//   process.on('message', (message) =>
//     console.log('Message to child: ', message)
//   );

//   // process.send('Ping parent');
//   process.exit()

//   // ================== Path =================
//   const path = require('path')

//   // basename() - возвращает конечную часть пути, первым параметром принимает путь, вторым необязательным аргументом - расширение файла, которое нужно убрать из возвращаемого результата;

//   path.basename('/srv/app/app.js'); //app.js
//   path.basename('/srv/app/app.js', '.js'); //app

//   // dirname() - возвращает директорию переданного пути

//   path.dirname('/srv/app/app.js'); // \srv\app

//   // extname() - возвращает расширение файла переданного пути;
  
//   path.extname('/srv/app/app.js'); //.js

//   // isAbsolute() - булевое значение, true, если переданный путь является абсолютным

//   path.isAbsolute('/srv/app/app.js'); //true
//   path.isAbsolute('srv/app/app.js'); //false

//   // join() - принимает неограниченное количество составных частей пути, включая возвраты в родительские директории, и возвращает полученный в результате путь

//   path.join('/srv/app', '../config/..', 'app/app.js'); // \srv\app\app.js

//   // normalize() - приводит к корректному и оптимальному виду переданный путь;

//   path.normalize('/srv//app///app.js'); // \srv\app\app.js

//   // parse() - разбирает переданный путь на элементы и возвращает объект со следующими свойствами:
//   // root - корень пути;
//   // dir - директория;
//   // base - конечная часть пути;
//   // ext - расширение файла;
//   // name - имя файла (директории) без расширения;

//   // relative() - принимает два пути и возвращает относительный путь от первого ко второму;

//   path.relative(
//     '/srv/app/app.js',
//     '/srv/config/default.conf'
//   ); // ..\..\config\default.conf

//   // resolve() - принимает составные части пути и возвращает абсолютный путь полученного в результате обработки переданных сегментов пути.

//   path.resolve('/srv/app', 'app.js'); // D:\srv\app\app.js

//   // ====================== Files ======================

//   // Для чтения файла в асинхронном режиме используется метод Node.js readFile(), который принимает три параметра:
//   // путь к файлу;
//   // кодировка;
//   // callback-функция, вызываемая после получения содержимого файла.

//   const fs  = require('fs')

//   fs.readFile('files/data.txt', 'utf8', (err: Error, data: Buffer) => {
//     if (err) throw err;

//     console.log(data);
//   });

//   // const content = fs.readFileSync('files/data.txt', 'utf8');
//   // console.log(content);

//   try {
//     const content = fs.readFileSync('files/data.txt', 'utf8');
//     console.log(content);
//   } catch (e) {
//     console.log(e);
//   }

//   // Методы readFile() и readFileSync() для работы с файлами используют Buffer. Но есть и другой способ считать содержимое файла: создать поток с помощью Node.js fs.createReadStream(). Любой поток в Node.js является экземпляром класса EventEmitter, который позволяет обрабатывать возникающие в потоке события.
//   // Параметры, принимаемые fs.createReadStream():
//   // путь к файлу;
//   // объект со следующими настройками:
//   // encoding - кодировка (по умолчанию utf8);
//   // mode - режим доступа (по умолчанию 0o666);
//   // autoClose - если true, то при событиях error и finish поток закроется автоматически (по умолчанию true).
//   const stream = fs.createReadStream(
//     'files/data.txt',
//     'utf8'
//   );
//   stream.on('data', (data: string) => console.log(data));
//   stream.on('error', (err: Error) => console.log(`Err: ${err}`));

//   // Node.js readdir() работает асинхронно и принимает три аргумента:
//   // путь к директории;
//   // кодировку;
//   // callback-функцию, которая принимает аргументами ошибку и массив файлов директории (при успешном выполнении операции ошибка передается как null).
//   fs.readdir('files', 'utf8', (err: Error, files: string[]) => {
//     if (err) throw err;
  
//     console.log(files);
//   });

//   try {
//     const files = fs.readdirSync('files', 'utf8');
//     console.log(files);
//   } catch (e) {
//     console.log(e);
//   }

//   // В Node.js файлы могут быть записаны также синхронно и асинхронно. Для асинхронной записи имеется метод writeFile(), принимающий следующие аргументы:
//   // путь к файлу;
//   // данные для записи;
//   // параметры записи:
//   // кодировка (по умолчанию utf8);
//   // права доступа (по умолчанию 0o666);
//   // callback-функция, которая вызывается по завершению операции и единственным аргументом принимает ошибку (в случае успешной записи передается null).

//   fs.writeFile(
//     'files/data.txt',
//     'File Content',
//     'utf8',
//     (err: Error) => {
//       if (err) throw err;

//       console.log('Done');
//     }
//   );

//   // try {
//   //   fs.writeFileSync(
//   //     'files/data.txt',
//   //     'File Content',
//   //     'utf8'
//   //   );
//   //   console.log('Done');
//   // } catch (e) {
//   //   console.log(e);
//   // }

//   // Методы writeFile() и writeFileSync() перезаписывают уже имеющуюся в файле информацию новыми данными. Если вам нужно внести новые данные без удаления старых, используйте методы appendFIle() и appendFileAsync(), которые имеют идентичные параметры.
//   fs.appendFile(
//     'files/data.txt',
//     '\nFile Content 2',
//     'utf8',
//     (err: Error) => {
//       if (err) throw err;

//       console.log('Done');
//     }
//   );

//   // Для записи файла через потока ввода имеется метод fs.createWriteStream(), который возвращает поток ввода и принимает два параметра:
//   // путь к файлу;
//   // объект со следующими настройками:
//   // encoding - кодировка (по умолчанию utf8);
//   // mode - режим доступа (по умолчанию 0o666);
//   // autoClose - если true, то при событиях error и finish поток закроется автоматически (по умолчанию true).
//   const stream1 = fs.createWriteStream(
//     'files/data.txt',
//     'utf8'
//   );

//   stream.on('error', (err: Error) => console.log(`Err: ${err}`));
//   stream.on('finish', () => console.log('Done'));

//   stream.write('First line\n');
//   stream.write('Second line\n');
//   stream.end();

//   // Чтобы создать директорию, используйте методы mkdir() и mkdirSync().
//   // Node.js mkdir() работает асинхронно и принимает в качестве параметров:
//   // путь к директории;
//   // объект со следующими настройками:
//   // recursive - если true, создает директорию и все ее родительские директории согласно указанному пути, если они еще не существуют (по умолчанию false, т. е. все родительские директории уже должны быть созданы, иначе будет сгенерирована ошибка);
//   // mode - режим доступа, параметр не поддерживается на ОС Windows (по умолчанию 0o777);
//   // callback-функцию, которая единственным аргументом принимает ошибку, при успешном создании директории передается null.
//   // Вторым параметром можно сразу передать callback-функцию.
//   fs.mkdir('files/dir/subdir', { recursive: true }, (err: Error) => {
//     if (err) throw err;

//     console.log('Created');
//   });

//   // Чтобы удалить в Node.js файлы используйте методы unlink() и unlinkSync().
//   // Метод unlink() асинхронный и принимает имя файла, который нужно удалить, и callback-функцию с ошибкой в качестве параметра (null, если удаление прошло успешно).
//   fs.unlink('files/data.txt', (err: Error) => {
//     if (err) throw err;

//     console.log('Deleted');
//   });

//   // Для удаления директорий имеются методы rmdir() и rmdirSync() соответственно. Они полностью идентичны unlink() и unlinkSync(), только вместо имени файла принимают имя директории.
//   // Пример rmdir().
//   fs.rmdir('files/dir', (err: Error) => {
//     if (err) throw err;

//     console.log('Deleted');
//   });

//   // Проверка наличия в Node.js файла или директории происходит с помощью метода existsSync(), который принимает путь к файлу или директории и возвращает либо true, либо false. Как вы понимаете, Node.Js existsSync() работает в синхронном режиме.
//   try {
//     const exists = fs.existsSync('files');
//     console.log('Exists: ', exists);
//   } catch (e) {
//     console.log(e);
//   }
//   // Раньше был и Node.js exists(), но сейчас он уже официально устарел и не поддерживается.

//   // ====================== Buffer ======================

//   // Для создания пустого буфера размером в 10 байт используйте метод Buffer.alloc().

//   Buffer.alloc(10); //<Buffer 00 00 00 00 00 00 00 00 00 00>
//   let buffer = Buffer.alloc(3);
//   buffer.length; //3

//   // Чтобы заполнить создаваемый буфера значением по умолчанию, просто передайте это значение Buffer.alloc() вторым параметром
//   Buffer.alloc(10, 'A'); //<Buffer 41 41 41 41 41 41 41 41 41 41>
//   Buffer.alloc(10, 'ABC'); //<Buffer 41 42 43 41 42 43 41 42 43 41>

//   // Для создания буфера сразу нужного размера в Node.js имеется метод Buffer.from(), который принимает строку и создает под нее буфер.

//   Buffer.from('ABCDE'); //<Buffer 41 42 43 44 45>

//   // Вторым необязательным параметром методу Buffer.from() можно передать кодировку.

//   Buffer.from('ABCDE', 'base64'); //<Buffer 00 10 83>

//   // Чтобы записать данные в пустой или уже заполненный буфер, используйте метод [Buffer instance].write(), который принимает следующие параметры:
//   // строку для записи;
//   // позицию, с которой необходимо начать запись;
//   // длину от изначальной строки, которую необходимо записать;
//   // кодировку (по умолчанию utf8).
//   // Обязательным аргументом является только строка для записи.
//   //Запись в пустой буфер

//   let buffer1 = Buffer.alloc(3); //<Buffer 00 00 00>
//   buffer1.write('ABC'); //<Buffer 41 42 43>

//   //Перезапись заполненного буфера
//   let buffer2 = Buffer.from('ABC'); //<Buffer 41 42 43>
//   buffer2.write('XYZ'); //<Buffer 58 59 5a>

//   //сдвиг позиции
//   buffer.write('A', 1); //<Buffer 00 41 00>

//   //ограничение записи переданной строки
//   buffer.write('ABC', 0, 2); //<Buffer 41 42 00>

//   // Для получения данных из буфера в том формате, в котором они в него заносились, в Node.js имеется метод [Buffer instance].toString(), принимающий следующие необязательные параметры:
//   // кодировку (по умолчанию utf8);
//   // позицию, с которой необходимо начать чтение;
//   // позицию, на которой закончить чтение.
//   let bufferR = Buffer.from('ABC');
//   bufferR.toString(); //ABC
//   bufferR.toString('utf8', 1, 1); //B

//   // Объект Node.js класса Buffer может быть преобразован в формат JSON с помощью метода [Buffer instance].toJSON().

//   let bufferE = Buffer.from('ABC');
//   bufferE.toJSON().data; //[65, 66, 67]

//   // Buffer.isEncoding() - принимает кодировку и возвращает true, если ее испольщование допустимо при работе с буфером;
//   Buffer.isEncoding('ascii'); //true
//   // Buffer.isBuffer() - принимает данные и возвращает true, если они являются экземпляром класса Buffer;
//   Buffer.isBuffer('ascii'); //false
//   // Buffer.byteLength() - возвращает длину переданной строки в байтах (это не то же самое, что количество символов в строке), вторым необязательным параметром можно передать кодировку;
//   Buffer.byteLength('ascii'); // 5
//   Buffer.byteLength('ascii', 'base64'); //
//   // Buffer.concat() - принимает массив объектов класса Buffer и объединяет их в один, вторым необязательным параметром можно передать длину итогового буфера.
//   let buffer1R = Buffer.from('ABC');
//   let buffer2R = Buffer.from('CDE');
//   Buffer.concat([buffer1R, buffer2R], 5); //
//   Buffer.concat([buffer1R, buffer2R], 7); //

//   // ==================== Express ==================
//   const express = require('express')
//   const app = require('express')()

//   const host = '127.0.0.1'
//   const port = 7000

//   app.get('/home', (req: any, res: any) => {
//     res.status(200).type('text/plain')
//     res.send('Home page')
//   })

//   app.get('/about', (req: any, res: any) => {
//     res.status(200).type('text/plain')
//     res.send('About page')
//   })

//   app.post('/api/admin', (req: any, res: any) => {
//     res.status(200).type('text/plain')
//     res.send('Create admin request')
//   })

//   app.post('/api/user', (req: any, res: any) => {
//     res.status(200).type('text/plain')
//     res.send('Create user request')
//   })

//   app.use((req: any, res: any, next: any) => {
//     res.status(404).type('text/plain')
//     res.send('Not found')
//   })

//   app.listen(port, host, function () {
//     console.log(`Server listens http://${host}:${port}`)
//   })

//   // Для того чтобы Node.js сервер мог передавать по запросу находящиеся у него статические файлы (изображения, аудио, HTML, CSS, JS), используется функция фреймворка Express static().
//   // Добавьте перед определением всех маршрутов следующий код.
//   // app.use(express.static(`${__dirname}/assets`);

//   // app.use(
//   //   '/photos',
//   //   express.static(`${__dirname}/assets/img`)
//   // );
//   // app.use(
//   //   '/styles',
//   //   express.static(`${__dirname}/assets/css`)
//   // );

//   const express1 = require('express');
//   const app1 = express()
  
//   const host1 = '127.0.0.1';
//   const port1 = 7000;
  
//   app1.use(
//     '/uploads',
//     express.static(`${__dirname}/assets/images`)
//   );
//   app1.use('/styles', express.static(`${__dirname}/css`));
  
//   app1.listen(port, host, function () {
//     console.log(`Server listens http://${host1}:${port1}`);
//   });

//   // ------------------ Routing --------------------

//   // app.get('/api/users', (req, res) => {...});
//   // app.post('/api/users', (req, res) => {...});
//   // app.put('/api/users', (req, res) => {...});
//   // app.delete('/api/users', (req, res) => {...});

//   // ------------------ Rest API -------------------

//   // const express = require('express'),
//   //   app = express(),
//   //   fs = require('fs')

//   // const host = '127.0.0.1'
//   // const port = 3000

//   // app.use(express.json())
//   // app.use(express.urlencoded({ extended: true }))

//   // let file = 'data.json'

//   // if ((process.env.NODE_ENV === 'test')) file = 'data-test.json'

//   // app.use((req, res, next) => {
//   //   fs.readFile(file, (err, data) => {
//   //     if (err)
//   //       return res
//   //         .status(500)
//   //         .send({ message: 'Error while getting users' })

//   //     req.users = JSON.parse(data)

//   //     next()
//   //   })
//   // })

//   // app
//   //   .route('/api/users')
//   //   .get((req, res) => {
//   //     if (req.query.id) {
//   //       if (req.users.hasOwnProperty(req.query.id))
//   //         return res
//   //           .status(200)
//   //           .send({ data: req.users[req.query.id] })
//   //       else
//   //         return res
//   //           .status(404)
//   //           .send({ message: 'User not found.' })
//   //     } else if (!req.users)
//   //       return res
//   //         .status(404)
//   //         .send({ message: 'Users not found.' })

//   //     return res.status(200).send({ data: req.users })
//   //   })
//   //   .post((req, res) => {
//   //     if (req.body.user && req.body.user.id) {
//   //       if (req.users.hasOwnProperty(req.body.user.id))
//   //         return res
//   //           .status(409)
//   //           .send({ message: 'User already exists.' })

//   //       req.users[req.body.user.id] = req.body.user

//   //       fs.writeFile(
//   //         file,
//   //         JSON.stringify(req.users),
//   //         (err, response) => {
//   //           if (err)
//   //             return res
//   //               .status(500)
//   //               .send({ message: 'Unable create user.' })

//   //           return res
//   //             .status(200)
//   //             .send({ message: 'User created.' })
//   //         }
//   //       )
//   //     } else
//   //       return res
//   //         .status(400)
//   //         .send({ message: 'Bad request.' })
//   //   })
//   //   .put((req, res) => {
//   //     if (req.body.user && req.body.user.id) {
//   //       if (!req.users.hasOwnProperty(req.body.user.id))
//   //         return res
//   //           .status(404)
//   //           .send({ message: 'User not found.' })

//   //       req.users[req.body.user.id] = req.body.user

//   //       fs.writeFile(
//   //         file,
//   //         JSON.stringify(req.users),
//   //         (err, response) => {
//   //           if (err)
//   //             return res
//   //               .status(500)
//   //               .send({ message: 'Unable update user.' })

//   //           return res
//   //             .status(200)
//   //             .send({ message: 'User updated.' })
//   //         }
//   //       )
//   //     } else
//   //       return res
//   //         .status(400)
//   //         .send({ message: 'Bad request.' })
//   //   })
//   //   .delete((req, res) => {
//   //     if (req.query.id) {
//   //       if (req.users.hasOwnProperty(req.query.id)) {
//   //         delete req.users[req.query.id]

//   //         fs.writeFile(
//   //           file,
//   //           JSON.stringify(req.users),
//   //           (err, response) => {
//   //             if (err)
//   //               return res
//   //                 .status(500)
//   //                 .send({ message: 'Unable delete user.' })

//   //             return res
//   //               .status(200)
//   //               .send({ message: 'User deleted.' })
//   //           }
//   //         )
//   //       } else
//   //         return res
//   //           .status(404)
//   //           .send({ message: 'User not found.' })
//   //     } else
//   //       return res
//   //         .status(400)
//   //         .send({ message: 'Bad request.' })
//   //   })

//   // app.listen(port, host, () =>
//   //   console.log(`Server listens http://${host}:${port}`)
//   // )
// }

// namespace HW {
//   // =========================== HW01 ==========================

//   // 1. Инициализируй npm в проекте
//   // В корне проекта создай файл index.js
//   // Поставь пакет nodemon как зависимость разработки (devDependencies)
//   // В файле package.json добавь "скрипты" для запуска index.js
//   // Скрипт start который запускает index.js с помощью node
//   // Скрипт start:dev который запускает index.js с помощью nodemon

//   // 2.В корне проекта создай папку db. Для хранения контактов скачай и используй файл contacts.json (смотри в конце дз), положив его в папку db.
//   // В корне проекта создай файл contacts.js.
//   // Сделай импорт модулей fs и path для работы с файловой системой
//   // Создай переменную contactsPath и запиши в нее путь к файле contacts.json. Для составления пути ипользуй методы модуля path.
//   // Добавь функции для работы с коллекцией контактов. В функциях используй модуль fs и его методы readFile() и writeFile()
//   // Сделай экспорт созданных функций через module.exports
//   // contacts.js

//   /*
//   * Раскомментируй и запиши значение
//   * const contactsPath = ;
//   */

//   // TODO: задокументировать каждую функцию
//   function listContacts() {
//     // ...твой код
//   }

//   function getContactById(contactId) {
//     // ...твой код
//   }

//   function removeContact(contactId) {
//     // ...твой код
//   }

//   function addContact(name, email, phone) {
//     // ...твой код
//   }

//   // 3. Сделай импорт модуля contacts.js в файле index.js и проверь работоспособность функций для работы с контактами.

//   // 4. В файле index.js импортируется пакет yargs для удобного парса аргументов командной строки. Используй готовую функцию invokeAction() которая получает тип выполняемого действия и необходимые аргументы. Функция вызывает соответствующий метод из файла contacts.js передавая ему необходимые аргументы.
//   // index.js
//   const argv = require('yargs').argv;

//   // TODO: рефакторить
//   function invokeAction({ action, id, name, email, phone }) {
//     switch (action) {
//       case 'list':
//         // ...
//         break;

//       case 'get':
//         // ... id
//         break;

//       case 'add':
//         // ... name email phone
//         break;

//       case 'remove':
//         // ... id
//         break;

//       default:
//         console.warn('\x1B[31m Unknown action type!');
//     }
//   }

//   invokeAction(argv);
//   // Так же, вы можете использовать модуль commander для парсинга аргументов командной строки. Это более популярная альтернатива модуля yargs
//   const { Command } = require('commander');
//   const program = new Command();
//   program
//     .option('-a, --action <type>', 'choose action')
//     .option('-i, --id <type>', 'user id')
//     .option('-n, --name <type>', 'user name')
//     .option('-e, --email <type>', 'user email')
//     .option('-p, --phone <type>', 'user phone');

//   program.parse(process.argv);

//   const argv = program.opts();

//   // TODO: рефакторить
//   function invokeAction({ action, id, name, email, phone }) {
//     switch (action) {
//       case 'list':
//         // ...
//         break;

//       case 'get':
//         // ... id
//         break;

//       case 'add':
//         // ... name email phone
//         break;

//       case 'remove':
//         // ... id
//         break;

//       default:
//         console.warn('\x1B[31m Unknown action type!');
//     }
//   }

//   invokeAction(argv);

//   // 5. Запусти команды в терминале и сделай отдельный скриншот результата выполнения каждой команды.
//   // # Получаем и выводим весь список контактов в виде таблицы (console.table)
//   // node index.js --action list

//   // # Получаем контакт по id
//   // node index.js --action get --id 5

//   // # Добавялем контакт
//   // node index.js --action add --name Mango --email mango@gmail.com --phone 322-22-22

//   // # Удаляем контакт
//   // node index.js --action remove --id=3

//    // =========================== HW01 ==========================

//   //  Написать REST API для работы с контактами. Для работы с REST API используй Postman.
//   // Для этого cоздайте репозиторий с именем node_hw2 . 
//   // Перед выполнением проделайте следующие действия:
//   // Проинициализировать npm. Команда npm init
//   // Установить npm - модули: express, fs
//   // Добавить папку node_modules в .gitignore. 

//   //1 - Получить список всех контактов
//   // Добавить endpoint для получения всех контаков. Контакты должны приходить в формате json.
//   // Действия:
//   // Не получает body
//   // Вызывает функцию listContacts для работы с json-файлом contacts.json
//   // Возвращает массив всех контактов в json-формате со статусом 200
//   // http://localhost:3000
//   // /api/contacts

//   // 2 - Получить контакт по id
//   // Добавить endpoint для получения контакта по id. Контакты должны приходить в формате json.
//   // Не получает body
//   // Получает параметр contactId
//   // вызывает функцию getById для работы с json-файлом contacts.json
//   // если такой id есть, возвращает обьект контакта в json-формате со статусом 200
//   // если такого id нет, возвращает json с ключом "message": "Not found" и статусом 404
//   // http://localhost:3000
//   // /api/contacts/:contactId

//   // 3 - Создать контакт
//   // Добавить endpoint для создания контакта. Id для контакта создается на стороне сервера (продолжаем использовать логику первого дз). Созданный контакт должен приходить в формате json.
//   // Получает body в формате {name, email, phone}
//   // Если в body нет каких-то обязательных полей, возвращает json с ключом {"message": "Missing required name field"} и статусом 400
//   // Если с body все хорошо, добавляет уникальный идентификатор в объект контакта
//   // Вызывает функцию addContact(body) для сохранения контакта в файле contacts.json
//   // По результату работы функции возвращает объект с добавленным id {id, name, email, phone} и статусом 201
//   // http://localhost:3000
//   // /api/contacts

//   // 4 - Удалить контакт
//   // Добавить endpoint для удаления контакта по id. Сообщение с информацией о статусе удаления контакта должно приходить в формате json.
//   // Не получает body
//   // Получает параметр contactId
//   // Вызывает функцию removeContact для работы с json-файлом contacts.json
//   // Eсли такой id есть - даляет и возвращает json формата {"message": "Сontact deleted"} и статусом 200
//   // если такого id нет, возвращает json с ключом {"message": "Contact not found"} и статусом 404
//   // http://localhost:3000
//   // /api/contacts/:contactId

//   // 5 - Обновить данные контакта
//   // Добавить endpoint для обновления данных контакта по id. Обновленный контакт должен приходить в формате json.
//   // Получает параметр contactId
//   // Получает body в json-формате c обновлением любых полей name, email и phone .
//   // Вы указываете только те поля, которые хотите обновить.
//   // Если body нет, возвращает json с ключом {"message": "Missing fields"} и статусом 400
//   // Если с body все хорошо, вызывает функцию updateContact(contactId, body) (напишите ее) для обновления контакта в файле contacts.json
//   // По результату работы функции возвращает обновленный объект контакта и статусом 200. В противном случае, возвращает json с ключом {"message": "Contact not found"} и статусом 404
//   // http://localhost:3000
//   // /api/contacts/:contactId

  
// }