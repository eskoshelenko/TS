import { Task } from './Task'

type CB = (...args: any[]) => any

export class ObservableTask extends Task {
  observables: Array<CB>

  constructor(name: string) {
    super(name)

    this.observables = []
  }

  addObservable(observer: CB) {
    if(typeof observer === 'function') {
      this.observables.push(observer)
    }

    return
  }

  removeObserver(observer: CB) {
    this.observables = this.observables
      .filter(item => item !== observer)
  }

  notify() {
    for(let observable of this.observables) {
      observable()
    }

    super.execute()
  }

  execute() {
    this.notify()
    super.execute()
  }
}