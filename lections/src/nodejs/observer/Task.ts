export class Task {
  status: string

  constructor(public name: string){
    this.status = 'Not started'
  }

  execute () {
    this.status = 'Done'
  }
}