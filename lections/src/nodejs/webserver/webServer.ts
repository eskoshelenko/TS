import { Server } from 'http'

const simpleServer = new Server((req, res) => {
  console.log(`Request is comming...`)
  res.write('hello World!')
  res.end()
})

simpleServer.listen(3000, 'localhost', () => {
  console.log(`Server listen on localhost:3000`)
})