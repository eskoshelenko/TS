const { random, floor } = Math

// no singleton
// export class MyClass {
class MyClass {
  id: number
  
  constructor() {
    this.id = floor(random() * 100500) 
  }
}

// singleton
export default new MyClass()