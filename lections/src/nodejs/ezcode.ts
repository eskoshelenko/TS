import { EventEmitter } from 'events'
import { Server } from 'http'
import path from 'path'
import zlib from 'zlib'
import dns from 'dns'
import {
  readFileSync, 
  readFile, 
  createReadStream, 
  createWriteStream 
} from 'fs'
import { 
  Readable, 
  Writable, 
  Duplex, 
  Transform 
} from 'stream'
import { createServer } from 'net'
import { spawn, fork, exec } from 'child_process'
import cluster from 'cluster'
import { ObservableTask } from './observer/ObservableTask'
import { notifier, logger, updater } from './observer/Listeners'
import myClass from './singleton/MyClass'


namespace Presentation {
  
}

// namespace LC01 {
//   const { GREETING } = process.env
//   const pathToFile = path.join(__dirname, '../../assets/test.txt')

//   function print(text?: string): void {
//     console.log(text)
//   }

//   function handler (err: Error | null, data: Buffer) {
//     if(err) console.log(err.message)

//     console.log('Async ', data.toString())
//   }

//   const fileSync = readFileSync(pathToFile)
//     // <Buffer 31 20 32 20 33 20 74 65 73 74 2e 2e 2e>
//     .toString()
//     // 1 2 3 test...

//   ;(function (): void {
//   //stdin  => GREETING='Hello world' node dist/nodejs/ezcode.js 
//   //stdout => Hello world
//     print(GREETING)
//     readFile(pathToFile, handler)
//     console.log('Sync ', fileSync)
//   })()
// }

// namespace LC02 {
//   // ===================== Observer ======================
//   const task = new ObservableTask('My Task')

//   task.addObservable(logger)
//   task.addObservable(notifier)
//   task.addObservable(updater)

//   task.removeObserver(updater)

//   task.execute()

//   // =================== Event emitter ====================
//   const server = new EventEmitter()
//   const LISTEN = 'listen'
//   type Method = 'GET' | 'POST' | 'PUT' | 'DELETE'

//   const fn = () => console.log('Listen...')

//   server.on(LISTEN, (host, port) => {
//     console.log(`Server listen on: ${host}:${port}`)
//   })

//   setImmediate(() => server.on(LISTEN, fn))

//   setTimeout(() => {
//     // server.removeListener(LISTEN, fn)

//     // const listeners = server.listeners(LISTEN)
//     // server.removeListener(LISTEN, <(...args: any[]) => void>listeners[1])

//     server.emit(LISTEN, '127.0.0.1', 3001)
//   }, 1000)

//   server.emit(LISTEN, `http:\\\\localhost`, 3000)

//   class HttpServer extends EventEmitter {
//     constructor(handler: (...args: any[]) => any) {
//       super()

//       this.once('listen', () => {
//         console.log('Listen http server...')
//       })
//       this.setMaxListeners(12)
//       this.on('request', handler)
//       this.on('request', handler)
//       this.on('request', handler)
//       this.on('request', handler)
//       this.on('request', handler)
//       this.on('request', handler)
//       this.on('request', handler)
//       this.on('request', handler)
//       this.on('request', handler)
//       this.on('request', handler)
//       this.on('request', handler)
//       this.on('request', handler)
//       console.log('Count of listeners: ',this.listenerCount('request'))
//     }

//     listen() {
//       this.emit('listen')
//     }

//     request(body: string = '{"name":"SomeName"}') {
//       this.emit('request', 'POST', body)
//     }
//   }

//   const httpHandler = (method: Method, body: string) => {
//     console.log(`[${method}]: ${body}`)
//   }
//   const httpServer = new HttpServer(httpHandler)

//   httpServer.listen()
//   httpServer.listen()
//   httpServer.listen()
//   httpServer.listen()
//   httpServer.request()

//   // -------------------- HTTP ----------------------

//   const simpleServer = new Server((req, res) => {
//     res.write('hello World!')
//     res.end()
//   })

//   simpleServer.listen(3000, 'localhost', () => {
//     console.log(`Server listen on localhost:3000`)
//   })
// }

// namespace LC03 {
//   // ========================= Error =======================
//   // jest - run unit tests, same as mocha
//   // module assert - same as chai
//   const server = new EventEmitter()

//   server.on('error', (err) => {
//     console.log('Error appears: ', err)
//   })

//   try {
//     // readFile()
//   } catch (err) {
//     server.emit('error', err)
//   }

//   // ======================== Debug ========================
//   // node inspect path/to/file.js
//   /**
//    * node inspect dist/nodejs/debug/debug.js 
//       < Debugger listening on ws://127.0.0.1:9229/7d3be149-57af-4dba-92ba-38599f5392a8
//       < For help, see: https://nodejs.org/en/docs/inspector
//       <
//       connecting to 127.0.0.1:9229 ... ok
//       < Debugger attached.
//       <
//       Break on start in dist\nodejs\debug\debug.js:2
//         1 "use strict";
//       > 2 let x = 1;
//         3 let z = 2;
//         4 x = z;
//       debug> next
//       break in dist\nodejs\debug\debug.js:3
//         1 "use strict";
//         2 let x = 1;
//       > 3 let z = 2;
//         4 x = z;
//         5 debugger;
//       debug> next
//       break in dist\nodejs\debug\debug.js:4
//         2 let x = 1;
//         3 let z = 2;
//       > 4 x = z;
//         5 debugger;
//         6
//       debug> repl
//       Press Ctrl+C to leave debug repl
//       > z
//       2
//       > x
//       1
//    */

//   // ==================== Singleton ==================
  
//   // const instance1 = new MyClass()
//   // const instance2 = new MyClass()

//   // console.log(instance1, instance2)
//   // MyClass { id: 40241 } MyClass { id: 73857 }

//   const instance1 = myClass
//   const instance2 = myClass

//   console.log(instance1, instance2)
// }

// namespace LC04 {
//   // ================== Streams ===================

//   // ------------------ Readable ------------------
//   const pathToFile = path.join(__dirname, '../../assets/JSlec.txt')
//   const readStream = createReadStream(pathToFile)

//   const showDataBuf = (chank: Buffer) => {
//     // console.log(chank)
//     readStream.pause()
//     readStream.resume()
//   }
//   const show1stChank = (chank: Buffer) => {
//     console.log(chank.toString())
//     readStream.pause()
//   }

//   readStream.on('start', () => console.log('Starting...'))
//   readStream.on('end', () => console.log('Finish...'))
//   readStream.on('pause', () => console.log('Pause...'))
//   readStream.on('resume', () => console.log('Resume...'))
//   readStream.on('data', showDataBuf)
//   // <Buffer d0 9b d0 b5 d0 ba d1 86 d0 b8 d1 8f 20 31 0d 0a 2f 2f d0 a2 d0 b8 d0 bf d1 8b 20 d0 b4 d0 b0 d0 bd d0 bd d1 8b d1 85 2e 20 d0 a2 d0 b8 d0 bf d1 8b 20 ... 65486 more bytes>
//   // ...
//   // <Buffer 6e 74 61 69 6e 65 72 22 3e 0d 0a 3c 64 69 76 20 63 6c 61 73 73 3d 22 72 6f 77 22 3e 0d 0a 3c 64 69 76 20 63 6c 61 73 73 3d 22 63 6f 6c 2d 38 22 3e 0d ... 52565 more bytes>
//   // readStream.on('data', show1stChank)
//   // 1st part of text

//   console.log(readStream.eventNames())
//   // [ 'start', 'end', 'pause', 'resume', 'data', Symbol(kConstruct) ]

//   // Events 'data', 'end', 'error', 'close', 'readable'
//   // Methods .pipe(), .unpipe(), .read(), .unshift(), .resume(), .pause(), .isPaused(), .setEncoding()

//   // ------------------ Writable ------------------

//   // Wrong!!!
//   // let bigBuffer = ''
//   // const writeData = () => {
//   //   const writeStream = createWriteStream(pathToFile.split('.')[0] + 'Copy.txt')

//   //   writeStream.write(bigBuffer)
//   // }

//   // readStream.on('data', (data) => bigBuffer += data)
//   // readStream.on('end', writeData)

//   const {dir, name, ext} = path.parse(pathToFile)

//   const writeStream = createWriteStream(`${dir}\\${name}Copy${ext}`)

//   // readStream.on('data', (data) => writeStream.write(data))
//   // readStream.on('end', () => writeStream.write('LLLoooolll'))
//   // console.log(path.parse(pathToFile))

//   readStream.pipe(writeStream, { end: false })
//   readStream.on('end', () => writeStream.write('LLLoooolll'))

//   // Events 'drain', 'finish', 'error', 'close', 'pipe/unpipe'
//   // Methods .write(), .end(), .cork(), .uncork(), .setDefaultEncoding()

//   const readable = new Readable()

//   const writable = new Writable({
//     write(chunk, encoding, cb) {
//       console.log(chunk.toString())
//       cb()
//     }
//   })

//   const { stdin, stdout } = process

//   const alphabet = 'abcdefghijklmnopqrstuvwxyz\n'

//   readable.push(alphabet)
//   readable.push(null)

//   readable.pipe(stdout)  // analog of console.log(alphabet)
//   // abcdefghijklmnopqrstuvwxyz

//   stdin.pipe(writable) // small echo server =)
//   // dfsaf
//   // dfsaf

//   // fsadfasdf
//   // fsadfasdf

//   // safsadfsad
//   // safsadfsad

//   interface Readable {
//     charCode?: string
//   }

//   const inStream = new Readable({
//     read(size) {
//       setTimeout(() => {
//         if(this.charCode > 90) {
//           this.push(null)
//           return
//         }
//         this.push(String.fromCharCode(this.charCode++))
//       }, 100)
//     }
//   })

//   inStream.charCode = 65 // first charcode = 'A'
//   inStream.pipe(stdout)
//   // ABCDEFGHIJKLMNOPQRSTUVWXYZ

//   // ------------------- Duplex -------------------

//   // const duplex = new Duplex({
//   //   read(size) {
//   //     if(this.charCode > 90) {
//   //       this.push(null)
//   //       return
//   //     }
//   //     this.push(String.fromCharCode(this.charCode++))
//   //   },
//   //   write(chunk, encoding, cb) {
//   //     console.log(chunk.toString())
//   //     cb()
//   //   }
//   // })
//   // ------------------ Transform -----------------
  
//   const zip = new Transform({
//     transform(chunk, encoding, cb) {
//       this.push(chunk.toString().toUpperCase())
//       cb()
//     }
//   })

//   stdin.pipe(zip).pipe(stdout) // mutate data from console
//   // fsdfsdaf => FSDFSDAF

//   // --------------------- zLib -------------------

//   const inStr = createReadStream(pathToFile)
//   const outStr = createWriteStream(pathToFile + '.gz')

//   // simple archivator
//   inStr.pipe(zlib.createGzip()).pipe(outStr).on('finish', () => {
//     console.log('Done!!!')
//   })
// }

// namespace LC05 {
//   // ============ DNS(domain name server) ===============

//   dns.lookup('localhost', (err, address) => {
//     console.log(address)
//   })
//   dns.resolve('localhost', (err, address) => {
//     console.log(address)
//   })
//   dns.reverse('127.0.0.1', (err, names) => {
//     console.log(names)
//   })
//   // ==================== Networking ====================

//   const netServer = createServer()
//   const { stdout } = process
//   const sockets= {}
//   let count = 0

//   // stdout.write('\u001b[2J\u001b[0:0f')

//   netServer.on('connection', (socket) => {
//     socket.id = count++
//     sockets[socket.id] = socket

//     console.log(`connection established`)

//     socket.write('Welcome new client\n\r')

//     // socket.on('data', (data) => {
//     //   console.log(`Obtained data is ${data}`)
//     //   /*
//     //   Listening...
//     //   Obtained data is 1
//     //   Obtained data is 2
//     //   Obtained data is 3
//     //   Obtained data is 4
//     //   Obtained data is 5
//     //   */
//     //   socket.write(`Data obtained: -------${data}--------\n\r`)
//     //   /*
//     //   Welcome new client
//     //   1Data obtained: -------1--------
//     //   2Data obtained: -------2--------
//     //   3Data obtained: -------3--------
//     //   4Data obtained: -------4--------
//     //   5Data obtained: -------5--------
//     //   */
//     // })
//     // socket.on('close', () => console.log(`Session end!`))

//     // setTimeout(() => {
//     //   socket.emit('end')
//     // }, 4 * 1000);

//     socket.on('data', (data) => {
//       Object.values(sockets).forEach(cs => {
//         cs.write(`From client(${socket.id}) obtain data ${data}\n\r`)
//       })
//     })
//     socket.on('end', () => {
//       Object.values(sockets).forEach(cs => {
//         cs.write(`Client(${socket.id}) disconnected\n\r`)
//       })
//       delete sockets[socket.id]
//     })
//   })


//   // netServer.listen(3000, () => console.log(`Listening... `))
//   // using to connect telnet [host] [port]
//   // using ctrl+] and q to disconnect

//   // ======================= HTTP =========================

//   const server = new Server()

//   /* HTTP request:
//   host
//   version
//   method
//   head 
//   body  
//   */

//   server.on('request', (req, res) => {
//     console.log(req.method)
//     console.log(req.headers.host)
//     console.log(req.url)

//     res.writeHead(200, {'content-type': 'application/json'})
//     res.end('Helloooo!')
//   })

//   server.listen(3001, () => {
//     console.log('Listen on 3001 port...')
//   })
// }

namespace LC06_07 {
  // ===================== Express =======================

  
}

namespace LC06 {
  // ================== Child process ====================

  
}

namespace HW {
  // 1. Написать свой ивент эмиттер, которые будет выводить нынешнее состояние теста, раннинг, рераннинг, имя его при запуске команды тест эмитить запуск и менять состояние на раннинг, и выводить нандомное значение завершения теста, саксесс, фаилд

  // 2. сделать свой нодпэкедж, сделать старт скрипт

  // 3. сделать свой вебпак со следующими способностями minify js, css, html files; объединить все файлы одного типа в один исходный файл, создать архив из всех файлов js, css, html, в конец html добавить фразу.

  // 4. Создать сокет чат, клиент должен отправлять сообщение. а второй клиент принимает сообщение. 3 => 4 му и так далее.
}

