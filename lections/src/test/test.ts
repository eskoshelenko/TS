import 'mocha'
import sinon from "sinon"
import { expect, assert } from 'chai'

import { LC03 } from '../ezcode'

const { sum, pow, log } = LC03
function hello() {
  return 'Hello World!';
}

describe('Hello function', () => {
  it('should return hello world', () => {
    const result = hello();
    expect(result).to.equal('Hello World!');
  });
})

describe('Function sum', () => {
  it('Returns defined result', () => {
    const actual = sum(2, 3)

    expect(actual).not.to.be.undefined
  })
  it('Does not throw err if args is missing', () => {
    const test = () => sum(2)

    expect(test).not.to.throw()
  })
  it('Being called with two numbers returns sum', () => {
    const actual = sum(5, 5)
    const expected = 10

    assert.equal(actual, expected)
  })
  it('Being called with two strings returns string', () => {
    const actual = sum('Hello', ' world')

    expect(actual).to.be.a('string')
  })
})

describe('Function log', () => {
  let consoleSpy: ReturnType<typeof sinon.spy>
  let result: ReturnType<typeof log>
  const args = [1, 2, '3', 8]

  before(() => {
    consoleSpy = sinon.spy(console, "log")
    result = log(...args)
  })
  after(() => {
    consoleSpy.restore()
  })
  it('It shuld call console.log()', () => {5
    expect(consoleSpy.calledOnce).to.be.a.true
  })
  it('console.log with valid argumnts', () => {
    const actialArgs = consoleSpy.firstCall.args[0]

    args.forEach((el, idx) => expect(actialArgs[idx]).to.be.equal(el))
  })
  it('returns the return value of console.log()', () => {
    expect(result).to.equal(consoleSpy.returnValues[0])
  })
})