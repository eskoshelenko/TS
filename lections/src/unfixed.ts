// const throttle = (fn, throttleTime) => {
//   let start = -Infinity;
//   let cachedResult;

//   return function() {
//       const end = Date.now();
      
//       if (end - start >= throttleTime) {
//           start = end;
//           cachedResult = fn.apply(this, arguments);
//       }
      
//       return cachedResult;
//   };
// }

// const throttle = (fn, throttleTime) => {
//   let isThrottled = false

//   return function() {
//       if (isThrottled) return

//       fn.apply(this, arguments)

//       isThrottled = true
      
//       setTimeout(function() {
//           isThrottled = false
//       }, throttleTime)

//   }
// };