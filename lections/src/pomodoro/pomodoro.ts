// find elements
const min: HTMLElement = document.getElementById('min')!
const sec: HTMLElement = document.getElementById('sec')!
const audio: HTMLAudioElement = document.querySelector('audio')!
const element: Element = document.querySelector('.pomodoro')!

// constants
const pomodoroDuration: number = 1000 * 60 * 0.5 // 25
const shortBreakDuration: number = 1000 * 60 * 0.2 // 5

// service variables
let intervalId: NodeJS.Timer, pomodoro: number, shortBreak: number, current: string

function currentTimestamp (): number {
  return new Date().getTime()
}

function formatOutput (num: number): string {
  return num < 10 ? `0${num}` : `${num}`
}


function tick(period: number) {
  let diff = period - currentTimestamp()

  if (diff <= 0) {
    audio.play()
    clearInterval(intervalId)
    next()
    return
  }
  
  const minutes: number = Math.floor(diff % (1000 * 60 * 60) / (1000 * 60))
  const seconds: number = Math.floor(diff % (1000 * 60) / 1000)

  min.innerHTML = formatOutput(minutes)
  sec.innerHTML = formatOutput(seconds)
}

function next () {
  switch(current) {
    case 'pomodoro':
      shortBreak = currentTimestamp() + shortBreakDuration
      tick(shortBreak)
      intervalId = setInterval(tick, 1000, shortBreak)
      current = 'short'
      element.classList.remove('bg_danger')
      element.classList.add('bg-warning')
      break
    case 'short':
      pomodoro = currentTimestamp() + pomodoroDuration
      tick(pomodoro)
      intervalId = setInterval(tick, 1000, pomodoro)
      current = 'pomodoro'
      element.classList.remove('bg-warning')
      element.classList.add('bg-danger')
      break
  }
}


// initial setup
pomodoro = currentTimestamp() + pomodoroDuration
current = 'pomodoro'

intervalId = setInterval(tick, 1000, pomodoro)
// intervalId = setInterval(tick, 1000, pomodoroDuration)

// реализовать чтобы каждый четвертый интервал помодоро + шорт брейк был лонгбрейк 15 мин
// реализовать кнопочки стоп старт, пауза продолжить