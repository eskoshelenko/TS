class Router {
  constructor (routes, root) {
    this.routes = routes
    this.root = root
  }

  init() {
    // event hashchange appears when part of url has changed
    window.addEventListener('hashchange', () => this.hashChange())
  }

  hashChange() {
    if (location.hash.length > 0) {
      this.routes.forEach(route => {
        if (route.isActiveRoute(location.hash)) {
          this.follow(route.htmlFileName)
        }
      })
    } else {
      this.routes.forEach(route => {
        if (route.isHome) {
          this.follow(route.htmlFileName)
        }
      })
    }
  }

  follow(path) {
    const xhr = new XMLHttpRequest()

    xhr.open('GET', path)
    xhr.onloadend = () => {
      this.root.innerHTML = xhr.responseText
    }
    xhr.onerror = function () {
      this.root.innerHTML = `Load Error, status: ${xhr.status}`
    }
    xhr.send()
  }
}