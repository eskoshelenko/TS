// Array and String methods

// 1.Создать поиск кандидатов в массиве 
 по номеру телефона. Номер телефона может быть указан не полностью и в любом формате.
 const searchCandidatesByPhoneNumber = phone => {
     Your code...
 }
 ​
 searchCandidatesByPhoneNumber('43') 
 /// [Candidate, Candidate, Candidate ...]
 ​
 searchCandidatesByPhoneNumber('+1(869) 40') 
 /// [Candidate, Candidate ...]
 ​
 searchCandidatesByPhoneNumber('43') 
 /// [Candidate, Candidate, Candidate ...]
 ​
 searchCandidatesByPhoneNumber('+1(869)408-39-82') 
 /// [Candidate]

 Создать функию которая найдет кандидата по  _id и вернет его из массива 
 c отформатированной датой регистрации (поле registred). Дата должна иметь формат DD/MM/YY.
const getCandidateById = id => {
    Your code...
}
​
getCandidateById('5e216bc9a6059760578aefa4') 
// {
//    _id: '5e216bc9a6059760578aefa4',
//    name: 'Bernice Walton',
//    registred: '05/11/2015',
//    ... other properties in candidate 
// }
3
Написать функцию которая будет сортировать массив 
 по количеству денег лежащих на балансе (смотрим свойство balance)   в том порядке, в котором ей укажут в аргементе sortBy. Если параметр не был передан, то вернет массив в первоначальном состоянии.
const sortCandidatesArr = sortBy => {
    Your code...
}
​
sortCandidatesArr('asc') 
// отсортирует по-возростанию и вернет отсортированный массив
​
sortCandidatesArr('desc') 
// отсортирует по-убыванию и вернет отсортированный массив
​
sortCandidatesArr() 
// не будет сортировать, а просто вернет первоначальный массив
4*
Написать функцию, которая вернет объект в котором название ключей будут цвета глаз, а значением - массив с кандидатами имеющие такой цвет глаз. При этом нельзя самому указывать первоначальный объект с возможными вариантами цветами глаз, а сгенерировать их на основе массива с кандидатами, то есть пройтись по массиву 
 и брать смотреть на свойство eyeColor и добавлять значение цвета глаз кандидата как ключ объекта, если такого ключа не существует, ну и не добавлять - если  одноименный ключ уже существует
const getEyeColorMap = () => {
    Your code...
}
​
getEyeColorMap()
// {
//    grey:  [Candidate, Candidate, Candidate, Candidate ...],
//    blue:  [Candidate, Candidate, Candidate, ...],
//    green: [Candidate, Candidate, Candidate, Candidate, Candidate ...]
//    ... etc.
// }