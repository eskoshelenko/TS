//OOP func-constructors classes

// 1.Создать функцию - конструктор, которая будет иметь внутри все свойства обьекта emplyee из массива 
// const employeeObj = new Emploee(employee);
//   {
//     id: 0,
//     name: 'Valeriy',
//     surname: 'Zhmishenko',
//     salary: 1000, 
//     workExperience: 10, 
//     isPrivileges: true, 
//     gender: 'male',
//     }
​
// etc.

// 2.Добавить функции - конструктору метод (помним про prototype): getFullName который вернет полное имя начиная с фамилии в виде строки

// 3.Создать новый массив на основе 
//  в котором будут содержаться те же обьекты, но созданные функцией - конструктором Emploee. Новый массив должен содержать имя emplyeeConstructArr.
//  let createEmployesFromArr = (arr) => {
//      .... Your code
//  };
//  const emplyeeConstructArr = createEmployesFromArr(emplyeeArr) /// [{id: 0, name: 'Valeriy', surname: 'Zhmishenko', salary: 1000,  workExperience: 10,  isPrivileges: true, gender:'male' }]

// 4.Создать функцию которая вернет массив со всеми полными именами каждого employee, содержащегося в emplyeeConstructArr;
// const getFullNamesFromArr = (arr) => {
//   ... Your code
// }
  
// getFullNamesFromArr(emplyeeConstructArr) /// ['Денис Хрущ', 'Сергей Войлов', ... ]

// 5.Создать функцию которая вернет среднее значение зарплаты всех employee
// const getMiddleSalary = (arr) => {
//   ... Your code
// }
// ​
// getMiddleSalary(emplyeeConstructArr) /// 1560

// 6.Создать функцию которая выберет наугад работника из массива emplyeeConstructArr. Вы должны учитывать в функции длинну массива, так как если работников 7, а рандомное число будет равно больше 7, то результат будет undefined. Вы можете использовать обьявленную функцию в сомой же себе. Подсказка Math.random
// const getRandomEmployee = (arr) => {
//   ... Your code
//   }
//   ​
//   getRandomEmployee(emplyeeConstructArr) /// {id: 0, name: 'Valeriy', surname: 'Zhmishenko', salary: 1000,  workExperience: 10,  isPrivileges: true, gender:'male' }

// 7.Создать дескриптор со свойством fullInfo который будет записывать все свойства переданные ему в экземпляр от которого он вызывается. И выдавать все свойства, если к нему обратиться, в виде строки <Название свойства> - <значение> через запятую.
getter
// Создаем экземпляр на основе объекта 
// который берем из массива по 0 индексу
​
const employeeObj = new Emploee(employeeArr[0]);
​
employeeObj.fullInfo
// Результат
//  id - 1, name - Денис, surname - Хрущ
setter
// Создаем экземпляр на основе объекта 
// который берем из массива по 0 индексу
​
const employeeObj = new Emploee(employeeArr[0]);
// Результат employeeObj
//   {
//     id: 1,
//     name: 'Денис',
//     surname: 'Хрущ',
//     salary: 1010, 
//     workExperience: 10, 
//     isPrivileges: false, 
//     gender: 'male',
//   }
​
​
employeeObj.fullInfo = {name: 'Вася', salary: 9000}
// Результат employeeObj
//   {
//     id: 1,
//     name: 'Вася',
//     surname: 'Хрущ',
//     salary: 1010, 
//     workExperience: 10, 
//     isPrivileges: false, 
//     gender: 'male',
//   }
​
Если свойства в передаваемом объекте не было объявленно в классе, то это свойство не записываем в экземпляр
employeeObj.fullInfo = {name: 'Вася', salary: 9000, email: 'ex@mail.ua'}
// Результат employeeObj
//   {
//     id: 1,
//     name: 'Вася',
//     surname: 'Хрущ',
//     salary: 9000, 
//     workExperience: 10, 
//     isPrivileges: false, 
//     gender: 'male',
//   }
​
// "name" св-во обновилось, "salary" обновилось, 
// а вот email не добавился
​
Массив employeeArr
Скопируйте данные .в отдельный файл js и подключите его в html перед файлом js с вашим основным кодом
const emplyeeArr = [
    {
        id: 1,
        name: 'Денис',
        surname: 'Хрущ',
        salary: 1010, 
        workExperience: 10, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'male'
    },
    {
        id: 2,
        name: 'Сергей',
        surname: 'Войлов',
        salary: 1200, 
        workExperience: 12, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'male'
    },
    {
        id: 3,
        name: 'Татьяна',
        surname: 'Коваленко',
        salary: 480, 
        workExperience: 3, /// стаж работы (1 = один месяц)
        isPrivileges: true, /// льготы
        gender: 'female'
    },
    {
        id: 4,
        name: 'Анна',
        surname: 'Кугир',
        salary: 2430, 
        workExperience: 20, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'female'
    },
    {
        id: 5,
        name: 'Татьяна',
        surname: 'Капустник',
        salary: 3150, 
        workExperience: 30, /// стаж работы (1 = один месяц)
        isPrivileges: true, /// льготы
        gender: 'female'
    },
    {
        id: 6,
        name: 'Станислав',
        surname: 'Щелоков',
        salary: 1730, 
        workExperience: 15, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'male'
    },
    {
        id: 7,
        name: 'Денис',
        surname: 'Марченко',
        salary: 5730, 
        workExperience: 45, /// стаж работы (1 = один месяц)
        isPrivileges: true, /// льготы
        gender: 'male'
    },
    {
        id: 8,
        name: 'Максим',
        surname: 'Меженский',
        salary: 4190, 
        workExperience: 39, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'male'
    },
    {
        id: 9,
        name: 'Антон',
        surname: 'Завадский',
        salary: 790, 
        workExperience: 7, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'male'
    },
    {
        id: 10,
        name: 'Инна',
        surname: 'Скакунова',
        salary: 5260, 
        workExperience: 49, /// стаж работы (1 = один месяц)
        isPrivileges: true, /// льготы
        gender: 'female'
    },
    {
        id: 11,
        name: 'Игорь',
        surname: 'Куштым',
        salary: 300, 
        workExperience: 1, /// стаж работы (1 = один месяц)
        isPrivileges: false, /// льготы
        gender: 'male'
    },
];