class MyFirstClass {
  count1: number = 1
  count2: number = 2

  getTotalCount(): number {
    return this.count1 + this.count2
  }
}

class MyFirstClassConstructor {
  count1: number
  count2: number

  constructor(price: number, tax: number) {
    this.count1 = price
    this.count2 = tax
  }

  getTotalCount(): number {
    return this.count1 + this.count2
  }
}

//static
class MyFirstClassStatic {
  static PI: number = 3.14
  count: number

  constructor(count: number = 10.00) {
    this.count = count
  }

  getTotalCount(): number {
    return this.count + MyFirstClassStatic.PI
  }
}

// Inheritance
class User {
  name: string
  age: number

  constructor (name: string, age: number) {
    this.name = name
    this.age = age
  }

  greetings(): void {
    console.log(`Greetings ${this.name}`)
  }
}
class Employee extends User {
  workingHours: number

  constructor(name: string, age: number, workingHours: number) {
    super(name, age)
    this.workingHours = workingHours
  }

  greetings(): void {
    super.greetings()
    console.log(`You are ${this.age} years old`)
  }
}

// Access public, protected, private
// Abstract
abstract class Animal {
  public age: number
  public name: string

  constructor(age: number, name: string) {
    this.age = age
    this.name = name
  }

  public abstract eat()
}

// Properties
class ClassWithProperties {
  private _count: number

  public get counter(): number {
    return this._count
  }
  public set counter(val: number) {
    this._count = val
  }

  constructor(){}

  getTotalCount(): number {
    return this.counter
  }
}

// Interfaces
interface Vehicle {
  type: string
  fuel: number

  go(): void
}

class Car implements Vehicle {
  public type: string
  public fuel: number

  go(): void {
    console.log('Go go go')    
  }
}

// Modules
module MyModule {
  export interface MyInterface {}
  export class MyClass implements MyInterface {}
}

const myClass = new MyModule.MyClass()

// Namespaces
namespace MyNameSpace {
  export interface MyInterface {}
  export class MyClass implements MyInterface {}
}

const myClass1 = new MyNameSpace.MyClass()

// Decorators
// const person: Person = {
//   name: 'John'
// }

// function addAge(age: number): (person: Person) => AgedPerson {
//   return function(person): AgedPerson {
//     return {
//       name: person.name,
//       age
//     }
//   }
// }

// const newPerson = addAge(37)(person)
// console.log(newPerson)

function AddAge (age: number) {
  return function (targetClass: Person) {
    return class {
      name = targetClass.name
      age = age
    }
  }
}

@AddAge(37)
class Person {
  public name: string = 'John'
}

console.log(new Person())

// Promises
const delay = function (time: number): Promise<string> {
  return new Promise<string>((res: (string) => void, rej: (string) => void) => {
    setTimeout((): void => res('Done'), time * 1000)
  })
}

delay(1)
  .then((data: string): Promise<string> => {
    console.log(data)
    return delay(2)
  })
  .then((data: string): Promise<string> => {
    console.log(data)
    return delay(0.5)
  })
  .then((data: string): void => {
    console.log(data)
    console.log('Completely done!')
  })

  //Our exports
  export function getProperturlEnergy(): any[] { return []}
  export function endWorldHunger(n: boolean): void {}

  // Make this available as a global for non-module code
  // export as namespace MyFavoriteLibrary