var a: number = 1
let b: boolean = true
const c: string = 'string'
let variable: any = 123

variable = "str"

enum Days {
  Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
}

enum NumberDays {
  Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6, Saturday = 7
}

const arrOfStr: string[] = ['1', '2', '3', '4', 'hello']
const arrOfNums: Array<number> = [1, 2, 3, 4, 5, 6, 7]

arrOfStr.find(el => !Number.isNaN(parseInt(el, 10)))
arrOfNums.reduce((acc, el) => acc += el, 0)

// const tuple: [string, number] = ['Jim', 12, 'lol', 10] // err appears

function MyFunc(name: string, age: number, isActive: boolean): void {
  console.log(isActive 
    ? `This is ${name}. I'm ${age}`
    : `User ${name} is inactive`)  
}

const AnotherFunc = function (name: string): string {
  return name.toUpperCase()
}

const ArrowFunc= (numbers: number[]): number => numbers
  .reduce((acc, el) => acc += el, 0)

ArrowFunc([1,2,3])

const OptionalFunc = function(name: string, age?: number): string {
  return age ? `User age is ${age}` : `Unknown ${name}'s age`
}

const defaultSettings = (name: string, isActive: boolean = true): void => {
  console.log(`User - ${name} : Active - ${isActive}`)  
}

function overloadedFunc():string
function overloadedFunc(name: string):string
function overloadedFunc(name: string, secondName: string):string
function overloadedFunc(name?:string, secondName?: string):string {
  return name ? name + secondName : 'Stranger'
}

function sayHello(text: string, cb: (err: Error, msg: string) => void): void {
  cb(null, text)
}
function callback(err: Error, msg: string): void {
  if(!err) console.log(msg) 
  else throw err   
}

sayHello('Hello world', callback)

function unionType(age: string | number): void {
  console.log(`I'm ${age} yers old`)
}

function useGenerics<T>(age: T): void {
  console.log(age)  
}
function genericWithRestruction<T extends string | number>(age: T) {
  console.log(age)  
}

const u2 = {
  a1: {
    ss: 23,
    jj: 55
  },
  b1: 'str'
}
let {a1:{ss}, b1} = u2